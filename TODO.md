[d] Evaluating policy synchronously
    - Creating git infrastructure for the policies with the flow documentation. Will follow with feature branch with dev and production branch. Let's use [policies repo of the code.gitlab](https://code.vereign.com/gaiax/tsa/policies) to push policies. Git is a dependency should be preinstalled.
  
    - @kalin Create an environment to demonstrate
        - Kubernetes
        - Mongo Replicaset 1 primary and 2 secondary(v3.6)
        - Redis
    - Create a pipeline to sync the committed policy with MongoDB
    - @yuli Create a script to syncrhonise the data from Git to Mongo 
    - @dancho Sync policies from Mongo (replicaset deployment) with the policy services
    + @penkovski Create a service with endpoint to evaluate the policy
    - Create an example policy for the tests and demonstration
    - @rosen Implement the test logic
[] Implement the CI pipeline to test the service.
    - imlement the simplest test.
    - setup the Gitlab pipeline. Pick the simplest trigger event 

[d] Distributed cache feature
    - create Rego extensions
    + create HTTP service
    - create 2 example policies (1 for setting value, 1 for getting value)
    - Implement the test logic 
[] Persistance feature
[] Resolving DID
[] Evaluating policy asynchronously
Evaluatating policy groups asynchronously
Crypto libs to work with ursa (sign, key management)
Policy to generate an endpoint in Kong
Communication with OCM
