#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/confirmAction
#Author: Rosen Georgiev rosen.georgiev@vereign.com
#Spec: https://code.vereign.com/code/restful-api/blob/master/docs/identityConfirmAction.md

@rest @identity @all
Feature: VIAM - identity - confirmAction POST
  Confirms previously created action. Event is sent to both devices and entity

  Background:
    Given we are testing the VIAM Api

## DESTROYMENT NO LONGER NEEDS TO BE CONFIRMED. TBH WHAT ELSE NEEDS TO BE CONFIRMED
#  @destroyKeysForDevice @confirmAction @bug-rest-71
#  Scenario: Confirm action to Destroy a key associated with a device - Positive
##Create a new member
#    Then I register a new random member via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
##login with sms
#    Given I clear the request body
#    Then I login member with mode previousaddeddevice via VIAM API
#    And the field {status} has the value {OK}
#    And the field {code} has the value {200}
##Login the new member
#    Given I clear the request body
#    Then I login member with mode newdevice via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
##Confirm the new device
#    Given I clear the request body
#    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    Given I confirm the current new device via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
#    And the field {data} has the value {Succesfully executed}
##Then I list the devices
#    Given I clear the request body
#    Then I set the following request body {{}}
#    Given I list the devices via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
##Then I authorize the new device with the first Device
#    Given I clear the request body
#    Given I authorize the current new device via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
##I call to destroy the current publicKey to the device
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest Body with key {authenticationPublicKey}
#    Then I destroy a key for a device via VIAM API
#    And the field {status} has the value {OK}
##Then I confirm the action
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    Then I confirm the current action via VIAM API
#    And the field {status} has the value {OK}
#    And the field {code} has the value {200}
#    And the field {data} has the value {Action executed}
#
#  @destroyKeysForDevice @confirmAction @negative
#  Scenario: Try to Confirm action to Destroy a key for the same device - Negative
#    #Create a new member
#    Then I register a new random member via VIAM API
#    And the field {status} has the value {OK}
##login with sms
#    Given I clear the request body
#    Then I login member with mode previousaddeddevice via VIAM API
#    And the field {status} has the value {OK}
#    And the field {code} has the value {200}
##I call to destroy the current publicKey to the device
#    Given I set the following request body {{}}
#    Then I destroy the key for the current device via VIAM API
#    And the field {status} has the value {OK}
##Then I try to confirm the action
#    Given I clear the request body
#    Then I confirm the current action via VIAM API
#    And the field {status} has the value {There was an error during getting of OTP}
#    And the field {code} has the value {400}
#    And the field {data} has the value {Can not authenticate action from same device}
#
#  @confirmAction @negative
#  Scenario Outline: Try to Confirm a newly created action without auth headers [<header>] - Negative
# #Create a new member
#    Then I register a new random member via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
##login with sms
#    Given I clear the request body
#    Then I login member with mode previousaddeddevice via VIAM API
#    And the field {status} has the value {OK}
#    And the field {code} has the value {200}
##Login the new member
#    Given I clear the request body
#    Then I login member with mode newdevice via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
##Confirm the new device
#    Given I clear the request body
#    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    Given I confirm the current new device via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
#    And the field {data} has the value {Succesfully executed}
##I call to destroy the current publicKey to the device
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest Body with key {authenticationPublicKey}
#    Then I destroy a key for a device via VIAM API
#    And the field {status} has the value {OK}
##Then I confirm the action
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    And I delete the headers
#      | <header> |
#    Then I confirm the current action via VIAM API
#    And the field {status} has the value {No authentication values}
#    And the field {code} has the value {400}
#    Examples:
#      | header    |
#      | uuid      |
#      | token     |
#      | publicKey |
#
#  @confirmAction @negative
#  Scenario Outline: Try to Confirm an action with invalid auth headers [<header>] - Negative
##Create a new member
#    Then I register a new random member via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
##login with sms
#    Given I clear the request body
#    Then I login member with mode previousaddeddevice via VIAM API
#    And the field {status} has the value {OK}
#    And the field {code} has the value {200}
##Login the new member
#    Given I clear the request body
#    Then I login member with mode newdevice via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
##Confirm the new device
#    Given I clear the request body
#    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    Given I confirm the current new device via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
#    And the field {data} has the value {Succesfully executed}
##Then I list the devices
#    Given I clear the request body
#    Then I set the following request body {{}}
#    Given I list the devices via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
##Then I authorize the new device with the first Device
#    Given I clear the request body
#    Given I authorize the current new device via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
##I call to destroy the current publicKey to the device
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest Body with key {authenticationPublicKey}
#    Then I destroy a key for a device via VIAM API
#    And the field {status} has the value {OK}
##Then I confirm the action
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    And I set the headers
#      | <header> | <value> |
#    Then I confirm the current action via VIAM API
#    And the field {status} has the value {<status>}
#    And the field {code} has the value {400}
#    Examples:
#      | header    | value | status                   |
#      | uuid      | dasda | No authentication values |
#      | token     | dasda | Bad session              |
#      | publicKey | dasda | No authentication values |
#
#  @destroyKeysForDevice @confirmAction  @negative @bug-rest-71
#  Scenario: Try to Confirm action to Destroy a key associated with a device with an expired QR code - Negative
##Create a new member
#    Then I register a new random member via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
##login with sms
#    Given I clear the request body
#    Then I login member with mode previousaddeddevice via VIAM API
#    And the field {status} has the value {OK}
#    And the field {code} has the value {200}
##Login the new member
#    Given I clear the request body
#    Then I login member with mode newdevice via VIAM API
#    And the field {status} has the value {OK}
#    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
##Confirm the new device
#    Given I clear the request body
#    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    Given I confirm the current new device via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
#    And the field {data} has the value {Succesfully executed}
##Then I list the devices
#    Given I clear the request body
#    Then I set the following request body {{}}
#    Given I list the devices via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
##Then I authorize the new device with the first Device
#    Given I clear the request body
#    Given I authorize the current new device via VIAM API
#    Then the field {status} has the value {OK}
#    And the field {code} has the value {200}
##I call to destroy the current publicKey to the device
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest Body with key {authenticationPublicKey}
#    Then I destroy a key for a device via VIAM API
#    And the field {status} has the value {OK}
##Then I confirm the action
#    Given I set the following request body {{}}
#    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
#    Then I try to confirm the action with an expired qr code via VIAM API
#    And the field {status} has the value {There was an error during getting of OTP}
#    And the field {code} has the value {400}
#    And the field {data} has the value {Wrong code}