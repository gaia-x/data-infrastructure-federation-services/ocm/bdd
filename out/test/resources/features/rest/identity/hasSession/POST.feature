#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/hasSession
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - hasSession POST
  Checks whether there is active session

  Background:
    Given we are testing the VIAM Api

  @hasSession
  Scenario: Check for Active session when logged with SMS mode - Positive
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Check for active session
    Given I set the following request body {{}}
    Then I check if there an active session via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Has session}

  @hasSession
  Scenario: Check for Active session when logged with previousaddeddevice mode - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Logout
    Given I clear the request body
    Then I call POST /logout via VIAM API
    And the field {status} has the value {OK}
#Login the new member with previousaddeddevice mode
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Check for active session
    Given I set the following request body {{}}
    Then I check if there an active session via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Has session}

  @hasSession @negative
  Scenario: Check for active session for unauthorized newdevice mode - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Check for active session
    Given I set the following request body {{}}
    Then I check if there an active session via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}

  @hasSession @addNewDevice
  Scenario: Check for Active session when logged with mode fromanotherauthenticateddevice - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
#Then login with the new device
    Given I set the following request body {{}}
    Given I login member with mode fromanotherauthenticateddevice via VIAM API
    And the field {status} has the value {OK}
#Then I list the devices
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Then I set the following request body {{}}
    Given I list the devices via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
#Then I authorize the new device with the first Device
    Given I clear the request body
    Given I authorize the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
#Check for active session
    Given I set the following request body {{}}
    Then I check if there an active session via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Has session}

  @hasSession @negative
  Scenario Outline: Try to Check for Active session without required header [<header>] - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Check for active session
    Given I set the following request body {{}}
    Then I delete the headers
      | <header> |
    Then I check if there an active session via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header    |
      | uuid      |
      | token     |
      | publicKey |

  @hasSession @negative
  Scenario Outline: Try to Check for Active session with invalid required header [<header>] - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Check for active session
    Given I set the following request body {{}}
    Then I set the headers
      | <header> | <value> |
    Then I check if there an active session via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    Examples:
      | header    | value  | status                   |
      | uuid      |        | No authentication values |
      | token     |        | No authentication values |
      | publicKey |        | No authentication values |
      | uuid      | dsadas | No authentication values |
      | token     | dsadas | Bad session              |
      | publicKey | dsadas | No authentication values |

