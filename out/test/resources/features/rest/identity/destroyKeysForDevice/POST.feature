#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/destroyKeysForDevice
#Author: Rosen Georgiev rosen.georgiev@vereign.com
#Spec: https://code.vereign.com/code/restful-api/blob/master/docs/identityDestroyKeysForDevice.md

@rest @identity @all
Feature: VIAM - identity - destroyKeysForDevice POST
  Creates action for destroying all keys on server for given device. Returns
  QR code and actionID. QR code is on top of OTP, which means every time
  getNewEvents is called new QRCodeUpdated event for that action is generated and
  returned to all devices attached to entity

  Background:
    Given we are testing the VIAM Api

  @destroyKeysForDevice
  Scenario: Destroy a key associated with a device - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Succesfully executed}
#I call to destroy the current publicKey to the device
    Given I set the following request body {{}}
    And I load object with key {secondPublicKey} from DataContainer into currentRequest Body with key {authenticationPublicKey}
    Then I destroy a key for a device via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Key destroyed}

  @addNewDevice @negative
  Scenario Outline: Try to destroy a publicKey of a device without auth header [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Succesfully executed}
#I call to destroy the current publicKey to the device
    Given I set the following request body {{}}
    Then I delete the headers
      | <header> |
    Then I destroy the key for the current device via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header    |
      | uuid      |
      | token     |
      | publicKey |
