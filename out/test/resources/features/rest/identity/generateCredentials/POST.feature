#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/generateCredentials
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - generateCredentials POST
  Genetarate a username and password for an entity

  Background:
    Given we are testing the VIAM Api

  @generateCredentials
  Scenario: Generate Credentials for an entity - Positive
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.Username} is present and not empty
    And the field {$.data.Password} is present and not empty

  @generateCredentials @negative
  Scenario Outline: Try to Generate Credentials with a missing header [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I delete the headers
      | <header> |
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}
    Examples:
      | header    |
      | uuid      |
      | token     |
      | publicKey |

  @generateCredentials @negative
  Scenario Outline: Try to Generate Credentials with invalid header [<header>] & [<value>] - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I set the headers
      | <header> | <value> |
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}
    Examples:
      | header    | value | status                   |
      | uuid      |       | No authentication values |
      | token     |       | No authentication values |
      | publicKey |       | No authentication values |
      | uuid      | dasda | No authentication values |
      | token     | dasda | Bad session              |
      | publicKey | dasda | No authentication values |

  @generateCredentials @negative
  Scenario: Try to create a new credentials with an authorized publicKey - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    And I add a new publicKey header to the currentRequest
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}