#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://env/api/identity/agreeOnRegistration
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - agreeOnRegistration POST
  This call adds register claims to an identity

  Background:
    Given we are testing the VIAM Api

  @agreeOnRegistration
  Scenario: Confirm the privacy policy and create new member with Email identificator - Positive
  #Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the Privacy Policy
    Given I confirm the privacy policy via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}

  @agreeOnRegistration
  Scenario: Confirm the privacy policy and create new member with Phone identificator - Positive
  #Add identificator
    Given I submit a new identificator with random phoneNumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator phonenumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
   #Confirm the Privacy Policy
    Given I confirm the privacy policy via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}

  @submitRegisterClaims @negative
  Scenario: Try to confirm the privacy policy without publickey - Negative
 #Add identificator
    Given I submit a new identificator with random phoneNumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator phonenumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the Privacy Policy
    Then I delete the headers
      | publicKey |
    Given I confirm the privacy policy via VIAM API
    And the field {status} has the value {Not provided public key}
    And the field {code} has the value {400}