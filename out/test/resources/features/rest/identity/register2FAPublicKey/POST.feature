#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/register2FAPublicKey
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - register2FAPublicKey POST
  Register 2FA publicKey with username nad password

  Background:
    Given we are testing the VIAM Api

  @register2FAPublicKey
  Scenario: Register new 2FA public Key - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
#Register the 2FA publicKey
    Given I clear ALL headers
    And I add a new publicKey header to the currentRequest
    Then I register the 2FA publicKey with the current username and password via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.actionID} is present and not empty

  @register2FAPublicKey @negative @bug-rest-82 @wip
  Scenario: Try to Register new 2FA public Key with already used publicKey - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
#Try to Register the 2FA publicKey that is already used
    Then I register the 2FA publicKey with the current username and password via VIAM API
    And the field {status} has the value {???}
    And the field {code} has the value {400}

  @register2FAPublicKey @negative
  Scenario: Try to Register new 2FA public Key without the publicKey - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
#Try to Register the 2FA publicKey that is already used
    Given I clear ALL headers
    Then I register the 2FA publicKey with the current username and password via VIAM API
    And the field {status} has the value {Not provided public key}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}