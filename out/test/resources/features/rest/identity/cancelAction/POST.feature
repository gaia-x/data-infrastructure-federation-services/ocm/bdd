#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/cancelAction
#Author: Rosen Georgiev rosen.georgiev@vereign.com
#Spec: https://code.vereign.com/code/restful-api/blob/master/docs/identityCancelAction.md

@rest @identity @all
Feature: VIAM - identity - cancelAction POST
  Cancels previously created action. Event is sent to entity

  Background:
    Given we are testing the VIAM Api

  @cancelAction
  Scenario: Cancel a newly created action - Positive
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.ActionID} is present and not empty
#Then I cancel the action
    Then I cancel the current action via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Action cancelled}

  @cancelAction @negative @bug-rest-70
  Scenario: Try to cancel an non existent Action - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Then I cancel the action
    Given I clear the request body
    And I set the request fields
      | actionID | 67076864-9f9a-11e8-98d0-529269fb1459 |
    Then I cancel an action via VIAM API
    And the field {status} has the value {Error during getting action}
    And the field {code} has the value {400}
    #And the field {data} has the value {?}

  @cancelAction @negative
  Scenario Outline: Try to Cancel a newly created action without auth headers [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.ActionID} is present and not empty
#Then I cancel the action
    Then I delete the headers
      | <header> |
    Then I cancel the current action via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header    |
      | uuid      |
      | token     |
      | publicKey |

  @cancelAction @negative
  Scenario Outline: Try to Cancel a newly created action with invalid auth headers [<header>] - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.ActionID} is present and not empty
#Then I cancel the action
    Then I set the headers
      | <header> | <value> |
    Then I cancel the current action via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    Examples:
      | header    | value | status                   |
      | uuid      | dasda | No authentication values |
      | token     | dasda | Bad session              |
      | publicKey | dasda | No authentication values |

  @cancelAction @negative
  Scenario: Try to Cancel an action with unverified publicKey - Negative
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data.ActionID} is present and not empty
#Then I cancel the action
    Then I add a new publicKey header to the currentRequest
    Then I cancel the current action via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}