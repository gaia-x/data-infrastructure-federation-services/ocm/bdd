#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/identity/submitIdentificator
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @identity @all
Feature: VIAM - identity - submitIdentificator POST
  This call registers a new member

  Background:
    Given we are testing the VIAM Api

  @submitIdentificator
  Scenario: Submit a new email Identificator - Positive
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.givenName} has the value {}
    And the field {$.data.familyName} has the value {}

  @submitIdentificator
  Scenario: Submit a new phone Identificator - Positive
    Given I submit a new identificator with random phoneNumber via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.givenName} has the value {}
    And the field {$.data.familyName} has the value {}

  @submitIdentificator @negative
  Scenario Outline: Try to register a user with the same identificator [<param>] - Positive
  #Add identificator
    Given I submit a new identificator with random email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Confirm the sms/email code
    Then I confirm indentificator with code {98128366} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Add register claims
    Given I load the REST request {Register.json} with profile {create}
    Then I submit registration claims with identificator email via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And I get the value of {$.<param>} from the last Request Body and store it in the DataContainer with key {identificator}
  #Confirm the Privacy Policy
    Given I confirm the privacy policy via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
  #Try to submit already registered identificator
    Given I clear the request body
    Then I set the request fields
     | registerToken | emptyToken |
    And I load object with key {identificator} from DataContainer into currentRequest Body with key {identificator}
    Given I submit a new identificator via VIAM API
    And the field {status} has the value {Entity is already registered}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}
    Examples:
      | param       |
      | email       |
      | phonenumber |

  @submitIdentificator @negative
  Scenario: Try to Submit Identificator without providing a publicKey- Negative
    Given I set the request fields
      | registerToken | emptyToken |
    Given I submit a new identificator via VIAM API
    And the field {code} has the value {400}

  @submitIdentificator @negative @bug-rest-201
  Scenario: Try to Submit Identificator without providing an identificator - Negative
    Given I add a new publicKey header to the currentRequest
    And I set the request fields
      | registerToken | emptyToken |
    Given I submit a new identificator via VIAM API
    And the field {status} has the value {There was an error with the input fields}
    And the field {code} has the value {400}
  # bug-rest-201 And the field {data} has the value {There was an error with the input fields: identificator is required}