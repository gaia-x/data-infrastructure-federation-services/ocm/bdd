#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/identity/revokeDevice
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@rest @deviceManager @all
Feature: VIAM - Device Manager - revokeDevice - POST
  This endpoint is for revoking device

  Background:
    Given we are testing the VIAM Api

  @revokeDevice
  Scenario: Revoke device
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Revoke the device
    And I revoke the current device via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {{}}

  @revokeDevice @negative
  Scenario Outline: Try to revoke a device with invalid deviceId [<deviceId>]
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Revoke the device
    And I revoke a device with device ID {<deviceId>} via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    And the field {data} has the value {<data>}
    Examples:
      | deviceId                             | status                                   | code | data                                                           |
      |                                      | There was an error with the input fields | 400  | There was an error with the input fields: deviceID is required |
      | ce13ffba-ece1-4062-908f-80ab3b4bbcb3 | Unknown deviceID                         | 401  | {}                                                             |


  @revokeDevice @negative
  Scenario Outline: Try to revoke a device without auth header [<header>]
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#List the devices
    Given I clear the request body
    Then I set the following request body {{}}
    And I list the devices via VIAM API
    And the field {status} has the value {OK}
#Delete the prop from the header
    And I delete the headers
      | <header> |
    And I revoke the current device via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    And the field {data} has the value {<data>}
    Examples:
      | header    | status                   | code | data |
      | uuid      | No authentication values | 400  | {}   |
      | publicKey | No authentication values | 400  | {}   |
      | token     | No authentication values | 400  | {}   |
