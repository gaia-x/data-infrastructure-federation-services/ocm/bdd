#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/removeGuardin
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @guardian @all
Feature: VIAM - entity - removeGuardian POST
  Remove a Guardian from an entity

  Background:
    Given we are testing the VIAM Api


  @removeGuardian
  Scenario: Remove a guardian from an child entity - Positive
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Remove the Guardian
    Then I remove a guardian via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @removeGuardian @negativе
  Scenario Outline: Try to remove a guardian with missing param - [<param>]- Negative
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Remove the Guardian
    Given I remove the following Keys from current request Body:
      | <param> |
    Then I remove a guardian via VIAM API
    And the field {status} has the value {There was an error with the input fields}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | param        | data                                                              |
      | entityUUID   | There was an error with the input fields: entityUUID is required   |
      | guardianUUID | There was an error with the input fields: guardianUUID is required |

  @removeGuardian @negative
  Scenario Outline: Try to remove a guardian without auth header [<header>] - Negative
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Remove the Guardian
    And I delete the headers
      | <header> |
    Then I remove a guardian via VIAM API
    Then I delete a claim {name} with tag {personalName} of the current member via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header |
      | token  |
      | uuid   |