#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/removeViewer
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @viewer @all
Feature: VIAM - entity - removeViewer POST
  Remove a viewer link between 2 entities

  Background:
    Given we are testing the VIAM Api

  @removeViewer
  Scenario: Remove the new viewer link between 2 entities - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
#Then I remove the viewer link
    Then I remove a viewer link via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @removeViewer
  Scenario: Remove the new viewer link between 2 entities and then get it to verify - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
#Then I remove the viewer link
    Then I remove a viewer link via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].viewed} contains the value {=1}

  @removeViewer
  Scenario: Remove the new viewed link between 2 entities and then get it to verify - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
#Then I remove the viewer link
    Then I remove a viewer link via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].viewers} contains the value {=1}

  @removeViewer @negative
  Scenario Outline: Try to Remove a viewer link with invalid [<key>] - [<value>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
#Then I remove the viewer link
    And I set the request fields
      | <key> | <value> |
    Then I remove a viewer link via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    And the field {data} has the value {<data>}
    Examples:
      | key        | value                                | status                                   | code | data                                                             |
      | entityUUID |                                      | There was an error with the input fields | 400  | There was an error with the input fields: entityUUID is required |
      | entityUUID | 9e3d1f32-8fdb-11e8-9eb6-529269fb1459 | Access denied                            | 401  | Can't get entity entityUUID                                      |
      | viewerUUID |                                      | There was an error with the input fields | 400  | There was an error with the input fields: viewerUUID is required |
      | viewerUUID | 9e3d1f32-8fdb-11e8-9eb6-529269fb1459 | Error removing viewer                    | 400  | Error removing opposite relation: could not find entity          |