#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/removeMember
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @member @all
Feature: VIAM - entity - removeMember POST
  Remove a new member link between entities

  Background:
    Given we are testing the VIAM Api

  @removeMember
  Scenario: Remove a member link from an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I create member link
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {memberUUID}
    Then I create new member link via VIAP API
    And the field {status} has the value {OK}
#Then I remove a member link
    Then I remove a member link from an entity via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @removeMember @negative
  Scenario Outline: Try to Remove a member link from an entity when providing invalid params [<profile>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Then I remove a member link
    Given I clear the request body
    And I load the REST request {Member.json} with profile {<profile>}
    Then I remove a member link from an entity via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile                 | status                                   | data                                                             |
      | missing_member          | There was an error with the input fields | There was an error with the input fields: memberUUID is required |
      | empty_member            | There was an error with the input fields | There was an error with the input fields: memberUUID is required |
  #    | invalid_uuid_member     | Error removing member                    | Error removing relation:Error removing a relation: could not find entity 80e41b47-d8cc-44a2-b696 |
      | missing_entityuuid      | There was an error with the input fields | There was an error with the input fields: entityUUID is required |
      | empty_entityuuid        | There was an error with the input fields | There was an error with the input fields: entityUUID is required |
      | invalid_uuid_entityuuid | Error removing member                    | Error removing relation: could not find entity                   |
      | non_existing_uuid       | Error removing member                    | Error removing relation: could not find entity                   |