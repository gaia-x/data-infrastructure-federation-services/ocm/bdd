#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/addClaim
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @all
Feature: VIAM - entity - addClaim POST
  Adding a new claim to an entity

  Background:
    Given we are testing the VIAM Api

  @addClaim @smoke
  Scenario Outline: Create a new user and add claim - [<profile>]- Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {<profile>}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
    Examples:
      | profile             |
      | create_private      |
      | create_invitational |
      | valueType_composite |

  @createEntity
  Scenario Outline: Add a claim to a guarded entity - [<type>] - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity
    Given I clear the request body
    Then I create a new entity with type {<type>} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the child entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    Examples:
      | type |
      | 1    |
      | 2    |
      | 3    |
      | 4    |
      | 5    |

  @addClaim @negative @bug-rest-20
  Scenario Outline: Try to add a new claim to an entity with invalid value [<profile>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
   # And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {UserUUID_1}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {<profile>}
    #And I load object with key {UserUUID_1} from DataContainer into currentRequest Body with key {entityuuid}
    Then I add a new claim to an entity via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    And the field {data} has the value {<data>}
    Examples:
      | profile            | status                                   | code | data                                                             |
      | missing_claim      | There was an error with the input fields | 400  | There was an error with the input fields: claim is required      |
      | missing_value      | There was an error with the input fields | 400  | There was an error with the input fields: value is required      |
      | missing_access     | There was an error with the input fields | 400  | There was an error with the input fields: access is required     |
      | missing_entityuuid | There was an error with the input fields | 400  | There was an error with the input fields: entityuuid is required |
      | missing_tag        | There was an error with the input fields | 400  | There was an error with the input fields: tag is required        |
      | missing_valueType  | There was an error with the input fields | 400  | There was an error with the input fields: valueType is required  |
    #  | invalid_access     | There was an error during addition of claim | 400  | There was an error with the input fields: entityuuid is required |

  @addClaim @negative
  Scenario Outline: Try to create a new claim with missing header [<header>] - negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    And I delete the headers
      | <header> |
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header    |
      | token     |
      | uuid      |
      | publicKey |

  @logout @addClaim @negative
  Scenario: Logout a new member and then try to add new claim - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Logout
    Given I clear the request body
    Then I call POST /logout via VIAM API
    And the field {status} has the value {OK}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {Bad session}
    And the field {code} has the value {400}