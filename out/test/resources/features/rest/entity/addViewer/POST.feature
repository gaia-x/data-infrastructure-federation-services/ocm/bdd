#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/addViewer
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @viewer @all
Feature: VIAM - entity - addViewer POST
  Add a new viewer link between 2 entities

  Background:
    Given we are testing the VIAM Api

  @addViewer
  Scenario: Add a new viewer link between 2 entities - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @addViewer @negative
  Scenario Outline: Try to Add a new viewer link with invalid viewerUUID [<value>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    And I set the request fields
      | viewerUUID | <value> |
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    And the field {data} has the value {<data>}
    Examples:
      | value                                | status                                   | code | data                                                           |
      |                                      | There was an error with the input fields | 400  | There was an error with the input fields: viewUUID is required |
      | 9e3d1f32-8fdb-11e8-9eb6-529269fb1459 | Access denied                            | 401  | Can't get entity viewerUUID                                    |

  @addViewer @negative @bug-rest-58
  Scenario Outline: Try to Add a new viewer link with invalid entityUUID [<value>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Then I add a new viewer
    Given I clear the request body
    And I set the request fields
      | entityUUID | <value> |
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    And the field {data} has the value {<data>}
    Examples:
      | value | status                                   | code | data                                                             |
      |       | There was an error with the input fields | 400  | There was an error with the input fields: entityUUID is required |
    #  | 9e3d1f32-8fdb-11e8-9eb6-529269fb1459 | Error adding viewer                      | 400  | Error adding a relation: could not find entity                   |

  @addViewer @entity
  Scenario: Get an entity that is being viewed - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].viewed} is containing the value stored in DataContainer with key {secondPassportUUID}

  @addViewer @entity
  Scenario: Get an entity that can view another - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Create a new second passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].viewers} is containing the value stored in DataContainer with key {secondPassportUUID}