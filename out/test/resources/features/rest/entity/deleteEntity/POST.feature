#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/deleteEntity
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @all @wip
Feature: VIAM - entity - deleteEntity POST
  Delete an entity

  Background:
    Given we are testing the VIAM Api

  @deleteEntity
  Scenario Outline: Create a new child entity with type [<type>] and then delete it  - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add a child entity
    Given I clear the request body
    Then I create a new entity with type {<type>} via VIAM API
    And the field {status} has the value {OK}
#Delete the child entity
    Given I clear the request body
    Then I delete the current guarded entity via VIAM API
    And the field {status} has the value {OK}
    Examples:
      | type |
      | 1    |
      | 2    |
      | 3    |
      | 4    |

  @deleteEntity @bug-rest-33 @wip
  Scenario: Delete a new parent entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityuuid}
#Try to Delete the entity
    Given I clear the request body
    And I load object with key {entityuuid} from DataContainer into currentRequest Body with key {entityuuid}
    Then I delete an entity via VIAM API
    And the field {status} has the value {Could not find entity:}
    And the field {code} has the value {400}

  @deleteEntity @negative
  Scenario: Try to delete an child entity of another entity - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add a guarded entity by creating one
    Given I clear the request body
    Then I create a new entity with type {1} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {createdEntityUuid}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the second member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Try to Delete the child entity of the first user
    Given I clear the request body
    And I load object with key {createdEntityUuid} from DataContainer into currentRequest Body with key {entityuuid}
    Then I delete an entity via VIAM API
    And the field {status} has the value {Access denied}
    And the field {code} has the value {401}
    And the field {data} has the value {Access denied to entity entityuuid}
