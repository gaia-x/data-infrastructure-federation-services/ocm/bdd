#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/getEntity
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @all
Feature: VIAM - entity - getEntity POST
  Get the data of an entity

  Background:
    Given we are testing the VIAM Api

  @entity
  Scenario: Create a new entity and get his data with filter - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    And I set the request fields
      | from | -1 |
      | to   | -1 |
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}

  @entity
  Scenario: Create a new entity and get his data - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
    And I assert that the entity data matches the data of the current member via VIAM API

  @entity @bug-rest-215
  Scenario Outline: Create a new entity with Type [<type>] and then Get it- Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity
    Given I clear the request body
    Then I create a new entity with type {<type>} via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I get the guarded entity via the VIAM API
    And the field {status} has the value {OK}
    Examples:
      | type |
      | 1    |
      | 2    |
      | 3    |
      | 4    |
      | 5    |
      | 6    |

  @entity @negative
  Scenario Outline: Try to get an entity with invalid data - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    And I set the request fields
      | from       | <from>       |
      | to         | <to>         |
      | entityuuid | <entityuuid> |
    Then I get an entity via the VIAM API
    And the field {status} has the value {There was an error with the input fields}
    Examples:
      | from | to | entityuuid |
      |      |    |            |

  @entity @negative
  Scenario: Try to Get the data of another user - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {UserUUID_1}
#Create a second member
    Given I clear the request body
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the second member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Get the data of the first entity
    Given I clear the request body
    Then I load object with key {UserUUID_1} from DataContainer into currentRequest Body with key {entityuuid}
    Then I get an entity via the VIAM API
    And the field {status} has the value {Access denied}
    And the field {code} has the value {401}
    And the field {data} has the value {Access denied to entity entityuuid}

  @entity @negative
  Scenario Outline: Try to Get the data of an entity with invalid authorization [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    And I delete the headers
      | <header> |
    Then I get the current entity via the VIAM API
    And the field {status} has the value {No authentication values}
    Examples:
      | header |
      | token  |
      | uuid   |

  @addClaim @entity
  Scenario Outline: Add a new claim to an entity and then Get the entity - [<profile>]- Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {<profile>}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Get the entity and verify the new claim is added
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {$..age..value.value} has the value {["23"]}
    #bug-173 And I assert that the entity data matches the data of the current member via VIAM API
    Examples:
      | profile             |
      | create_private      |
      | create_invitational |

  @deleteClaim @entity
  Scenario Outline: Delete a new claim to an entity and then Get the entity to verify its really deleted - [<profile>]- Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Add new claim
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {<profile>}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Delete the new claim
    Given I clear the request body
    Then I delete a claim {age} with tag {ageValue} of the current member via VIAM API
    And the field {status} has the value {OK}
#Get the entity and verify the new claim is added
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {$..age..value.value} is not containing the value {["23"]}
    #bug-173 And I assert that the entity data matches the data of the current member via VIAM API
    Examples:
      | profile             |
      | create_private      |
      | create_invitational |

  @createEntity @guardian @bug-rest-39
  Scenario: Create entity by calling /entity/createEntity then get it and verify its guarded - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new guarded entity by calling /entity/createEntity
    Given I clear the request body
    Then I create a new entity with type {1} via VIAM API
    And the field {status} has the value {OK}
#Get the data of current main entity
    Given I clear the request body
    Then I get the current entity via the VIAM API
    And the field {status} has the value {OK}

  @entity @negative
  Scenario: Try to get an entity with invalid from & to filter - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUuid}
#Get the data of current entity
    Given I clear the request body
    And I set the request fields
      | from | 10 |
      | to   | 12 |
    Then I get the current entity via the VIAM API
    And the value of field {status} should contain the text {Could not find entity:}
    And the field {status} is containing the value stored in DataContainer with key {entityUuid}
    And the field {code} has the value {400}

  @child @entity
  Scenario: Get a child entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a new child to the new entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {childUUID}
    Then I create new child link via VIAP API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityuuid}
    And I get an entity via the VIAM API
    And the field {status} has the value {OK}
#    And Verify the response value with the value in the datacontainer
#      | @.data[0].parents[0] | EntityUUID |
    And the field {@.data[0].parents[0]} has the value stored in DataContainer with key {}

  @removeChild
  Scenario: Remove a child link of an entity and then Get the entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a new child to the new entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {childUUID}
    Then I create new child link via VIAP API
    And the field {status} has the value {OK}
#Then I remove the child link
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {childUUID}
    Then I remove a child link via VIAP API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityuuid}
    And I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {$.data[0].parents} contains the value {=1}

  @parent @entity
  Scenario: Get Parent entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a parent link
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {parentUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I create new parent link via VIAP API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the array {$.data[0].children} contains the value stored in DataContainer with key {PassportUUID}

  @removeParent
  Scenario: Remove a Parent link from an entity and then Get the entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a parent link
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {parentUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I create new parent link via VIAP API
    And the field {status} has the value {OK}
#Then I remove a parent link
    Then I remove a parent link from an entity via VIAP API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].children} contains the value {=1}

  @guarded @entity
  Scenario: Get a guardian entity - Positive
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the array {@.data[0].guarded} contains the value stored in DataContainer with key {EntityUUID}

  @removeGuardian
  Scenario: Remove a guardian from an child entity and then Get it - Positive
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Remove the Guardian
    Then I remove a guardian via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity and verify guarded is not present
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].guarded} contains the value {=1}

  @guardian @entity
  Scenario: Get a guarded entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport to the user
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a new guarded link to the first entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {guardedUUID}
    Then I create a new guarded link via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the array {@.data[0].guardians} contains the value stored in DataContainer with key {PassportUUID}

  @removeGuarded @entity
  Scenario: Remove a guarded link from an entity and then get it - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport to the user
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a new guarded link to the first entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {guardedUUID}
    Then I create a new guarded link via VIAM API
    And the field {status} has the value {OK}
#Remove the guarded link
    Then I remove a guarded link via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].guardians} contains the value {=1}

  @member @entity @wip
  Scenario: Get memberOf entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I add a new memberOf link to the new entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {memberOfUUID}
    Then I create new memberOf link via VIAP API
    And the field {status} has the value {OK}
#Get memberOf entity
    Given I clear the request body
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And Verify the response value with the value in the datacontainer
      | @.data[0].members[0] | member |

  @member @entity
  Scenario: Remove a member link and then get the organisation - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I create member link
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {memberUUID}
    Then I create new member link via VIAP API
    And the field {status} has the value {OK}
#Then I remove a member link
    Then I remove a member link from an entity via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].members} contains the value {=1}

  @memberOf @entity
  Scenario: Get member entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I add a new child to the new entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {memberOfUUID}
    Then I create new memberOf link via VIAP API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the array {@.data[0].memberOf} contains the value stored in DataContainer with key {firstEntityUUID}

  @member @entity
  Scenario: Remove memberOf link and the Get the member - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I add a new child to the new entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {memberOfUUID}
    Then I create new memberOf link via VIAP API
    And the field {status} has the value {OK}
#Then I remove a memberOf link
    Then I remove a memberOf link from an entity via VIAM API
    And the field {status} has the value {OK}
#Get the data of current entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I get an entity via the VIAM API
    And the field {status} has the value {OK}
    And the field {@.data[0].memberOf} contains the value {=1}