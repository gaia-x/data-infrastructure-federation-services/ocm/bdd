#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/addParent
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @parent @all
Feature: VIAM - entity - addParent POST
  Add a new parent link between entities

  Background:
    Given we are testing the VIAM Api


  @addParent
  Scenario: Add Parent link to an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a parent link
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {parentUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I create new parent link via VIAP API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @addParent @negative @wip
  Scenario: Try to add Parent link to an entity a second time - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a parent link
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {parentUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I create new parent link via VIAP API
    And the field {status} has the value {OK}
#Try to create a the same link a second time
    Then I create new parent link via VIAP API
    And the field {status} has the value {Some error}

  @addParent @negative
  Scenario Outline: Try to Add Parent link to an entity with invalid params [<profile>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Then I add a parent link
    Given I clear the request body
    And I load the REST request {Parent.json} with profile {<profile>}
    Then I create new parent link via VIAP API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile                 | status                                   | data                                                             |
      | missing_parent          | There was an error with the input fields | There was an error with the input fields: parentUUID is required |
      | empty_parent            | There was an error with the input fields | There was an error with the input fields: parentUUID is required |
    #  | invalid_uuid_parent     | Error adding parent                      | Error adding opposite relation:Error adding a relation: could not find entity. |
      | missing_entityuuid      | There was an error with the input fields | There was an error with the input fields: entityUUID is required |
      | empty_entityuuid        | There was an error with the input fields | There was an error with the input fields: entityUUID is required |
    #  | invalid_uuid_entityuuid | Error adding parent                      | Error adding a relation: could not find entity                   |
    #  | non_existing_uuid       | Error adding parent                      | Error adding a relation: could not find entity                   |