#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/addMemberOf
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @memberOf @all
Feature: VIAM - entity - addMemberOf POST
  Add a new memberOf link between entities

  Background:
    Given we are testing the VIAM Api

  @addMemberOf
  Scenario: Add memberOf link to an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I add a new memberOf link to the new entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {memberOfUUID}
    Then I create new memberOf link via VIAP API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @addMemberOf @negative @bug-rest-45 @wip
  Scenario: Try to get add memberOf link to an entity a second time - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {member}
#Then I add a new memberof link to the new entity
    Given I clear the request body
    Then I load object with key {member} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {memberOfUUID}
    Then I create new memberOf link via VIAP API
    And the field {status} has the value {OK}
#Try to add the same memberOf link a second time
    Then I create new memberOf link via VIAP API
#    And the field {status} has the value {Some error}
#    And the field {code} has the value {400}
    #Get the data of current entity
    Given I clear the request body
    Then I get the current entity via the VIAM API

  @addMemberOf @negative
  Scenario Outline: Try to add a memberOf link to an entity with invalid params [<profile>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
#Then I add a new memberOf link to the new entity
    Given I clear the request body
    And I load the REST request {MemberOf.json} with profile {<profile>}
    Then I create new memberOf link via VIAP API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile                 | status                                   | data                                                               |
      | missing_memberOf        | There was an error with the input fields | There was an error with the input fields: memberOfUUID is required |
      | empty_memberOf          | There was an error with the input fields | There was an error with the input fields: memberOfUUID is required |
     # | invalid_uuid_memberOf   | Error adding memberOf                    | Error adding opposite relation:Error adding a relation: could not find entity. |
      | missing_entityuuid      | There was an error with the input fields | There was an error with the input fields: entityUUID is required   |
      | empty_entityuuid        | There was an error with the input fields | There was an error with the input fields: entityUUID is required   |
      | invalid_uuid_entityuuid | Error adding memberOf                    | Error adding a relation: could not find entity                     |
      | non_existing_uuid       | Error adding memberOf                    | Error adding a relation: could not find entity                     |


