#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/addGuardian
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @guardian @bug-rest-32 @all
Feature: VIAM - entity - deleteClaim POST
  Add a new guarding to an entity

  Background:
    Given we are testing the VIAM Api

  @addGuardian
  Scenario: Add a new Guardian to a main entity - Positive
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @addGuardian
  Scenario: Add a Guardian link to an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @addGuardian @negative @wip
  Scenario: Try to make an entity be guardian of it self - Negative
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {uuid}
#Try to add guardian on its self
    Given I clear the request body
    Then I load object with key {uuid} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {uuid} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {Some error}
    And the field {code} has the value {400}

  @addGuardian @negative
  Scenario Outline: Try to Add a Guardian with an invalid params [<profile>]- Negative
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Try to add a guardian
    Given I clear the request body
    Given I load the REST request {Guardian.json} with profile {<profile>}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {There was an error with the input fields}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | profile            | data                                                              |
      | missing_guardian   | There was an error with the input fields: guardianUUID is required |
      | empty_guardian     | There was an error with the input fields: guardianUUID is required |
 #     | invalid_uuid_guardian   | There was an error with the input fields: guardianUUID is required   |
      | missing_entityuuid | There was an error with the input fields: entityUUID is required   |
      | empty_entityuuid   | There was an error with the input fields: entityUUID is required   |
#      | invalid_uuid_entityuuid | There was an error with the input fields: entityUUIDis required |

  @addGuardian @negative
  Scenario Outline: Try to add a guardian with missing auth header [<header>] - Negative
#Create a new member that will be the guardian
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member to get the data
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {guardianUuid}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a guardian on this entity
    Given I clear the request body
    Then I load object with key {guardianUuid} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    And I delete the headers
      | <header> |
    Then I add a new guardian via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header |
      | token  |
      | uuid   |