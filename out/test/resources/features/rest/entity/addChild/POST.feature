#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/entity/addChild
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @child @all
Feature: VIAM - entity - addChild POST
  Add a new child link between entities

  Background:
    Given we are testing the VIAM Api

  @addChild
  Scenario: Add child link to an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a new child to the new entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {childUUID}
    Then I create new child link via VIAP API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {ok }

  @addChild @negative @bug-rest-45 @wip
  Scenario: Try to add the same link a second time - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a new child to the new entity
    Given I clear the request body
    Then I load object with key {EntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I load object with key {PassportUUID} from DataContainer into currentRequest Body with key {childUUID}
    Then I create new child link via VIAP API
    And the field {status} has the value {OK}
#Try to add it a second time
    Then I create new child link via VIAP API
    And the field {status} has the value {Some error}
    And the field {code} has the value {400}
    And the field {data} has the value {Some error}

  @addChild @negative
  Scenario Outline: Try to add a child link to an entity with invalid params [<profile>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login that member to get the uuid
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {EntityUUID}
#Create a new passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {PassportUUID}
#Then I add a new child to the new entity
    Given I clear the request body
    And I load the REST request {Child.json} with profile {<profile>}
    Then I create new child link via VIAP API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    And the field {data} has the value {<data>}
    Examples:
      | profile                 | status                                   | code | data                                                             |
      | missing_child           | There was an error with the input fields | 400  | There was an error with the input fields: childUUID is required  |
      | empty_child             | There was an error with the input fields | 400  | There was an error with the input fields: childUUID is required  |
      | invalid_uuid_child      | Access denied                            | 401  | Can't get entity childUUID                                       |
      | missing_entityuuid      | There was an error with the input fields | 400  | There was an error with the input fields: entityUUID is required |
      | empty_entityuuid        | There was an error with the input fields | 400  | There was an error with the input fields: entityUUID is required |
      | invalid_uuid_entityuuid | Access denied                            | 401  | Can't get entity childUUID                                       |
      | non_existing_uuid       | Access denied                            | 401  | Can't get entity childUUID                                       |
