#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/action/getActions
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @action @all
Feature: VIAM - action - getActions POST
  Get all actions for given entity or device key

  Background:
    Given we are testing the VIAM Api

  @getActions
  Scenario: Get all the actions of the current entity that has unconfirmed new device - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {publicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUuid}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.QrCode} from the last response and store it in the DataContainer with key {qrCode}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Get the actions
    Given I get the actions with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].status} has the value {1}
    And the field {$.data[0].type} has the value {1}
    And the field {$.data[0].functionName} has the value {ConfirmDevice}
    And the field {$.data[0].createdByEntityUUID} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].confirmedByEntityUUId} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].appliedOnEntityUUID} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].qrCode} has the value stored in DataContainer with key {qrCode}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId}
    And the field {$.data[0].authenticationPublicKey} has the value stored in DataContainer with key {publicKey}

  @getActions
  Scenario: Get an confirmed action for adding a new device - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
    And I get the value of {$.data.QrCode} from the last response and store it in the DataContainer with key {qrCode}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Succesfully executed}
#Get the actions
    Given I clear the request body
    Given I get the actions with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].status} has the value {4}
    And the field {$.data[0].type} has the value {1}
    And the field {$.data[0].functionName} has the value {ConfirmComputerDevice}
    And the field {$.data[0].createdByEntityUUID} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].confirmedByEntityUUId} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].appliedOnEntityUUID} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].qrCode} has the value stored in DataContainer with key {qrCode}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId}
    And the field {$.data[0].authenticationPublicKey} has the value stored in DataContainer with key {secondPublicKey}

  @getActions
  Scenario: Try to Get all the actions of the current publicKey when he doesnt have any - Positive
    #Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
#Get the actions
    Given I get the actions with mode devicekey and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {[]}

  @getActions
  Scenario: Get canceled actions of an entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {publicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUuid}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.QrCode} from the last response and store it in the DataContainer with key {qrCode}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Then I cancel the action
    Then I cancel the current action via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Get the actions for the current entity
    And I clear the request body
    Given I get the actions with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].status} has the value {5}
    And the field {$.data[0].type} has the value {1}
    And the field {$.data[0].functionName} has the value {ConfirmDevice}
    And the field {$.data[0].createdByEntityUUID} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].confirmedByEntityUUId} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].appliedOnEntityUUID} has the value stored in DataContainer with key {entityUuid}
    And the field {$.data[0].qrCode} has the value stored in DataContainer with key {qrCode}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId}
    And the field {$.data[0].authenticationPublicKey} has the value stored in DataContainer with key {publicKey}

  @getActions @negative
  Scenario Outline: Try to Get actions with invalid from [<from>] and to[<to>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {publicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUuid}
#Add new device to the current user
    Given I set the following request body {{}}
    Then I add a new device via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.QrCode} from the last response and store it in the DataContainer with key {qrCode}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Get the actions
    Given I get the actions with mode entity and from {<from>} and to {<to>} filters via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | from | to   | status                                   | data                                                       |
      |      | +inf | There was an error with the input fields | There was an error with the input fields: From is required |
      | 0    |      | There was an error with the input fields | There was an error with the input fields: To is required   |
      | asd  | +inf | Error during getting actions             | ERR min or max is not a float                              |
      | 0    | asd  | Error during getting actions             | ERR min or max is not a float                              |