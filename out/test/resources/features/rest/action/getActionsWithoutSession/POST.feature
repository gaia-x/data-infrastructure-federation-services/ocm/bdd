#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/action/getActionsWithoutSession
#Author: Rosen Georgiev rosen.georgiev@vereign.com
#Spec: https://code.vereign.com/code/restful-api/blob/master/docs/actionGetActionsWithoutSession.md

@rest @action @all
Feature: VIAM - action - getActionsWithoutSession POST
  Get all actions for given anonymous device key

  Background:
    Given we are testing the VIAM Api

  @getActionWithoutSession
  Scenario: Get an login action without a session - Positive
    Given I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.QrCode} from the last response and store it in the DataContainer with key {qrCode}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {publicKey}
#Get the actions
    And I clear the request body
    Given I get the actions without session for a devicekey and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data[0].status} has the value {1}
    And the field {$.data[0].type} has the value {1}
    And the field {$.data[0].functionName} has the value {ConfirmComputerDevice}
    And the field {$.data[0].qrCode} has the value stored in DataContainer with key {qrCode}
    And the field {$.data[0].actionID} has the value stored in DataContainer with key {actionId}
    And the field {$.data[0].authenticationPublicKey} has the value stored in DataContainer with key {publicKey}

  @getActionWithoutSession @negative
  Scenario Outline: Try to Get an action without a session with invalid from [<from>] and to [<to>] filters - Negative
    Given I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
#Get the actions
    Then I get the actions without session for a devicekey and from {<from>} and to {<to>} filters via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | from | to   | status                                   | data                                                       |
      |      | +inf | There was an error with the input fields | There was an error with the input fields: From is required |
      | 0    |      | There was an error with the input fields | There was an error with the input fields: To is required   |

  @getActionWithoutSession @negative
  Scenario: Try to Get an action without a session without publicKey - Negative
    Then I get the actions without session for a devicekey and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {Not provided public key}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}