#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/document/listDocuments
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @document @all
Feature: VIAM - document - listDocuments POST
  This endpoint is for getting all documents in container documents

  Background:
    Given we are testing the VIAM Api

  @listDocuments
  Scenario: List all the documents a member has - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUUID}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
#List the documents that the entity has
    Given I get the list of all documents the logged member has via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data} contains {2} elements

  @listDocuments
  Scenario: Update a document and then Get the list of the documents for that entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | testpdf1.pdf    |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Get the document
    Given I get the file info of the current document via VIAM API
    And the field {status} has the value {OK}
#Put document
    Then I delete the headers
      | path        |
    Then I update the current document with pdf {testpdf.pdf} via VIAM API
    And the field {status} has the value {OK}
#List the documents that the entity has
    Given I get the list of all documents the logged member has via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @listDocuments
  Scenario: Update a document and then Get the list of the documents for that entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | testpdf1.pdf    |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Put document
    Then I delete the headers
      | path        |
    Then I update the current document with pdf {testpdf.pdf} via VIAM API
    And the field {status} has the value {OK}
#Put another document version
    And I set the headers
      | contenttype | text/plain |
    Then I update the current document with pdf {test.txt} via VIAM API
    And the field {status} has the value {OK}
#List the documents that the entity has
    Given I get the list of all documents the logged member has via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @listDocuments @negative
  Scenario Outline: Try to List all the documents an entity has without providing required header [<haeder>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {entityUUID}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
#List the documents that the entity has
    And I delete the headers
      | <header> |
    Given I get the list of all documents the logged member has via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    Examples:
      | header       | status                    | code |
      | token        | No authentication values  | 400  |
      | uuid         | No authentication values  | 400  |
      | passportuuid | No passport UUID provided | 401  |