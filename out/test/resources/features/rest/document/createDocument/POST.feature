#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/document/createDocument
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @document @all
Feature: VIAM - document - createDocument POST
  This endpoint is for creating new file entry.

  Background:
    Given we are testing the VIAM Api

  @createDocument
  Scenario: Create a new document - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @createDocument @negative @bug-rest-324 @wip
  Scenario: Try to Create a new document without providing a passportuuid header - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
    Then I create a new document via VIAM API
    And the field {status} has the value {No passport UUID provided}
    And the field {code} has the value {401}

  @createDocument @negative @bug-rest-323
  Scenario Outline: Try to Create a new document with missing required header [<header>]- Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
    And I delete the headers
      | <header> |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    Examples:
      | header      | status                   | code |
      | token       | No authentication values | 400  |
      | uuid        | No authentication values | 400  |
  # @bug-rest-323    | contenttype | No content type provided | 401  |
      | path        | No path provided         | 401  |

  @createDocument @negative
  Scenario: Try to Create a new document with invalid passportuuid - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | 123456.pdf      |
      | passportuuid | b55f283c-919c-11e8-9eb6-529269fb1459 |
    Then I create a new document via VIAM API
    And the field {status} has the value {Cannot get passport entity}
    And the field {code} has the value {401}