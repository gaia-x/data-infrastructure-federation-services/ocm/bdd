#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/document/putDocument
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @document @all
Feature: VIAM - document - putDocument POST
  This endpoint is for putting new file version for file entry.

  Background:
    Given we are testing the VIAM Api

  @putDocument
  Scenario: Putting a new document file version - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | testpdf.pdf     |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Put document
    Then I delete the headers
      | path        |
    Then I update the current document with pdf {testpdf.pdf} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @putDocument @negative
  Scenario Outline: Try to Update document version with missing Auth headers [<header>]- Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | testpdf.pdf     |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Put document
    Then I delete the headers
      | path        |
    And I delete the headers
      | <header> |
    Then I update the current document with pdf {testpdf.pdf} via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {<code>}
    Examples:
      | header | status                   | code |
      | token  | No authentication values | 400  |
      | uuid   | No authentication values | 400  |
      | passportuuid | No passport UUID provided | 401  |

  @putDocument @negative
  Scenario Outline: Try to Update document version with invalid resourceid [<resourseid>]- Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | testpdf.pdf     |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Put document
    Then I delete the headers
      | path        |
    Then I update a document with resourceid {<resourseid>} and pdf {testpdf.pdf} via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {401}
    Examples:
      | resourseid                           | status                  |
      |                                      | No resource ID provided |
      | f8379b88-88ed-11e8-a812-a6cf71072f73 | Resource does not exist |
      | f8379b88                             | Resource does not exist |

  @putDocument @negative @wip
  Scenario: Try Updating a new document file version without providing a file - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | testpdf.pdf     |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Put document
    Then I delete the headers
      | path        |
    Then I try to update the current document without providing pdf via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @putDocument
  Scenario: Update a file version with txt file - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Create new document
    Given I clear the request body
    And I set the headers
      | contenttype | application/pdf |
      | path        | testpdf.pdf     |
    Then I create a new document with the current passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Put document
    Then I delete the headers
      | path        |
    And I set the headers
      | contenttype | text/plain |
    Then I update the current document with pdf {test.txt} via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

