#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/hyperledger/getInfo
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @hyperledger @all @wip
Feature: VIAM - hyperledger - getInfo POST
  This endpoint is for getting the HyperLedger blockchain information.

  Background:
    Given we are testing the VIAM Api

  @getInfo
  Scenario: Get the current hyperledger blockchain info - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I get the current blockchain info
    Given I set the following request body {{}}
    Then I get the hyperledger blockchain info via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {$.data.err} has the value {null}
    And the response is valid according to the {Hyperledger_getinfo_schema.json} REST schema
    And the field {$.data.channelname} has the value {defaultchannel}
    And the field {$.data.nbBlocks} is present and not empty

  @getInfo @negative
  Scenario: Try to Get the current hyperledger blockchain info without authentication - Positive
#Then I get the current blockchain info
    Given I set the following request body {{}}
    Then I get the hyperledger blockchain info via VIAM API
    And the field {status} has the value {Not provided public key}
    And the field {code} has the value {400}
    And the field {$.data} has the value {{}}

