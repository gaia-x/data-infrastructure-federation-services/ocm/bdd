#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/passport/linkClaim
#spec: https://code.vereign.com/code/restful-api/blob/master/docs/passportLinkClaim.md
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @passport @all
Feature: VIAM - passport - linkClaim POST
  Links an existing claim to a passport

  Background:
    Given we are testing the VIAM Api

  @linkClaim @bug-rest-317 @wip
  Scenario: Create a new passport and link it to a claim - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Then I link the passport entity to the new claim
    Given I clear the request body
    Then I link the current entity claim {age} with tag {ageValue} to the passport entity via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}

  @linkClaim @negative
  Scenario Outline: Try to create a link to a passport with non existing claim [<claim>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Then I link the passport entity to the new claim
    Given I clear the request body
    Then I link the current entity claim {<claim>} with tag {ageValue} to the passport entity via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | claim  | status                                   | data                                                            |
      |        | There was an error with the input fields | There was an error with the input fields: claimName is required |
      | degree | The entity does not have such claim      | {}                                                              |

  @linkClaim @negative
  Scenario Outline: Try to Create a new claim link to a passport with missing auth header [<header>] - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Then I link the passport entity to the new claim
    Given I clear the request body
    And I delete the headers
      | <header> |
    Then I link the current entity claim {age} with tag {ageValue} to the passport entity via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header |
      | token  |
      | uuid   |
