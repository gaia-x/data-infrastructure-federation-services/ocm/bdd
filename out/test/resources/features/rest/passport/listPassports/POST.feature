#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#/passport/listPassports
#spec: https://code.vereign.com/code/restful-api/blob/master/docs/passportLinkClaim.md
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @entity @passport @all
Feature: VIAM - passport - listPassports POST
  List all passports to an entity

  Background:
    Given we are testing the VIAM Api

  @listPassports @bug-rest-317 @wip
  Scenario: List all passports to the logged entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {guardianUuid}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstMemberPublicKey}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {passportUuid}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Then I link the passport entity to the new claim
    Given I clear the request body
    Then I link the current entity claim {age} with tag {ageValue} to the passport entity via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Login the first user to get his passport
    Given I clear the request body
    Then I load object with key {firstMemberPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I call listPassports
    And I set the following request body {{}}
    Then I get the list of all passports of the logged user via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} contains {4} element
    And the array {$..uuid} contains the value stored in DataContainer with key {passportUuid}
    And the field {$..ageValue.value.value} has the value {["23"]}
    And the array {$.data[0].guardians[0]} contains the value stored in DataContainer with key {guardianUuid}

  @listPassports
  Scenario: Get passports of an entity that has only default Passports - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I call listPassports
    Given I clear the request body
    And I set the following request body {{}}
    Then I get the list of all passports of the logged user via VIAM API
    And the field {status} has the value {OK}
    And the field {$.data} contains {3} elements
    And the field {$..passportName} contains the value {Social}
    And the field {$..passportName} contains the value {Friends}
    And the field {$..passportName} contains the value {Email}

  @listPassports
  Scenario: List all passports that are not linked to a claim - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {guardianUuid}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {passportUuid}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I call listPassports
    Given I clear the request body
    And I set the following request body {{}}
    Then I get the list of all passports of the logged user via VIAM API
    And the field {status} has the value {OK}
    And the array {$..uuid} contains the value stored in DataContainer with key {passportUuid}
    And the array {$.data[0].guardians} contains the value stored in DataContainer with key {guardianUuid}

  @negative @listPassports
  Scenario: Try to List the passports of another member - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstMemberUuid}
    And I get the value of {$.data.Session} from the last response and store it in the DataContainer with key {firstMemberSession}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Create a second member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I call listPassports
    Given I clear the request body
    Then I load object with key {firstMemberUuid} from DataContainer into currentRequest HEADER {uuid}
    Then I load object with key {firstMemberSession} from DataContainer into currentRequest HEADER {token}
    And I set the following request body {{}}
    Then I get the list of all passports of the logged user via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}

  @listPassports @negative
  Scenario Outline: Try to List all passports with missing auth headers [<header>]- Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I set random value with UUID format to field {uuid} inside Request Body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I call listPassports
    Given I clear the request body
    And I delete the headers
      | <header> |
    And I set the following request body {{}}
    Then I get the list of all passports of the logged user via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    Examples:
      | header |
      | token  |
      | uuid   |