#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/passport/listFunctions
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @hyperledger @passport @all @wip @bug-passport-8
Feature: VIAM - passport - listFunctions POST
  This endpoint is for getting functions list in the passport-generation-agent

  Background:
    Given we are testing the VIAM Api

  @listFunctions
  Scenario: List Functions from the passport-generation-agent - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I call list Functions
    Given I set the following request body {{}}
    Then I get the functions list from the passport generation agent via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the response is valid according to the {ListFunctions_schema.json} REST schema
    And the field {$..Name} contains the value {FormatAddress}
    And the field {$..Name} contains the value {FormatName}
    And the field {$..Name} contains the value {Identity}

