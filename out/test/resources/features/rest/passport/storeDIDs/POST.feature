#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/passport/storeDIDs
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@rest @hyperledger @passport @all @wip
Feature: VIAM - passport - storeDIDs POST
  This endpoint is for generating DIDs record and storing it in the HyperLedger-agent.

  Background:
    Given we are testing the VIAM Api

  @storeDids
  Scenario: Generate and Store DIDs in the hyperledger agent - Positive
    Given I set the headers
      | publicKey | lQ6eTBloJINOxKoOfFjvtO0qP4NFsD8sOHIgI+/qhafaY2HmzRAufu2iUSh0OcuxDVwqbkc1ztwCl1CR2JPE6owmtaR9+6Ku+D1Jh7VvbljlkiX9+0A5CXdTlhV7bwK7yD12D+P0EYflwQqxplcotmWyr1HQxW+VQVr/VjXA2Q3AbCsS27m3n3nSE7YH2tw+QN6Ix71ozI2RXQ9rQHIBHpwCLDAz/n+VuI2MaYfAlD5lc/qXUxDVzzyl8idU3b39X3bBcg0Q0v+qt+gGYYBc3pJjSmqV6EfU6uFLfj80/G7qGSCOKy8aSnRbpMEr4NZre5KN92SG3bBCBETMFZlEFQ== |
    Then I login member with mode previousaddeddevice via VIAM API
#Then I store the DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | 704cc39a-b2db-4df2-a87e-3829997e9dcb |
    Then I store the DID for a passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the value of field {data} should contain the text {-----BEGIN CERTIFICATE-----}

  @storeDids @negative
  Scenario: Try to Generate and Store DIDs in the hyperledger agent without authentication - Negative
#Then I store the DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | 704cc39a-b2db-4df2-a87e-3829997e9dcb |
    Then I store the DID for a passport via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    And the value of field {data} should contain the text {{}}

  @storeDids @negative @bug-rest-106 @wip
  Scenario: Try to Store a DID with a member that is not a guardian/viewer ot that passport - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Then I try to store a DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | 704cc39a-b2db-4df2-a87e-3829997e9dcb |
    Then I store the DID for a passport via VIAM API
    And the field {status} has the value {No authentication values}
    And the field {code} has the value {400}
    And the field {data} has the value {{}}

  @storeDids @negative
  Scenario Outline: Try to Store DIDs in the hyperledger agent with invalid passportUUID [<value>] - Negative
    Given I set the headers
      | publicKey | lQ6eTBloJINOxKoOfFjvtO0qP4NFsD8sOHIgI+/qhafaY2HmzRAufu2iUSh0OcuxDVwqbkc1ztwCl1CR2JPE6owmtaR9+6Ku+D1Jh7VvbljlkiX9+0A5CXdTlhV7bwK7yD12D+P0EYflwQqxplcotmWyr1HQxW+VQVr/VjXA2Q3AbCsS27m3n3nSE7YH2tw+QN6Ix71ozI2RXQ9rQHIBHpwCLDAz/n+VuI2MaYfAlD5lc/qXUxDVzzyl8idU3b39X3bBcg0Q0v+qt+gGYYBc3pJjSmqV6EfU6uFLfj80/G7qGSCOKy8aSnRbpMEr4NZre5KN92SG3bBCBETMFZlEFQ== |
    Then I login member with mode previousaddeddevice via VIAM API
#Then I store the DID
    Given I clear the request body
    Then I set the request fields
      | passportUUID | <value> |
    Then I store the DID for a passport via VIAM API
    And the field {status} has the value {<status>}
    And the field {code} has the value {400}
    And the field {data} has the value {<data>}
    Examples:
      | value                                | status                                                                                            | data                                                               |
      |                                      | There was an error with the input fields                                                          | There was an error with the input fields: passportUUID is required |
      | d37388e2-73ce-4d59-8512-fda5f0168f35 | can't generate DID:Error retrieving entity d37388e2-73ce-4d59-8512-fda5f0168f35: response is null | {}                                                                 |
      | d37388e2-73ce-4d59-8512              | can't generate DID:Error retrieving entity d37388e2-73ce-4d59-8512: response is null              | {}                                                                 |

#This test is only in case the record of the passport was deleted. To generate new one with all the required data
  @wip
  Scenario: Create new Passport with claims and then generate and store DID of it - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {create_private}
    Then I add a new claim to the current entity via VIAM API
    And the field {status} has the value {OK}
#Then I link the claim to the passport
    Given I clear the request body
    Then I link the current entity claim {emails} with tag {registration} to the passport entity via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Then I add a function to the current claim passport
    Given I clear the request body
    Then I attach function {Identity} to the current claim {emails} with tag {registration} passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}