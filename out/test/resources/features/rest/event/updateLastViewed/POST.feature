#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/event/updateLastViewed
#Author: Rosen Georgiev rosen.georgiev@vereign.com
#Spec: https://code.vereign.com/code/restful-api/blob/master/docs/eventUpdateLastViewed.md

@rest @event @all
Feature: VIAM - event - updateLastViewed POST
  Updates the last viewed timestamp. This method is used to tell the server that client for
  given entity or user device has viewed and shown all the events for that mode.

  Background:
    Given we are testing the VIAM Api

  @updateLastViewed
  Scenario: Update the last viewed timestamp of the current entity - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#Get the events
    And I clear the request body
    Given I get the new events with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data[0].stamp} from the last response and store it in the DataContainer with key {eventStamp}
#I update the event viewed timestamp
    Given I clear the request body
    And I load object with key {eventStamp} from DataContainer into currentRequest Body with key {lastViewed}
    Then I update the last viewed event with mode entity via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {Successfully updated}

  @updateLastViewed
  Scenario: Update the last viewed timestamp of the current devicyKey  - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {secondPublicKey}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#Then I list the devices
    Given I clear the request body
    Then I set the following request body {{}}
    Given I list the devices via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
#Then I authorize the new device with the first Device
    Given I clear the request body
    Given I authorize the current new device via VIAM API
    Then the field {status} has the value {OK}
    And the field {code} has the value {200}
#Get the events
    And I clear the request body
    Given I get the new events with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data[0].stamp} from the last response and store it in the DataContainer with key {eventStamp}
#I update the event viewed timestamp
    Given I clear the request body
    And I load object with key {eventStamp} from DataContainer into currentRequest Body with key {lastViewed}
    And I load object with key {secondPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Then I update the last viewed event with mode devicekey via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And the field {data} has the value {}