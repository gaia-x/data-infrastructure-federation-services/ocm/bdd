#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://localhost:3443/event/getNewEventsWithoutSession
#Author: Rosen Georgiev rosen.georgiev@vereign.com
#Spec: https://code.vereign.com/code/restful-api/blob/master/docs/eventGetNewEventsWithoutSession.md

@rest @event @all
Feature: VIAM - event - getNewEventsWithoutSession POST
  Returns all events from update time from UpdateLastViewedWithoutSession. This method works
  anynomously only for given public key, that is not attached to entity. It shows only events for last hour

  Background:
    Given we are testing the VIAM Api

  @getNewEventsWithoutSession
  Scenario: Get all the new events of the current devicyKey without session - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of the {publicKey} HEADER from the current request and store it in DataContainer using key {firstPublicKey}
#login with sms
    Given I clear the request body
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Login the new member
    Given I clear the request body
    Then I login member with mode newdevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.ActionID} from the last response and store it in the DataContainer with key {actionId}
#Confirm the new device
    Given I clear the request body
    And I load object with key {firstPublicKey} from DataContainer into currentRequest HEADER {publicKey}
    Given I confirm the current new device via VIAM API
    Then the field {status} has the value {OK}
#Get the events
    Given I clear the request body
    Then I delete the headers
      | uuid  |
      | token |
    Given I get the new events without session with devicyKey via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}