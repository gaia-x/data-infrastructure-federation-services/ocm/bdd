#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#WOPI http://localhost:8091
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@all
Feature: WebDav - PUT
  Updates a passport with a pdf file via WEBDAV

  Background:
    Given we are testing the VIAM Api

  @webdav
  Scenario: Update a passport by adding a pdf file via WebDav - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Update the current passport by adding a pdf
    Given we are testing with the current session the WebDav
    Then I update a passport by adding a pdf {testpdf.pdf} via WebDav
    And the status code should be {201}
    Then the response body contains {Created}

  @webdav @negative
  Scenario: Try to Update a passport by adding a non pdf file via WebDav - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Update the current passport by adding a pdf
    Given we are testing with the current session the WebDav
    Then I update a passport by adding a pdf {test.txt} via WebDav
    And the status code should be {415}
    Then the response body contains {Unsupported Media Type}

  @webdav @negative
  Scenario Outline: Try to Update a passport by adding a pdf file with missing auth header [<header>] via WebDav - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Update the current passport by adding a pdf
    Given we are testing with the current session the WebDav
    Then I delete the headers
      | <header> |
    Then I update a passport by adding a pdf {testpdf.pdf} via WebDav
    And the status code should be {401}
    Then the response body contains {Unauthorized}
    Examples:
      | header       |
      | uuid         |
      | token        |
      | publicKey    |
      | passportuuid |

  @webdav @negative @bug-webdav-3 @bug-webdav-4
  Scenario Outline: Try to Update a passport by adding a pdf file with invalid header [<header>] & [<value>] via WebDav - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {personUUID1}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Update the current passport by adding a pdf
    Given we are testing with the current session the WebDav
    Then I set the headers
      | <header> | <value> |
    Then I update a passport by adding a pdf {testpdf.pdf} via WebDav
    And the status code should be {401}
    Then the response body contains {Unauthorized}
    Examples:
      | header       | value |
      | uuid         |       |
      | token        |       |
      | publicKey    |       |
      | passportuuid |       |
 #bug-webdav-4      | uuid         | dsada |
      | token        | dsada |
#bug-webdav-4      | publicKey    | dsada |
#bug-webdav-3      | passportuuid | dsada |