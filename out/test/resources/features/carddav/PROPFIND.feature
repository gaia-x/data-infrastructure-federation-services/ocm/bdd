#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@carddav @propfind @all
Feature: CARDDAV - PROPFIND
  Checking the cards for a specific entity via CardDav

  Background:
    Given we are testing the VIAM Api

  @getCardDavPassports @bug-rest-317 @wip
  Scenario: Check the passports for specific entity via CardDav  - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.phonenumber} from the last Request Body and store it in the DataContainer with key {phonenumber}
#Login the new member
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data.Uuid} from the last response and store it in the DataContainer with key {firstEntityUUID}
    And I get the value of the {publicKey} HEADER from the last request and store it in DataContainer using key {publicKey}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {firstPassportUUID}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I link the passport entity to person Claim
    Given I clear the request body
    Then I link the current entity claim {phoneNumbers} with tag {registration} to the passport entity via VIAM API
    And the field {status} has the value {OK}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Then I add a function to the current claim passport
    Given I clear the request body
    Then I attach function {exists} to the current claim {phoneNumbers} with tag {registration} passport via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
#Create new credentials for the current entity
    Given I set the following request body {{}}
    Then I create new credentials for the current entity via VIAM API
    And the field {status} has the value {OK}
#I call the CardDav with the new credentials to trigger the process
    Given we are testing the CardDav
    Then I get all the passports for the current logged user via CardDav
    And the status code should be {401}
#Login the new member with previousaddeddevice mode
    Given I clear the request body
    Given we are testing the VIAM Api
    And I load object with key {publicKey} from DataContainer into currentRequest HEADER {publicKey}
    Then I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Get Action and QRcode
    Given I clear the request body
    Then I get the actions with mode entity and from {0} and to {+inf} filters via VIAM API
    And the field {status} has the value {OK}
    And the field {code} has the value {200}
    And I get the value of {$.data[0].qrCode} from the last response and store it in the DataContainer with key {qrCode}
    And I get the value of {$.data[0].actionID} from the last response and store it in the DataContainer with key {actionId}
#Then I confirm the new cardDav publicKey
    Given I clear the request body
    And I load object with key {publicKey} from DataContainer into currentRequest HEADER {publicKey}
    Then I load object with key {qrCode} from DataContainer into currentRequest Body with key {code}
    Then I load object with key {actionId} from DataContainer into currentRequest Body with key {actionID}
    Then I confirm CardDav publicKey via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
    And I get the value of {$.data} from the last response and store it in the DataContainer with key {secondPassportUUID}
#Then I make the new passport a guardian the entity
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {guardianUUID}
    Then I load object with key {firstEntityUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new guardian via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I add a new viewer
    Given I clear the request body
    Then I load object with key {secondPassportUUID} from DataContainer into currentRequest Body with key {viewerUUID}
    Then I load object with key {firstPassportUUID} from DataContainer into currentRequest Body with key {entityUUID}
    Then I add a new viewer link via VIAM API
    And the field {status} has the value {OK}
#Then I get the passports info via CardDav
    Given we are testing the CardDav
    Then I get all the passports for the current logged user via CardDav
    And the status code should be {207}
    And I validate the headers in the response with the following data
      | Content-Type | application/xml   |
      | Dav          | 1, 2, addressbook |

  @getCardDavPassports @negative
  Scenario: Try to get passport info via CardDav with invalid authorization - Negative
    Given we are testing the CardDav
    Then I set the headers
      | Authorization | Basic 2131231331 |
    Then I get all the passports via CardDav
    And the status code should be {401}
    And the response body contains {Unauthorized}