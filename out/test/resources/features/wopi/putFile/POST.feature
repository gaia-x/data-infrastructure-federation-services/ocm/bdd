#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#WOPI http://localhost:8787/putFile
#Author: Rosen Georgiev rosen.georgiev@vereign.com

@wopi @all
Feature: WOPI - putFile POST
  Attach a file to a passport via WOPI

  Background:
    Given we are testing the VIAM Api

  @putFile @bug-wopi-5 @bug-wopi-6
  Scenario Outline: Attach a file [<file>] to a passport via WOPI - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I get the passport via WOPI
    Given we are testing with the current session the WOPI
    Then I get all the passports with fileid {testFileId} via WOPI
    And the field {status} has the value {ok}
#Then I attach a file to the passport
    Then I clear ALL headers
    Given I attach a pdf file {<file>} to the current passport with fileid {testFileId} via WOPI
    Then the response body contains {Created}
    And the status code should be {201}
    Examples:
      | file        |
      | testpdf.pdf |
     # | testOdf.odt |
     # | testOOXML.docx |

  #You can add anything right now.
  @putFile @negative @wip
  Scenario Outline: Try to Attach a file [<file>] thats unsuported type via WOPI - Positive
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I get the passport via WOPI
    Given we are testing with the current session the WOPI
    Then I get all the passports with fileid {testFileId} via WOPI
    And the field {status} has the value {ok}
#Then I attach a file to the passport
    Then I clear ALL headers
    Given I attach a pdf file {<file>} to the current passport with fileid {testFileId} via WOPI
    And the status code should be {415}
    Then the response body contains {Unsupported Media Type}
    Examples:
      | file         |
      | apple_ex.png |

  @putFile @negative @wip
  Scenario Outline: Try to Attach a file to a passport with invalid fileId [<fileid>] via WOPI - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I get the passport via WOPI
    Given we are testing with the current session the WOPI
    Then I get all the passports with fileid {testFileId} via WOPI
    And the field {status} has the value {ok}
#Then I attach a file to the passport
    Then I clear ALL headers
    Given I attach a pdf file {testpdf.pdf} to the current passport with fileid {<fileid>} via WOPI
    Then the response body contains {Unauthorized}
    And the status code should be {401}
    Examples:
      | fileid |
      | dasdad |

  @putFile @negative
  Scenario Outline: Try to Attach a file to a passport with invalid AccessToken [<token>] via WOPI - Negative
#Create a new member
    Then I register a new random member via VIAM API
    And the field {status} has the value {OK}
#Login the new member via VIAM
    Given I clear the request body
    Given I login member with mode previousaddeddevice via VIAM API
    And the field {status} has the value {OK}
#Create a new entity - passport
    Given I clear the request body
    Then I create a new entity with type {4} via VIAM API
    And the field {status} has the value {OK}
#Add new claim to the current entity
    Given I clear the request body
    Given I load the REST request {Claims.json} with profile {passportName}
    Then I add a new claim to the guarded entity via VIAM API
    And the field {status} has the value {OK}
#Then I get the passport via WOPI
    Given we are testing with the current session the WOPI
    Then I get all the passports with fileid {testFileId} via WOPI
    And the field {status} has the value {ok}
#Then I attach a file to the passport
    Then I clear ALL headers
    Given I attach a pdf file {testpdf.pdf} to a passport with fileid {testFileId} and accessToken {<token>} via WOPI
    Then the response body contains {Unauthorized}
    And the status code should be {401}
    Examples:
      | token   |
      | dsadada |
      |         |