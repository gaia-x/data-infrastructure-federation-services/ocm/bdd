#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard
Feature: Dashboard - Plugins

  Background:
    Given we are testing the VIAM Api

  @plugins
  Scenario: Validate Plugins Page
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
    And User click on Plugins tab
#Privacy Policy and Beta Agreement validation
    Then Privacy policy and Beta Participation Agreement Links have the correct text and href
#Chrome Plugin validation
    And Validate Chrome Plugin have title "Chrome extension"
    And Validate Chrome Plugin have subtitle "Extend Vereign into your Gmail and Drive accounts"
#Collabora plugin validation
    And Validate Collabora Plugin have title "Collabora plugin"
    And Validate Collabora Plugin have subtitle "Included by default in the leading open source collaboration suite"
#Roundcube Plugin validation
    And Validate Roundcube Plugin have title "Roundcube plugin"
    And Validate Roundcube Plugin have subtitle "Add Vereign to your favorite open source webmail"
#Chrome Plugin download button validation
    When User click on visit Chrome
    Then Title of the page is "Vereign Beta for GMail - Chrome Web Store"
    And Validate Add to Chrome button is presented
#Collabora Plugin download button validation
    When User click on visit Collabora
    Then Title of the page is "Collabora Online Development Edition (CODE) - Collabora Productivity"
    And I close the browser

