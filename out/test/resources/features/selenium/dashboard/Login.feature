#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard
Feature: Dashboard - Login

  Background:
    Given we are testing the VIAM Api

  @login
  Scenario Outline: Login with valid PIN code
#Create new random user
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    And User logs out of vereign
#Welcome back page
    Then Main Info Message has the correct text "<Expected_Main_Message_Text>"
    And Validate page contains element with text "<Expected_Secondary_Message_Text>"
    And Authenticate button is disabled
    When User populates PIN code field "1111"
    Then Authenticate button is enabled
    When User click on Authenticate button
#Logged in
    Then User is redirected to Vereign Inbox
    And Title of the page is "Vereign | My Inbox"
    And I close the browser
    Examples:
      | Expected_Main_Message_Text | Expected_Secondary_Message_Text                                   |
      | Welcome back, BD!          | Please unlock your personal device key to log into the dashboard. |

  @login @negative
  Scenario: Try to login with invalid PIN Code
#Create new random user
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    And User logs out of vereign
#Welcome back Page
    And User populates PIN code field "0000"
#First attempt
    And User click on Authenticate button
    Then Error Message is presented with text "Wrong code" and message disappears
#Second Attempt
    When User click on Authenticate button
    Then Error Message is presented with text "Wrong code" and message disappears
#Third Attempt
    When User click on Authenticate button
    Then Error Message is presented with text "3 failed attempts. Identity is locked!" and message disappears
#Fourth attempt
    When User click on Authenticate button
    Then Error Message is presented with text that contains "Your identity has been locked. Try again in 4 minutes and" and error message disappears
#Validate that the error message will be shown even with valid PIN
    When User populates PIN code field "1111"
    And User click on Authenticate button
    Then Error Message is presented with text that contains "Your identity has been locked. Try again in 4 minutes and" and error message disappears
    When I close the browser

  @login
  Scenario Outline: Validate all labels in login flow
#Sign in to Vereign Page
    Given I open Chrome browser and navigate to vereign login page
    Then Validate sign in to Vereign page labels
    And Validate Link is presented with text "www.vereign.com" and href "https://www.vereign.com"
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    When User click on Access Account Button
    Then Main Info Message has the correct text "Sign in to Vereign"
    And Validate recover your account page labels
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    When User click on Need help with sign in button
#Authorise account on another device Page
    Then Main Info Message has the correct text "Authorise account on another device"
    And Validate Authorise account on another device page labels
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    And User click on Cancel button
    When User click on Recover your Vereign Account Button
#Restore access page
    Then Main Info Message has the correct text "Restore access"
    And Validate page contains element with text "<info_message>"
    And Placeholder of the field has value "Email or phone number"
    And Validate page contains element with text "Restore access"
    And Validate page contains element with text "Skip"
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    And I close the browser
    Examples:
      | info_message                                                                                                                        |
      | In order to initiate access recovery, please enter a mobile phone number or email address that you have been using for the account. |

  @login
  Scenario: Restore Access with Email
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
#Forget credentials
    And User logs out of vereign and click on cleanup local identity
#Restore account
    And User click on Access Account Button
    And User click on Recover your Vereign Account Button
    And User populates the Email/Mobile field with the same email address in Restore Access
    And User click on Restore access Button
#Confirmation code send - check your email
    Then Main Info Message has the correct text "Check your email"
    And Secondary Info Message has the correct text "Please enter the access code in the field below:"
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    When User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Add new pin and confirm new pin
    And User populates the New Pin Code and Confirm New Pin Code field with "1111" and "1111"
    And User click on Confirm button
#Validate User is logged in
    Then Title of the page is "Vereign | My Inbox"
    And I close the browser

  @login
  Scenario: Restore an account without finishing the process
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
#Restore account
    And User click on Access Account Button
    And User click on Recover your Vereign Account Button
    And User populates the Email/Mobile field with the same email address in Restore Access
    And User click on Restore access Button
#Click on the Vereign logo
    And User click on Vereign Logo
    And I wait for {500} mseconds
    And Error message is not presented
    And I refresh the current page
    And I wait for {500} mseconds
    And Error message is not presented
    And I close the browser
