#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

  @selenium @all @dashboard @wip
  Feature: Activity - Documents

    @activity
    Scenario: Upload odt and pdf files in valid format and validate the Activity Page
      Given I open Chrome browser and navigate to vereign login page
#Register a new random user
      And I register a new user with email via VIAM API
      And I login with the registered via the VIAM API user into dashboard
      When User click on close tour button
#Upload a valid pdf document
      And User uploads a document in pdf format
#Validate the message
      Then Confirmation Message is presented with text "The document was successfully uploaded with the name testpdf.pdf." and message disappears
#Upload a valid odt document
      And User click on Inbox tab
      And User uploads a document in odt format
      Then Confirmation Message is presented with text "The document was successfully uploaded with the name testOdf.odt." and message disappears
#Validate the upload of the odt document in the Activity Page
      And User click on Activity tab
#Create document
      Then Validate the title of the "1" activity is "<string>"
      And Validate "1" activity with key Login from has value "<string>"
      And Validate "1" activity with key UUID has value "<string>"
      And Validate "1" activity with key Device key has value "<string>"
      And Validate "1" activity with key User entity UUID has value "<string>"
      And Validate "1" activity with key Passport entity UUID has value "<string>"
#Update document
      Then Validate the title of the "2" activity is "<string>"
      And Validate "2" activity with key Login from has value "<string>"
      And Validate "2" activity with key UUID has value "<string>"
      And Validate "2" activity with key Device key has value "<string>"
      And Validate "2" activity with key User entity UUID has value "<string>"
      And Validate "2" activity with key Passport entity UUID has value "<string>"
#Validate the upload of the pdf document in the Activity Page
#Create document
      Then Validate the title of the "3" activity is "<string>"
      And Validate "3" activity with key Login from has value "<string>"
      And Validate "3" activity with key UUID has value "<string>"
      And Validate "3" activity with key Device key has value "<string>"
      And Validate "3" activity with key User entity UUID has value "<string>"
      And Validate "3" activity with key Passport entity UUID has value "<string>"
#Update document
      Then Validate the title of the "4" activity is "<string>"
      And Validate "4" activity with key Login from has value "<string>"
      And Validate "4" activity with key UUID has value "<string>"
      And Validate "4" activity with key Device key has value "<string>"
      And Validate "4" activity with key User entity UUID has value "<string>"
      And Validate "4" activity with key Passport entity UUID has value "<string>"
      And I close the browser