#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@all @selenium @dashboard @wip
Feature: Activity - Email

  @activity
  Scenario: Send verified gmail email and validate it in the activity page
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
#    And Register testing account if not registered and forget account credentials#TODO:REMOVE
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Activity - Send Verified gmail email"
    And User click on gmail Send button
#Navigate to dashboard
    And User navigates to vereign login page
    And User click on Continue button
    And Validate the subject of the first email is the one used to send email
#Validate the actions in the activity page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Send verified roundcube email and validate it in the activity page
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
#    And Register testing account if not registered and forget account credentials#TODO:REMOVE
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Activity - Send verified roundcube email"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
    And Validate the subject of the first email is the one used to send email
#Validate the actions in the activity page
    And User click on Activity tab
    Then Validate the title of the first activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Receive verified gmail email and validate it in the activity page
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
#    And Register testing account if not registered and forget account credentials#TODO:REMOVE
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Activity - Receive verified gmail email"
    And User click on gmail Send button
#Navigate to dashboard
    And User navigates to vereign login page
    And User click on Continue button
#Logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with testing account
    And User open the "1" email
#Click on Activity
    And User click on Activity tab
#Validate the restoring of the access action in the activity page
    Then Validate the title of the "1" activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    Then Validate the title of the "2" activity is "<string>"
    And Validate "2" activity with key Login from has value "<string>"
    And Validate "2" activity with key UUID has value "<string>"
    And Validate "2" activity with key Device key has value "<string>"
    And Validate "2" activity with key User entity UUID has value "<string>"
#Validate the receive email action in the Activity page
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key Login from has value "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key Device key has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Receive verified roundcube email and validate it in the activity page
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
#    And Register testing account if not registered and forget account credentials#TODO:REMOVE
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Activity - Receive verified roundcube email"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
      #Logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with testing account
    And User open the "1" email
#Click on Activity
    And User click on Activity tab
#Validate the restoring of the access action in the activity page
    Then Validate the title of the "1" activity is "<string>"
    And Validate "1" activity with key Login from has value "<string>"
    And Validate "1" activity with key UUID has value "<string>"
    And Validate "1" activity with key Device key has value "<string>"
    And Validate "1" activity with key User entity UUID has value "<string>"
    Then Validate the title of the "2" activity is "<string>"
    And Validate "2" activity with key Login from has value "<string>"
    And Validate "2" activity with key UUID has value "<string>"
    And Validate "2" activity with key Device key has value "<string>"
    And Validate "2" activity with key User entity UUID has value "<string>"
#Validate the receive email action in the Activity page
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key Login from has value "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key Device key has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
    And I close the browser

  @activity
  Scenario: Read verified gmail email and validate it in the Activity Page
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
#    And Register testing account if not registered and forget account credentials#TODO:REMOVE
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Activity - Read verified gmail email"
    And User click on gmail Send button
#Navigate to dashboard
    And User navigates to vereign login page
    And User click on Continue button
#Logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with testing account
#Receive the email
    And User open the "1" email
#Read the email
    And User click on email thread with message "Activity - Read verified gmail email"
#Validate the Read Action in the activity page of the receiver
    And User click on Activity tab
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key Login from has value "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key Device key has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
#Logout and login into dashboard with the senders account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the Read Action in the Activity of the sender
    And User click on Activity tab
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key Login from has value "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key Device key has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
    And I close the browser


  @activity
  Scenario: Read verified roundcube email and validate it in the Activity Page
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
#    And Register testing account if not registered and forget account credentials#TODO:REMOVE
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Activity - Read verified roundcube email"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with testing account
#Receive the email
    And User open the "1" email
#Read the email
    And User click on email thread with message "Activity - Read verified roundcube email"
#Validate the read action in the Activity ot the receiver
    And User click on Activity tab
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key Login from has value "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key Device key has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
#Logout and login with the senders account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the read action in the activity page of the sender
    And User click on Activity tab
    Then Validate the title of the "3" activity is "<string>"
    And Validate "3" activity with key Login from has value "<string>"
    And Validate "3" activity with key UUID has value "<string>"
    And Validate "3" activity with key Device key has value "<string>"
    And Validate "3" activity with key User entity UUID has value "<string>"
    And I close the browser









