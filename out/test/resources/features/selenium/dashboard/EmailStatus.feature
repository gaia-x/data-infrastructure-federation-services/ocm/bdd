#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @dashboard @all @notParallel
Feature: Email Status

  Background:
    Given we are testing the VIAM Api

  @emailStatus @gmail
  Scenario: Send email from gmail to roundcube and read it in dashboard
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if roundcube account is created
    And Check if roundcube account is registered and register it if not via VIAM API
#Log in with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Send email from gmail to roundcube and receive it in dashboard"
    And User click on gmail Send button
#Validate the email in the gmail has Sent status
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the status is updated to Read after opening the email in the dashboard of the receiver
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Login with the sender account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the read status is updated in the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Log into chrome extension
    And User Logs in chrome extension with password "1111"
#Navigate to gmail and validate the status is updated there too
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Read"
    And I close the browser

  @emailStatus @roundcube
  Scenario: Send email from roundcube to gmail and read it in dashboard
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Check if gmail account is created
    And Check if gmail account is registered and register it if not via VIAM API
#Log in with roundcube account
    And User logs into vereign with roundcube account
#Change the environment
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "vereign.automation@gmail.com"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Send email from roundcube to gmail and receive it in dashboard"
    And User click on roundcube Send button
#Validate the email in the roundcube has Sent status
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User click on Continue button
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Deliver the email in the dashboard of the recipient and validate the status
    And User open the "1" email
    And Validate the email has the subject used to send the email
#    And Validate the status of the email in the dashboard is Delivered
##Login with gmail account and validate the message have status delivered in the dashboard
#    And User logs out of vereign and click on cleanup local identity
#    And User logs into vereign with roundcube account
##Validate the delivered status in the dashboard of the sender
#    And User open the "1" email
#    And Validate the email has the subject used to send the email
#    And Validate the status of the email in the dashboard is Delivered
##Validate the delivered status in roundcube
#    And User navigates to roundcube
#    And User click on resume your previous session
#    And User enables Vereign On
#    And User click on roundcube Sent folder
#    And User Validates the roundcube sent email has status "Delivered"
##Login with the recipient account
#    And User navigates to vereign login page
#    And User logs out of vereign and click on cleanup local identity
#    And User logs into vereign with gmail account
#    And User open the "1" email
#    And Validate the email has the subject used to send the email
##Read the email in the dashboard of the recipient and validate the status
#    And User click on the "1" item in the email thread
    And Validate the status of the email in the dashboard is Read
#Login with the sender account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the read status is updated in the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Navigate to roundcube and validate the status is updated there too
    And User navigates to roundcube
    And User click on resume your previous session
    And User enables Vereign On
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Read"
    And I close the browser

  @emailStatus @roundcube @gmail
  Scenario: Send email from roundcube to gmail and read it in gmail
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
    And User navigates to vereign login page
#Check if testing and gmail accounts are created
    And Check if gmail account is registered and register it if not via VIAM API
#Log in with roundcube account
    And User logs into vereign with roundcube account
#Change the environment
    And User navigates to roundcube
    And User logs into roundcube
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "vereign.automation@gmail.com"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Send email from roundcube to gmail and read it in gmail"
    And User click on roundcube Send button
#Validate the email in the roundcube has Sent status
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User click on Continue button
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Login with the recipient account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
    And User navigates to gmail
    And User click on gmail Sent folder
    And User click on gmail Inbox folder
    And I wait for {15000} mseconds
#Validate the delivered status in the dashboard of the sender
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Login with the receivers account in the dashboard
#    And User logs out of vereign and click on cleanup local identity
#    And User logs into vereign with gmail account
##Validate the delivered status in the dashboard of the recipient
#    And User open the "1" email
#    And Validate the email has the subject used to send the email
#    And Validate the status of the email in the dashboard is Delivered
##Log into vereign with the senders account into dashboard
#    And User logs out of vereign and click on cleanup local identity
#    And User logs into vereign with roundcube account
#Validate the delivered status in roundcube
    And User navigates to roundcube
    And User click on resume your previous session
    And User click on roundcube Inbox folder
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Delivered"
#Login into vereign with the recipient account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Read the email in gmail
    And User Logs in chrome extension with password "1111"
    And User navigates to gmail
    And User click on the received roundcube email
    And I wait for {15000} mseconds
#Validate the status in the dashboard of the recipient
    And User navigates to vereign login page
    And User click on Continue button
#    And User open the "1" email
#    And Validate the email has the subject used to send the email
#    And Validate the status of the email in the dashboard is Read
#Login into vereign with the senders account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the Read status in the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Validate the Read status in roundcube
    And User navigates to roundcube
    And User click on resume your previous session
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Read"
    And I close the browser

  @emailStatus @gmail @roundcube @bug-roundcube-79
  Scenario: Send email from gmail to roundcube and read it in roundcube
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if  and roundcube accounts are created
    And Check if roundcube account is registered and register it if not via VIAM API
#Log in with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Send email from gmail to roundcube and read it in roundcube"
    And User click on gmail Send button
#Validate the email in the gmail has Sent status
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Change the environment
    And User navigates to roundcube
    And User logs into roundcube
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
    And User click on Mail button
    And User enables Vereign On
#Deliver the Mail
    And User click on roundcube Inbox folder
    And I wait for {2000} mseconds
#Validate the Delivered Status in the dashboard of the sender
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Login with the recipient account into dasbhaord
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the Delivered status in the dashboard on the recipient
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Login with the senders account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Validate the status of the email is Delivered in gmail
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Delivered"
#Login into dashboard with the recipient account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Loginto roundcube
    And User navigates to roundcube
    And User click on resume your previous session
#Read the email
    And User click on roundcube Inbox folder
    And User click on the received gmail email
    And User click on roundcube Inbox folder
    And I wait for {10000} mseconds
#Validate Read Status in the dashboard of the recipient
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Login into dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate Read Status in the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Validate the status of the email is Delivered in gmail
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Read"
    And I close the browser

  @emailStatus @gmail @bug-dashboard-536
  Scenario: Send email from gmail to multiple recipients and read it in dashboard
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if testing and roundcube accounts are created
    And Check if gravatar account is registered and register it if not via VIAM API
    And Check if testing account is registered and register it if not via VIAM API
#Log in with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
    And User navigates to gmail
#Compose the email
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com, automation.vereign@gmail.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Send email from gmail to multiple recipients and receive it in dashboard"
    And User click on gmail Send button
#Validate the email in the gmail has Sent status
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the roundcube receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Receive And Read the email with the roundcube receiver in the dashboard
    And User open the "1" email
#    And User click on email thread with message "Send email from gmail to multiple recipients and receive it in dashboard"
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and Login with the gravatar account in the dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gravatar account
#Receive the email in the dashboard of the gravatar account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Logout and Login with the roundcube account in the dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the email has Delivered status
    When User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Logout and login with the senders account into the dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the status of the email in the dashboard of the sender is Delivered
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
    And User Logs in chrome extension with password "1111"
#Validate the status of the email is Delivered in gmail
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Read"
    And I close the browser

  @emailStatus @roundcube @bug-dashboard-536
  Scenario: Send email from roundcube to multiple recipients and read it in dashboard
#Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
#Check if testing and gmail accounts are created
    And Check if gravatar account is registered and register it if not via VIAM API
    And Check if gmail account is registered and register it if not via VIAM API
#Log in with roundcube account
    And User logs into vereign with roundcube account
#Change the environment
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "vereign.automation@gmail.com, automation.vereign@gmail.com"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Send from roundcube to multiple recipients and read it in dashboard"
    And User click on roundcube Send button
#Validate the email in the roundcube has Sent status
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User click on Continue button
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the gmail account into the dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Receive and Read the email with the gmail account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And User click on email thread with message "Send from roundcube to multiple recipients and read it in dashboard"
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the gravatar account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gravatar account
#Receive the email with the gravatar account
    And User open the "1" email
    And Validate the email has the subject used to send the email
#Validate the status of the email in the gravatar account dashboard
    And Validate the status of the email in the dashboard is Delivered
#Logout and login with the senders account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the status in the sender dashboard
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Validate the status in roundcube
    And User navigates to roundcube
    And User click on resume your previous session
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Delivered"
#Login with the gmail account into the dashboard
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the delivered status in the dashboard of the gmail account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Read the email in the dashboard of the gravatar account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gravatar account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And User click on email thread with message "Send from roundcube to multiple recipients and read it in dashboard"
#Validate the status is Read in the dashboard of the gravatar account
    And Validate the status of the email in the dashboard is Read
#Logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the read status in the dashboard of the gmail account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Logout and login with the senders account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the Sent status in the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Validate the Read status in roundcube client
    And User navigates to roundcube
    And User click on resume your previous session
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Read"
    And I close the browser

  @roundcube @emailStatus
  Scenario: Send email from roundcube to gmail and one not registered receiver and read it in dashboard
#Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
#Check if testing and gmail accounts are created
    And Check if gmail account is registered and register it if not via VIAM API
#Log in with roundcube account
    And User logs into vereign with roundcube account
#Change the environment
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "vereign.automation@gmail.com, "
    And User populates roundcube To Field with random email
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Send from roundcube to gmail and not registered user and receive it in dashboard"
    And User click on roundcube Send button
#Validate the email in the roundcube has Sent status
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User click on Continue button
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Deliver the email in the dashboard of the recipient and validate the status
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Login with roundcube account and validate the message have status delivered in the dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the delivered status in the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Validate the delivered status in roundcube
    And User navigates to roundcube
    And User click on resume your previous session
    And User enables Vereign On
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Sent"
#Register the not registered user
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
#Receive the email in the dashboard of the not registered user
    And User open the "1" email
    And Validate the email has the subject used to send the email
#Validate the status is changed to Delivered after the registration of the not registered user and receiving the email
    And Validate the status of the email in the dashboard is Delivered
#Read the email in the dashboard of the not registered user
    And User click on the "1" item in the email thread
#Login with the roundcube account into dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate Delivered status in the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Validate the Delivered status in roundcube client (senders client)
    And User navigates to roundcube
    And User click on resume your previous session
    And User enables Vereign On
    And User click on roundcube Inbox folder
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Delivered"
#Validate the Delivered status in the dashboard of the gmail account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Read the email with the gmail account in the dashboard
    And User click on the "1" item in the email thread
#Validate the Read Status in the dashboard of the gmail account
    And Validate the status of the email in the dashboard is Read
#Login with the newly created account and validate the status in the dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with newly created account
#Validate the Read Status in the dashboard of the newly created account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Validate the Read Status in the dashboard of the sender
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Validate the Read Status in roundcube sent folder
    And User navigates to roundcube
    And User click on resume your previous session
    And User enables Vereign On
    And User click on roundcube Inbox folder
    And User click on roundcube Sent folder
    And User Validates the roundcube sent email has status "Read"
    And I close the browser

  @gmail @emailStatus
  Scenario: Send email from gmail to roundcube and one not registered receiver and read it in dashboard
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
    And I navigate to 10minuteEmail and save the email address
#Change the environment
    When I change the extension environment
#Check if testing and roundcube accounts are created
    And Check if roundcube account is registered and register it if not via VIAM API
#Log in with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com,"
    And User populates gmail To field with the email from 10minuteEmail
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Send email from gmail to roundcube and not registered user and receive it in dashboard"
    And User click on gmail Send button
#Validate the email in the gmail has Sent status
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Sent"
#Validate the email has Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the receivers account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Read the email in the dashboard of the registered receiver
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Validate the Sent status in the dashboard of the sender
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Validate the Sent Status in gmail
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Sent"
#Register the not registered receiver
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver via VIAM API
    And User logs into vereign with newly created account
#Read the email in the dashboard of the not registered user
    And User open the "1" email
    And Validate the email has the subject used to send the email
#Validate the status is updated to Read in the dashboard of the not registered receiver
    And Validate the status of the email in the dashboard is Read
#Validate the Read status in gmail dashboard
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Validate the Read status in gmail
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Read"
#Validate the Read status in roundcube dashboard
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
    And I close the browser

  @gmail @roundcube @bug-roundcube-79
  Scenario: Send gmail email to two roundcube receivers and read it in roundcube
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if both roundcube accounts are created
    And Check if roundcube account is registered and register it if not via VIAM API
    And Check if second roundcube account is registered and register it if not via VIAM API
#Log in with gmail account into the dashboard
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com, bb8.droid@kolab-qa.vereign.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Send gmail email to two roundcube receivers and read it in roundcube"
    And User click on gmail Send button
#Validate the email in the gmail has Sent status
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Sent"
#Validate the Sent status in the dashboard of the sender
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Sent
#Logout and login with the first roundcube account into dashboard
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Log into roundcube with the first roundcube account
    And User navigates to roundcube
    And User logs into roundcube
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
    And User click on Mail button
    And User enables Vereign On
#Receive and Read the email in the roundcube of the first roundcube account
    And User click on roundcube Inbox folder
    And User click on the received gmail email
    And I wait for {2000} mseconds
#Logout and Login into dashboard with second roundcube account
    When User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with second roundcube account
#Login into roundcube with second roundcube account
    And User navigates to roundcube
    And User click on resume your previous session
    And User click on roundcube Logout button
    And User logs into roundcube with second testing account
#Change the environment for the second roundcube account too
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
    And User click on Mail button
    And User enables Vereign On
#Receive the email
    And User click on roundcube Inbox folder
    And I wait for {2000} mseconds
#Logout and Login into dashboard with the senders account
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the Delivered status into the dashboard of the sender
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Validate the Delivered status into gmail (senders client)
    And User Logs in chrome extension with password "1111"
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Delivered"
#Logout and Login with the first roundcube account into the dashboard
    And User navigates to vereign login page
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the Delivered status in the dashboard of the first roundcube account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Logout and login into dashboard with the second roundcube account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with second roundcube account
#Validate the Delivered status in the dashboard of the second roundcube account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Delivered
#Login into roundcube with the second roundcube account
    And User navigates to roundcube
    And User click on resume your previous session
    And User click on roundcube Logout button
    And User logs into roundcube with second testing account
    And User click on roundcube Inbox folder
#Read the email with the second roundcube account
    And User click on the received gmail email
    And I wait for {2000} mseconds
#Validate the Read status in the dashboard of the second roundcube account
    And User navigates to vereign login page
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Logout and Login  into dashboard with the first roundcube account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Validate the Read status in the dashboard of the first roundcube account
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Logout and Login into dashboard with the senders(gmail) account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
#Validate the Read Status in the Dashboard of the Sernder(Gmail)
    And User open the "1" email
    And Validate the email has the subject used to send the email
    And Validate the status of the email in the dashboard is Read
#Validate the Read Status into gmail
    And User navigates to gmail
    And User click on gmail Sent folder
    And User Validates the gmail sent email has status "Read"
    And I close the browser










  



