#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @dashboard @all @notParallel
Feature: Dashboard - Inbox listing

  Background:
    Given we are testing the VIAM Api

  @inbox @gmail
  Scenario: Send verified gmail email to registered user
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Log into vereign with testing account - vereign.automation@gmail.com
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Inbox Listing. Verified gmail email to registered user"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
    And I close the browser

  @inbox @gmail
  Scenario: Send verified gmail email to registered user with changed profile
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Log into vereign with testing account - vereign.automation@gmail.com
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
#Select friends profile
    And User selects "Friends" from the gmail profile select
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Inbox Listing. Verified gmail email to registered user with changed profile"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "Friends"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
    And I close the browser

  @inbox @gmail
  Scenario: Send multiple gmail emails to not registered user
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Log into vereign with testing account - vereign.automation@gmail.com
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User sends "5" gmail emails
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate all of the emails in the Inbox
    And Validate all of the gmail emails in the Email
    And I close the browser

  @inbox @roundcube
  Scenario: Send multiple roundcube emails to not registered user
#Navigate to vereign login page
    Given I open Chrome browser and navigate to vereign login page
#Login with testing account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail
    And User click on Mail button
#Enable Vereign
    And User enables Vereign On
    And User sends "4" roundcube emails
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate all of the emails in the Inbox
    Then Validate all of the roundcube emails in the Email
    And I close the browser

  @gmail @delete
  Scenario: Gmail Delete the email from the dashboard of the sender and check the email in the receivers dashboard
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Change the environment
    When I change the extension environment
#Check if the testing account is created and login with gmail account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And User Logs in chrome extension with password "1111"
#Send verified email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "tested23@abv.bg"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Verified gmail email to registered user"
    And User click on gmail Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Email
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#    When User click on email thread with message "Verified gmail email to registered user"
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And Validate the subject of the first email is not the one used to send email
#Logout and login with the testing account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with testing account
#Validate the Email in the inbox of the receiver after the email is deleted
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the email and validate it
    When User open the "1" email
    Then Validate the email has the subject used to send the email
    Then Validate the message of the email for registered user has value "Verified gmail email to registered user"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate To value in the email thread has value "Boris Dimitrov <tested23@abv.bg>"
    And Validate From value in the email thread has value "Vereign Automation <vereign.automation@gmail.com>"
    And Validate Name field in the email template has value "Vereign Automation"
    And Validate Email field in the email template has value "vereign.automation@gmail.com"
    And Validate verified icon is presented on the Email claim of the Email Thread
    And I close the browser

  @roundcube @delete
  Scenario: Roundcube Delete the email from the dashboard of the sender and check the email in the receivers dashboard
#Navigate to vereign
    Given I open Chrome browser and navigate to vereign login page
#Check if the testing account is created and login with roundcube account
    And Check if testing account is registered and register it if not via VIAM API
    And User logs into vereign with roundcube account
#Navigate to roundcube
    And User navigates to roundcube
#Log into roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Compose the email
    And User click on roundcube Compose button
    And I wait for {2000} mseconds
    And Validate vereign icon is presented in roundcube
    And User populates roundcube To Field "tested23@abv.bg"
    And User populates roundcube Subject Field with random subject
    And User populates roundcube Message Field "Verified roundcube email to registered user"
    And User click on roundcube Send button
#Navigate to vereign
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Open the Email thread and validate it
#    When User click on email thread with message "Verified roundcube email to registered user"
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And Validate the subject of the first email is not the one used to send email
#Logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with testing account
#Validate the email in the inbox of the receiver
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Validate the email in the email
    When User open the "1" email
    Then Validate the message of the email for registered user has value "Verified roundcube email to registered user"
    And Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
    And Validate the date and time in the email has correct value
    And Validate vereign label in Email thread when email is send to registered user
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Roundcube Robot"
    And Validate Name field in the email template has value "Roundcube Robot"
    And Validate Email field in the email template has value "mister.test@kolab-qa.vereign.com"
    And I close the browser

  @delete @gmail @roundcube
  Scenario: Delete the email from the dashboard of the sender and reply to the email
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Check if roundcube account is created and log into dashboard with gmail account
    And Check if roundcube account is registered and register it if not via VIAM API
#Check if the testing account is created and login with gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And I change the extension environment
    And User Logs in chrome extension with password "1111"
#Send gmail email to the registered roundcube email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Gmail email waiting for reply"
    And User click on gmail Send button
#Check the email in the dashboard
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Open the Email thread and validate it
#    When User click on email thread with message "Verified roundcube email to registered user"
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And Validate the subject of the first email is not the one used to send email
#Logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
    And User navigates to roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Reply to the email
    And User click on the received gmail email
    And User click on the roundcube details button in the sent email
    And I wait for {5000} mseconds
    And User click on roundcube Reply button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube Message Field "Repling to gmail email"
    And User click on roundcube Send button
    And User navigates to vereign login page
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
#    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the email
    And User open the "1" email
#Validate the email
    Then Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
#Validate there are 2 items in the email thread
    And Validate there are "2" items in the email thread
#Open the second item from the email thread
    When User click on the "1" item in the email thread
    When User click on the "2" item in the email thread
    Then Validate vereign label in the "2" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Gmail email waiting for reply"
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Vereign Automation"
    And Validate Name field in the "2" item from the Email Thread has value "Vereign Automation"
    And Validate Email field in the "2" item from the Email Thread has value "vereign.automation@gmail.com"
#Close the second item from the email thread
    When User click on the "2" item in the email thread
#Open the first item from the email thread
    When User click on the "1" item in the email thread
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email after reply has value "Repling to gmail email"
    And Validate To value in the email thread has value "Vereign Automation"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the "1" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "1" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "1" message send to you
    And Validate there is "1" message send from you
#End of the validation with the mister.test@kolab-qa.vereign.com account
    And User logs out of vereign and click on cleanup local identity
#Log into vereign with the gmail account - vereign.automation@gmail.com
    And User logs into vereign with gmail account
#Validate the email in the inbox of the sender
    Then Validate the subject of the first email is the one used to send email
    And Validate the Interaction Type in the inbox has value "Email"
#    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Validate the email
    And User open the "1" email
    Then Validate the Interaction Type in the email has value "Email"
#    And Validate the profile in the email has value "Email"
    And Validate there are "2" items in the email thread
#Validate the second item in the email thread
    When User click on the "1" item in the email thread
    When User click on the "2" item in the email thread
    Then Validate vereign label in the "2" item from the Email thread when email is send to registered user
    And Validate the message of the email for registered user has value "Gmail email waiting for reply"
    And Validate To value in the email thread has value "Roundcube Robot"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the "2" item from the Email Thread has value "Vereign Automation"
    And Validate Email field in the "2" item from the Email Thread has value "vereign.automation@gmail.com"
    When User click on the "2" item in the email thread
#Validate the first item in the email thread
    And User click on the "1" item in the email thread
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email after reply has value "Repling to gmail email"
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Roundcube Robot"
    And Validate Name field in the "1" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "1" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "1" message send to you
    And Validate there is "1" message send from you
    And I close the browser

    @delete @gmail @roundcube
  Scenario: Delete and email chain and reply to the email
#Open chrome and load the extension and the profile
    Given I open chrome and load chrome extension and chrome profile
#Check if the roundcube account is registered
    And Check if roundcube account is registered and register it if not via VIAM API
#Login with the gmail account
    And User logs into vereign with gmail account
#Log into vereign chrome extension
    And I change the extension environment
    And User Logs in chrome extension with password "1111"
#Send gmail email to the registered roundcube email
    And User navigates to gmail
    And User click on gmail Compose button
    Then Validate vereign icon is presented in gmail
    When User populates gmail To field with value "mister.test@kolab-qa.vereign.com"
    And User populates gmail Subject field with random subject
    And User populates gmail Message field with value "Gmail email waiting for reply"
    And User click on gmail Send button
#Forget account credentials
    And User navigates to vereign login page
    And User click on Continue button
#Validate the email in the Inbox
    Then Validate the subject of the first email is the one used to send email
#Open the Email and validate the fields
    When User open the "1" email
    Then Validate the email has the subject used to send the email
#Delete the email in the dashboard of the gmail account
#    When User click on email thread with message "Verified roundcube email to registered user"
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And Validate the subject of the first email is not the one used to send email
#Logout and login with the roundcube account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with roundcube account
#Delete the email in the dashboard of the roundcube account
    And User open the "1" email
    Then Validate the email has the subject used to send the email
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And Validate the subject of the first email is not the one used to send email
#Log into roundcube
    And User navigates to roundcube
    And User logs into roundcube
#Change the environment
    And User click on Settings button
    And User click on vereign settings button
    And User populates the correct environment
    And User click on Save button
#Click Mail button
    And User click on Mail button
#Enable vereign
    And User enables Vereign On
#Reply to the email
    And User click on the received gmail email
    And User click on the roundcube details button in the sent email
    And I wait for {5000} mseconds
    And User click on roundcube Reply button
    And Validate vereign icon is presented in roundcube
    And User populates roundcube Message Field "Repling to gmail email"
    And User click on roundcube Send button
    And User navigates to vereign login page
#Validate the email in the inbox
    And Validate the Interaction Type in the inbox has value "Email"
#    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the email
    And User open the "1" email
#Validate the email
    Then Validate the Interaction Type in the email has value "Email"
    And Validate the count of the contacts in the email is "1"
    And Validate the profile in the email has value "Email"
#Validate the email thread in the dashboard of the receiver
    And Validate there are "1" items in the email thread
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email after reply has value "Repling to gmail email"
    And Validate To value in the email thread has value "Vereign Automation"
    And Validate From value in the email thread has value "You"
    And Validate Name field in the "1" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "1" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "0" message send to you
    And Validate there is "1" message send from you
#End of the validation with the mister.test@kolab-qa.vereign.com account
    And User logs out of vereign and click on cleanup local identity
#Log into vereign with the gmail account - vereign.automation@gmail.com
    And User logs into vereign with gmail account
#Validate the email in the inbox of the gmail account
    And Validate the Interaction Type in the inbox has value "Email"
#    And Validate the profile in the inbox has value "Email"
    And Validate the date and time in the inbox has correct value
    And Validate the count of the contacts in the inbox is "1"
#Open the email
    And User open the "1" email
    Then Validate the Interaction Type in the email has value "Email"
#    And Validate the profile in the email has value "Email"
    And Validate there are "1" items in the email thread
#Validate the email thread in the dashboard of the gmail account
    Then Validate vereign label in the "1" item from the Email thread when email is send to registered user
    And Validate the message of the email after reply has value "Repling to gmail email"
    And Validate To value in the email thread has value "You"
    And Validate From value in the email thread has value "Roundcube Robot"
    And Validate Name field in the "1" item from the Email Thread has value "Roundcube Robot"
    And Validate Email field in the "1" item from the Email Thread has value "mister.test@kolab-qa.vereign.com"
#Click on chat view and validate
    When User click on Chat View
    Then Validate there is "1" message send to you
    And Validate there is "0" message send from you
    And I close the browser





