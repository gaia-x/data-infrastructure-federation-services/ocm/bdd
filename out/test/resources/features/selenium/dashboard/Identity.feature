#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard
Feature: Dashboard - Identity

  Background:
    Given we are testing the VIAM Api

  @identity
  Scenario: Validate Identity of a user after registration
#Create new Random User
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
#Identity page
    Then Title of the page is "Vereign | Manage your Identity"
    And Validate Identity email field has the expected value - the email used for registration
    And Validate email field is validated in the Identity Page
    When User expand all sub folders
    Then Validate Identity Given name "Boris" and Family Name "Dimitrov" and Name fields
    And Validate Identity claim with label "Phone" has value "+359884045187"
    And Validate phone field is not validated in the Identity Page
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
    And I close the browser

  @identity
  Scenario:Validate Identity Page labels and fields
#Create new Random User
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
#Identity Page
    Then Privacy policy and Beta Participation Agreement Links have the correct text and href
    And Validate Identity Page labels are presented
    And Validate Back button is presented
    And I close the browser

  @identity
  Scenario: Validate Identity Select options
#Create new Random User
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
#Identity Page
    Then Validate main Identity select options "Name, Date of Birth, Gender, Address, Communication, Social, Language"
    When User select "Name" option from main Identity select
    Then Validate secondary Identity select options "Full name, Nickname"
    When User select "Full name" option from secondary Identity select
    Then Placeholder of the field has value "First name"
    And Placeholder of the field has value "Last name"
    When User select "Nickname" option from secondary Identity select
    Then Placeholder of the field has value "Type your nickname here"
    When User select "Date of Birth" option from main Identity select
    When User select "Gender" option from main Identity select
    Then Validate Gender radio buttons "Male, Female, Other"
    When User select "Address" option from main Identity select
    Then Placeholder of the field has value "Street address"
    And Placeholder of the field has value "Locality"
    And Placeholder of the field has value "Region"
    And Placeholder of the field has value "Postal code"
    And Placeholder of the field has value "Country"
    When User select "Communication" option from main Identity select
    Then Validate secondary Identity select options "Email, Phone, Chat"
    When User select "Email" option from secondary Identity select
    Then Placeholder of the field has value "Type your email here"
    When User select "Phone" option from secondary Identity select
    Then Placeholder of the field has value "Type your phone number here"
    When User select "Chat" option from secondary Identity select
    Then Placeholder of the field has value "Chat Service"
    And Placeholder of the field has value "Number or Username"
    When User select "Social" option from main Identity select
    Then Validate secondary Identity select options "Twitter, LinkedIn"
    When User select "Twitter" option from secondary Identity select
    Then Placeholder of the field has value "Url"
    When User select "LinkedIn" option from secondary Identity select
    Then Placeholder of the field has value "Url"
    When User select "Language" option from main Identity select
    Then Validate all presented languages
    And I close the browser

  @identity
  Scenario: Validate the deletion of default claims is not possible
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
#Identity Page
    And User click on delete "Email" claim button
    Then Validate the delete claim confirmation title and text
    And User confirms the deletion
    Then Error Message is presented with text "Cannot delete default claim" and message disappears
    When User click on delete "Full  name" claim button
    Then Validate the delete claim confirmation title and text
    And User confirms the deletion
    Then Error Message is presented with text "Cannot delete default claim" and message disappears
    When User click on delete "Phone" claim button
    Then Validate the delete claim confirmation title and text
    And User confirms the deletion
    Then Error Message is presented with text "Cannot delete default claim" and message disappears
    And I close the browser

  @identity
  Scenario: Upload a photo with valid format
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
    And User upload a photo with valid format to the identity
    Then Validate image is presented
    And Validate image is presented in the top right of the page
    And I close the browser

  @identity
  Scenario: Upload a photo with invalid format
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
    And User upload a photo with invalid format to the identity
    Then Error Message is presented with text "Please use images in jpg or png format." and message disappears
    And Validate image is not presented
    And Validate image is not presented in the top right of the page
    And I close the browser

  @identity
  Scenario: Add claims to the Identity
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on Identity tab
#Adding Full Name
    And User select "Name" option from main Identity select
    And User select "Full name" option from secondary Identity select
    And User populates the field with placeholder "First name" with value "Ivan"
    And User populates the field with placeholder "Last name" with value "Petrov"
    And User click on Add to your Identity button
#Adding Nickname
    And User select "Name" option from main Identity select
    And User select "Nickname" option from secondary Identity select
    And User populates the field with placeholder "Type your nickname here" with value "myNickname"
    And User click on Add to your Identity button
#Adding Random Gender
    And User select random Gender from the select
    And User click on Add to your Identity button
#Adding Address
    And User select "Address" option from main Identity select
    And User populates the field with placeholder "Street address" with value "G.M Dimitrov"
    And User populates the field with placeholder "Locality" with value "Sofia"
    And User populates the field with placeholder "Region" with value "Sofia"
    And User populates the field with placeholder "Postal code" with value "1777"
    And User populates the field with placeholder "Country" with value "Bulgaria"
    And User click on Add to your Identity button
#Adding Email
    And User select "Communication" option from main Identity select
    And User select "Email" option from secondary Identity select
    And User populates the field with placeholder "Type your email here" with value "tested23@abv.bg"
    And User click on Add to your Identity button
#Adding Phone
    And User select "Communication" option from main Identity select
    And User select "Phone" option from secondary Identity select
    And User populates the field with placeholder "Type your phone number here" with value "+359883353610"
    And User click on Add to your Identity button
#Adding Chat
    And User select "Communication" option from main Identity select
    And User select "Chat" option from secondary Identity select
    And User populates the field with placeholder "Chat Service" with value "Mirc"
    And User populates the field with placeholder "Number or Username" with value "testingUser"
    And User click on Add to your Identity button
#Adding Twitter
    And User select "Social" option from main Identity select
    And User select "Twitter" option from secondary Identity select
    And User populates the field with placeholder "Url" with value "www.localhost.com"
    And User click on Add to your Identity button
#Adding LinkedIn
    And User select "Social" option from main Identity select
    And User select "LinkedIn" option from secondary Identity select
    And User populates the field with placeholder "Url" with value "www.localhost.com"
    And User click on Add to your Identity button
#Adding Language
    And User select random Language from the select
    And User click on Add to your Identity button
#Adding Date of birth
    And User select "Date of Birth" option from main Identity select
    And User populates the date of birth "01/01/2000"
    And User click on Add to your Identity button
##Validate expanding folders are presented
    Then Validate expanding folder is presented in the "Emails" claim
    And Validate expanding folder is presented in the "Full  names" claim
    And Validate expanding folder is presented in the "Phones" claim
    And Validate expanding folder is presented in the "Social" claim
    And Validate warning icon is presented in the "Emails" claim
    And Validate warning icon is presented in the "Phones" claim
    And Validate warning icon is not presented in the "Full  names" claim
    And Validate warning icon is not presented in the "Social" claim
    And I refresh the current page
    And User expand all folders
#Validate the values of the identity
    Then Validate Identity claim with label "Given name" has value "Ivan"
    And Validate Identity claim with label "Family name" has value "Petrov"
    And Validate Identity claim with label "Nickname" has value "myNickname"
    And Validate Identity claim with label "Country" has value "Bulgaria"
    And Validate Identity claim with label "Locality" has value "Sofia"
    And Validate Identity claim with label "Postal code" has value "1777"
    And Validate Identity claim with label "Region" has value "Sofia"
    And Validate Identity claim with label "Street address" has value "G.M Dimitrov"
    And Validate Identity claim with label "Email" has value "tested23@abv.bg"
    And Validate Identity claim with label "Phone" has value "+359883353610"
    And Validate Identity claim with label "Chat service name" has value "Mirc"
    And Validate Identity claim with label "User" has value "testingUser"
    And Validate Identity claim with label "Social network" has value "LinkedIn"
    And Validate Identity claim with label "Url" has value "www.localhost.com"
    And Validate Identity claim with label "Social network" has value "Twitter"
    And Validate Identity claim with label "Url" has value "www.localhost.com"
    And Validate Identity claim with label "Date of  birth" has value "Jan 01 2000"
    And Validate claim with label Gender has the expected value
    And Validate claim with label Language has the expected value
    And I close the browser

  @identity
  Scenario: Edit Claims of the Identity
    Given I open Chrome browser and navigate to vereign login page
#Login and Add claims to the identity
    And I register a new user with email and add all claims via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on Identity tab
#Expand all folders
    And I refresh the current page
    And User expand all folders
#Edit the Address claim
    When User click on edit "Address" claim button
    And User populates the field with placeholder "Street address" with value "Street Address Edited"
    And User populates the field with placeholder "Locality" with value "Locality Edited"
    And User populates the field with placeholder "Region" with value "Region Edited"
    And User populates the field with placeholder "Postal code" with value "Code Edited"
    And User populates the field with placeholder "Country" with value "Country Edited"
    And User click on Update claim button
#Validate the Address claim after edit
    Then Validate Identity claim with label "Country" has value "Country Edited"
    And Validate Identity claim with label "Locality" has value "Locality Edited"
    And Validate Identity claim with label "Postal code" has value "Code Edited"
    And Validate Identity claim with label "Region" has value "Region Edited"
    And Validate Identity claim with label "Street address" has value "Street Address Edited"
#Edit Chat claim
    When User click on edit "Chat" claim button
    And User populates the field with placeholder "Chat Service" with value "Chat Service Edited"
    And User populates the field with placeholder "Number or Username" with value "Username Edited"
    And User click on Update claim button
#Validate the Chat claim after edit
    Then Validate Identity claim with label "Chat service name" has value "Chat Service Edited"
    And Validate Identity claim with label "User" has value "Username Edited"
#Edit Date of Birth claim
    When User click on edit "Date of  birth" claim button
    And User populates the date of birth "02/02/2002"
    And User click on Update claim button
#Validate the Date of Birth claim after edit
    Then Validate Identity claim with label "Date of  birth" has value "Feb 02 2002"
#Edit Email claim
    When User click on edit "Email" claim button
    And User populates the field with placeholder "Type your email here" with value "edited@example.com"
    And User click on Update claim button
#Validate the Email claim after edit
    Then Validate Identity claim with label "Email" has value "edited@example.com"
#Edit Gender claim
    When User click on edit "Gender" claim button
    And User select random Gender from the select
    And User click on Update claim button
#Validate the Gender claim after edit
    Then Validate claim with label Gender has the expected value
#Edit Language claim
    When User click on edit "Language" claim button
    And User select "Bulgarian" from the Language select
    And User click on Update claim button
#Validate the Language claim after edit
    Then Validate Identity claim with label "Language" has value "Bulgarian"
#Edit Name claim
    When User click on edit "Full  name" claim button
    And User populates the field with placeholder "First name" with value "First Name Edited"
    And User populates the field with placeholder "Last name" with value "Last Name Edited"
    And User click on Update claim button
#Validate the Name claim after edit
    Then Validate Identity claim with label "Given name" has value "First Name Edited"
    And Validate Identity claim with label "Family name" has value "Last Name Edited"
#Edit Nickname claim
    When User click on edit "Nickname" claim button
    And User populates the field with placeholder "Type your nickname here" with value "Nickname Edited"
    And User click on Update claim button
#Validate the Nickname claim after edit
    Then Validate Identity claim with label "Nickname" has value "Nickname Edited"
#Edit Phone claim
    When User click on edit "Phone" claim button
    And User populates the field with placeholder "Type your phone number here" with value "+359884045187"
    And User click on Update claim button
#Validate the Phone claim after update
    Then Validate Identity claim with label "Phone" has value "+359884045187"
#Edit Social claim
    When User click on edit "Social" claim button
    And User populates the field with placeholder "Url" with value "www.localhost.com/edited"
    And User click on Update claim button
#Validate the Social claim after edit
    Then Validate Identity claim with label "Url" has value "www.localhost.com/edited"
    And I close the browser

  @identity
  Scenario: Validate the deletion of non default claims is possible
    Given I open Chrome browser and navigate to vereign login page
#Login and add claims to the identity
    And I register a new user with email and add all claims via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on Identity tab
#Expand all folders
    And I refresh the current page
    And User expand all folders
#Delete Address Claim
    And User click on delete "Address" claim button
    And User confirms the deletion
    Then Validate "Address" claim is deleted from the Identity page
#Delete Chat Claim
    When User click on delete "Chat" claim button
    And User confirms the deletion
    Then Validate "Chat" claim is deleted from the Identity page
#Delete Date of birth Claim
    When User click on delete "Date of  birth" claim button
    And User confirms the deletion
    Then Validate "Date of  birth" claim is deleted from the Identity page
#Delete Email claim
    And User expand all main folders
    And User click on delete "Email" claim button
    And User confirms the deletion
    Then Validate claim "Email" with value "tested23@abv.bg" is not presented
#Delete Gender Claim
    And User click on delete "Gender" claim button
    And User confirms the deletion
    Then Validate "Gender" claim is deleted from the Identity page
#Delete Language Claim
    And User click on delete "Language" claim button
    And User confirms the deletion
    Then Validate "Language" claim is deleted from the Identity page
#Delete Name Claim
    And User expand all main folders
    And User click on delete "Full  name" claim button
    And User confirms the deletion
    Then Validate claim "Full  name" with value "Petrov, Ivan" is not presented
#Delete Nickname Claim
    And User click on delete "Nickname" claim button
    And User confirms the deletion
    Then Validate "Nickname" claim is deleted from the Identity page
#Delete Phone Claim
    And User expand all main folders
    And User click on delete "Phone" claim button
    And User confirms the deletion
    Then Validate claim "Phone" with value "+359883353610" is not presented
#Delete Social Claim
    And User click on delete "Social" claim button
    And User confirms the deletion
    Then Validate "Social" claim is deleted from the Identity page
    And I close the browser

  @identity
  Scenario: Edit already validated email and phone and validate them again
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    And User click on Identity tab
#Edit Email
    And User click on edit "Email" claim button
    And User populates the Identity Email field with random valid email
    And User click on Update claim button
#Email is not validated after the edit
    Then Validate email field is not validated in the Identity Page
#Validate the email
    When User click on validate "Email" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
#Email is validated
    Then Validate email field is validated in the Identity Page
#Edit Phone - phone is not validated at this point
    When User click on edit "Phone" claim button
    And User populates the Phone field with random value
    And User click on Update claim button
    Then Validate phone field is not validated in the Identity Page
#Validate the phone
    When User click on validate "Phone" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
    Then Validate phone field is validated in the Identity Page
#Edit the Phone - phone is validated at this point
    When User click on edit "Phone" claim button
    And User populates the Phone field with random value
    And User click on Update claim button
#Phone is not validated after the edit
    Then Validate phone field is not validated in the Identity Page
#Validate the Phone
    When User click on validate "Phone" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
    Then Validate phone field is validated in the Identity Page
    And I close the browser

  @identity
  Scenario: Validate email in the Identity Page when the registration was not finished
#Start the registration
    Given I open Chrome browser and navigate to vereign login page
    When User clicks on Create Account button
    And User populates the Email address or mobile number field with other valid email
    And User click on Confirm button
#Check your email message is presented
    Then Main Info Message has the correct text "Check your email"
#Close the browser without populating the confirmation code
    And User navigates to vereign login page
#Start a new registration with other email and finish the registration
    When I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on close tour button
    And User click on Identity tab
#Add the email that did not finished the registration
    And User select "Communication" option from main Identity select
    And User select "Email" option from secondary Identity select
    And User populates the Identity Email field with the email that he did not finished the registration
    And User click on Add to your Identity button
#Expand all folders
    And User expand all main folders
#Validate the email
    And User click on validate "Email" claim button
#Validate the modal
    Then Main Info Message in modal dialog is "Check your email"
    And Validate page contains element with text "We've sent a 8-digit confirmation code to your email"
    And Validate page contains element with text "It will expire shortly, so enter your code soon."
    When User populates the confirmation code "9812" "8366"
#Confirm and validate the email field is validated
    And User click on Confirm button
    Then Validate email field is validated in the Identity Page
    And I close the browser

  @identity @bug-dashboard-741
  Scenario: Import image from Gravatar when the email is not registered in Gravatar
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    And User click on Identity tab
    And User imports image from gravatar
#bug-dashboard-741
#    Then Error Message is presented with text "Your email address doesn't have gravatar image" and message disappears
    And Validate image is not presented
    And Validate image is not presented in the top right of the page
    And I close the browser

  @identity
  Scenario: Import image from Gravatar when the email is registered in Gravatar
    Given I open Chrome browser and navigate to vereign login page
    And Check if gravatar account is registered and register it if not via VIAM API
    And I login with account that is registered in Gravatar
    When User click on Identity tab
    And User removes the image claim if there is such
    And User imports image from gravatar
    Then Confirmation Message is presented with text "Gravatar picture is imported" and message disappears
    And Validate image is presented
    And Validate image is presented in the top right of the page
    And I close the browser

  @identity
  Scenario: Resend confirmation code
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on close tour button
    And User click on Identity tab
    And User select "Communication" option from main Identity select
    And User select "Email" option from secondary Identity select
    And User populates the Identity Email field with random valid email
    And User click on Add to your Identity button
    And User expand all folders
    And Validate Identity email field has the expected value - the email used for registration
    And Validate email field is not validated in the Identity Page
    And User click on validate "Email" claim button
    And I wait for {60000} mseconds
    And User click on send another code button
   And Confirmation Message is presented with text "A new code has been sent." and message disappears
    And Error message is not presented
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button in modal
    And I wait for {5000} mseconds
    And Validate email field is validated in the Identity Page
    And I close the browser

  @identity
  Scenario: Cancel the validation of a claim and validate it after that
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    And User click on close tour button
    And User click on Identity tab
    And User select "Communication" option from main Identity select
    And User select "Email" option from secondary Identity select
    And User populates the Identity Email field with random valid email
    And User click on Add to your Identity button
    And User expand all folders
    And Validate Identity email field has the expected value - the email used for registration
    And Validate email field is not validated in the Identity Page
    And User click on validate "Email" claim button
    And User click on Cancel button in modal
    And I wait for {5000} mseconds
    And User click on validate "Email" claim button
    And Error Message is presented with text "Please wait 60 seconds between tries." and message disappears
    And I wait for {60000} mseconds
    And User click on validate "Email" claim button
    And Error message is not presented
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
    And Validate email field is validated in the Identity Page
    And I close the browser



