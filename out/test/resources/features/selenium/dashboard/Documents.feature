#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard
Feature: Dashboard - Documents

  Background:
    Given we are testing the VIAM Api

  @document
  Scenario: Upload a document with invalid format
    Given I open Chrome browser and navigate to vereign login page
#Create a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
    When User uploads a document in invalid format
#Validate the error message
    Then Error Message is presented with text "Please upload document in docx, pdf or odt format." and message disappears
    And Validate inbox is empty
    And I close the browser

  @document @bug-dashboard-724
  Scenario: Upload a pdf document and validate the Text of the document
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload a valid pdf document
    And User uploads a document in pdf format
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "The document was successfully uploaded with the name pdf" and message disappears
#Validate the pdf document in the Email
    When User open the "1" email
    And Validate the email has the expected subject
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
    And Validate the text of the pdf document is "Test pdf"
    And I close the browser

  @document @delete @bug-dashboard-724
  Scenario Outline: Upload a document with valid <Document_Type> format and delete it
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload document
    And User uploads a document in <Document_Type> format
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "0"
    And Validate the profile in the inbox has value "Social"
#Validate the document in the Email
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "0"
    And Validate the email has the expected subject
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
#Validate participants tab
    And User click on Participants tab
    And Validate there are "1" participants presented in the Participants tab
    And Validate participants with name "Dimitrov Boris" is presented
    And Validate participants with phone "+359884045187" is presented
    And Validate participants with the email of the sender is presented
#Delete the document
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And Validate inbox is empty
    And User click on Browser Back button
    And Title of the page is not "Vereign | 404 Page Not Found"
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      |
#      | odt           | The document was successfully uploaded with the name odt  |
#      | docx          | The document was successfully uploaded with the name docx |
      | pdf           | The document was successfully uploaded with the name pdf  |

  @document @delete @bug-dashboard-724
  Scenario Outline: Upload a <Document_Type> and cancel the deletion
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload document
    And User uploads a document in <Document_Type> format
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
    And User open the "1" email
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User cancel the deletion of the interaction
    And Title of the page is "Vereign | Interaction"
    And User click on Inbox tab
#Validate the document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "0"
    And Validate the profile in the inbox has value "Social"
#Validate the document in the Email
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "0"
    And Validate the email has the expected subject
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      |
#      | odt           | The document was successfully uploaded with the name odt  |
#      | docx          | The document was successfully uploaded with the name docx |
      | pdf           | The document was successfully uploaded with the name pdf  |


  @document @delete @signing @bug-dashboard-724
  Scenario Outline: Sign a <Document_Type> document and delete it
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload a valid pdf document
    And User uploads a document in <Document_Type> format
    #Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the pdf document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the profile in the inbox has value "Social"
#Validate the pdf document in the Email
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "0"
    And Validate the email has the expected subject
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
#Sign the pdf document
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And I wait for {1000} mseconds
    And Validate there are "<Expected_Number_Of_Items>" items in the document thread
#Validate the document
    And Validate the document is signed
    And Validate the status of the document is Signed
    And User downloads the document and validate there are "2" signatures
#Validate the status in Participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
#Delete the document
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And Validate inbox is empty
    And User click on Browser Back button
    And Title of the page is not "Vereign | 404 Page Not Found"
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      | Expected_Number_Of_Items |
#      | odt           | The document was successfully uploaded with the name odt  | 2                        |
#      | docx          | The document was successfully uploaded with the name docx | 2                        |
      | pdf           | The document was successfully uploaded with the name pdf  | 1                        |

  @document @delete @signing @bug-dashboard-724
  Scenario Outline: Sign a <Document_Type> document by 2 Users and delete it from 1
#Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
    And Check if receiver account is registered and register it if not via VIAM API
#    And Check if gmail account is registered and register it if not via VIAM API
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload a valid pdf document
    And User uploads a document in <Document_Type> format
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
    And Validate the subject of the first item is the expected one
#Sign the document
    And User open the "1" email
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And Validate the document is signed
    And Validate the status of the document is Signed
#Request signing
    And User click on the request signing button
    And User populates the request signing search field with random email
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
    And Validate the status of the document is Pending
#Validate the status in Participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
#logout and login with the receivers account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with receiver account
    And User click on Continue button
    And User click on close tour button
#Validate the notifications page
    And User click on Notifications
    And I refresh the current page
    And Validate the notification is presented
#Sign t he document
    And User signs the received signing request
    And User click on Inbox tab
    And I refresh the current page
#Validate the document in the Inbox page
    And Validate the document is presented in the inbox
    And User open the "1" email
    And Validate the email has the expected subject
    And Validate the status of the document is Signed
    And User downloads the document and validate there are "4" signatures
#Participants tab validation
    And User click on Participants tab
    And Validate there are "2" participants presented in the Participants tab
    And Validate participants with the email of the sender is presented
    And Validate participants with the email of the receiver is presented
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is signed
#Delete the document in the Second User
    And User click on the Delete button
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
#Logout from the Second User
    And User logs out of vereign and click on cleanup local identity
#Login with User 1 to validate the document is not deleted
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Validate the document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the profile in the inbox has value "Social"
    And User open the "1" email
#Validate the document in the email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the status of the document is Signed
    And Validate the document is signed
    And Validate the count of the contacts in the email is "1"
    And Validate the email has the expected subject
    And Validate there are "<Expected_Number_Of_Items>" items in the document thread
    And Validate the document is signed
    And User downloads the document and validate there are "4" signatures
    And User click on Participants tab
    And Validate there are "2" participants presented in the Participants tab
    And Validate participants with the email of the sender is presented
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is signed
    And Validate participants with the email of the receiver is presented
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      | Expected_Number_Of_Items |
#      | odt           | The document was successfully uploaded with the name odt  | 2                        |
#      | docx          | The document was successfully uploaded with the name docx | 2                        |
      | pdf           | The document was successfully uploaded with the name pdf  | 1                        |


  @document @signing @bug-dashboard-724
  Scenario Outline: Request signing of a "<Document_Type>" to a registered user
#Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
    And Check if receiver account is registered and register it if not via VIAM API
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload a valid pdf document
    And User uploads a document in <Document_Type> format
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
    And Validate the subject of the first item is the expected one
#Validate the pdf document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the profile in the inbox has value "Social"
#Sign the document
    And User open the "1" email
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And Validate the document is signed
    And Validate the status of the document is Signed
#Request signing
    And User validate there is "1" button for request signing
    And User click on the request signing button
    And User populates the request signing search field with random email
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
    And Validate the status of the document is Pending
#Validate the participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
#logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with receiver account
    And User click on Continue button
    And User click on close tour button
#Validate the notifications page
    And User click on Notifications
    And I refresh the current page
    And Validate the notification is presented
    And Validate the email of the requester is the expected one
#Sign the document
    And User signs the received signing request
    And User click on Inbox tab
    And I refresh the current page
    And Validate the document is presented in the inbox
#Validate the document
    And User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "1"
    And Validate the email has the expected subject
    And Validate there are "<Expected_Number_Of_Items>" items in the document thread
    And Validate the document is signed
    And Validate the status of the document is Signed
    And User downloads the document and validate there are "4" signatures
#Validation of participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is signed
    And Validate there are "2" participants presented in the Participants tab
#Validate the Contact
    When User click on Contacts tab
    And Validate the contact is presented
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      | Expected_Number_Of_Items |
      | odt           | The document was successfully uploaded with the name odt  | 2                        |
      | docx          | The document was successfully uploaded with the name docx | 2                        |
      | pdf           | The document was successfully uploaded with the name pdf  | 1                        |


  @document @signing @bug-dashboard-724
  Scenario Outline: Request signing of a "<Document_Type>" to a not registered user
#Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload a valid pdf document
    And User uploads a document in <Document_Type> format
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the pdf document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the profile in the inbox has value "Social"
#Sign the document
    And User open the "1" email
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And Validate the document is signed
    And Validate the status of the document is Signed
#Request signing
    And User validate there is "1" button for request signing
    And User click on the request signing button
    And Placeholder of the field has value "Add a note"
    And Validate the Important message in the Invite a signatory modal has "Social" profile in it
    And User populates the comment field with value "Request signing to a not registered user"
    And User populates the search field with value "notregistered23@abv.bg"
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
    And Validate the document is signed
    And Validate the status of the document is Pending
#Participants tab validation
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
    And Validate there are "2" participants presented in the Participants tab
    And I wait for {20000} mseconds
    And User navigates to abv and validate the signing request is received
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      |
      | odt           | The document was successfully uploaded with the name odt  |
      | docx          | The document was successfully uploaded with the name docx |
      | pdf           | The document was successfully uploaded with the name pdf  |


  @document @signing @bug-dashboard-724 @notParallel
  Scenario Outline: Request signing of a "<Document_Type>" to two registered users
#Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
    And Check if testing account is registered and register it if not via VIAM API
    And Check if gmail account is registered and register it if not via VIAM API
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload a valid pdf document
    And User uploads a document in <Document_Type> format
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the pdf document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the profile in the inbox has value "Social"
#Sign the document
    And User open the "1" email
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And Validate the document is signed
    And Validate the status of the document is Signed
#Request signing
    And User validate there is "1" button for request signing
    And User click on the request signing button
    And User populates the search field with value "vereign.automation@gmail.com"
    And User populates the search field with value "tested23@abv.bg"
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
#Participants validation
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
    And Validate the status of the third participant is pending
#logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with gmail account
    And User click on Continue button
    And I refresh the current page
#Validate the notifications page
    And User click on Notifications
    And I refresh the current page
    And Validate the notification is presented
    And Validate the email of the requester is the one used to request the signature
#Sign the document
    And User signs the received signing request
    And User click on Inbox tab
    And I refresh the current page
#Validate the document in the inbox
    And Validate the document is presented in the inbox
    And User open the "1" email
#Validate the document in the email page
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "2"
    And Validate the email has the expected subject
    And Validate there are "<Expected_Number_Of_Items>" items in the document thread
    And Validate the document is signed
    And Validate the status of the document is Pending
    And User downloads the document and validate there are "4" signatures
#Validate participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
    And Validate the status of the third participant is signed
#Login with the second account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with testing account
    And User click on Continue button
#Validate the Notification page
    And User click on Notifications
    And I refresh the current page
    And Validate the notification is presented
    And Validate the email of the requester is the one used to request the signature
#Sign the document
    And User signs the received signing request
    And User click on Inbox tab
    And I refresh the current page
    And Validate the document is presented in the inbox
#Validate the document
    And User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "2"
    And Validate the email has the expected subject
    And Validate there are "<Expected_Number_Of_Items>" items in the document thread
    And Validate the document is signed
    And Validate the status of the document is Signed
    And User downloads the document and validate there are "6" signatures
#Validate participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is signed
    And Validate the status of the third participant is signed
    And Validate there are "3" participants presented in the Participants tab
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      | Expected_Number_Of_Items |
      | odt           | The document was successfully uploaded with the name odt  | 2                        |
      | docx          | The document was successfully uploaded with the name docx | 2                        |
      | pdf           | The document was successfully uploaded with the name pdf  | 1                        |

  @document @signing @bug-dashboard-724
  Scenario Outline: Request signing of a "<Document_Type>" to not registered phone number
#Open chrome and load the extension and the profile
    Given I open Chrome browser and navigate to vereign login page
    And Check if testing account is registered and register it if not via VIAM API
    And Check if gmail account is registered and register it if not via VIAM API
    And User navigates to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload a valid pdf document
    And User uploads a document in <Document_Type> format
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the pdf document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the profile in the inbox has value "Social"
#Sign the document
    And User open the "1" email
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And Validate the document is signed
    And Validate the status of the document is Signed
#Request signing
    And User validate there is "1" button for request signing
    And User click on the request signing button
    And User populates the search field with random phone number
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
    And Validate the status of the document is Pending
    And Validate the document is signed
#Validation of participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
#Logout and created the account
    And User logs out of vereign and click on cleanup local identity
    And User registers the not registered receiver with phone claim via VIAM API
    And User logs into vereign with newly created account
    And User click on Continue button
    And User click on close tour button
    And User validate there are "1" number of notifications
    And User click on Notifications
    And Validate the notification is presented
    And Validate the email of the requester is the one used to request the signature
#Sign the document
    And User Signs the document from the Notifications Page
    And User click on Inbox tab
    And I refresh the current page
    And Validate the subject of the first item is the expected one
    And User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "1"
    And Validate the email has the expected subject
    And Validate there are "<Expected_Number_Of_Items>" items in the document thread
    And Validate the document is signed
    And Validate the status of the document is Signed
    And User downloads the document and validate there are "4" signatures
#Validation of participants tab
    And User click on Participants tab
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is signed
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      | Expected_Number_Of_Items |
      | odt           | The document was successfully uploaded with the name odt  | 2                        |
      | docx          | The document was successfully uploaded with the name docx | 2                        |
      | pdf           | The document was successfully uploaded with the name pdf  | 1                        |

  @document @search @bug-dashboard-724
  Scenario Outline: Upload a "<Document_Type>" document and search for it
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload document
    And User uploads a document in <Document_Type> format
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the document is uploaded
    And Validate the subject of the first item is the expected one
#Search by subject
    And User search for the document by the name of the document
#Validation of the Document in the Inobx after search
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "0"
    And Validate the profile in the inbox has value "Social"
#Validation of the Document after search
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "0"
    And Validate the email has the expected subject
    And Validate the status of the document is Unsigned
    And Validate the document is not signed

    Examples:
      | Document_Type | Message_after_upload                                      |
      | odt           | The document was successfully uploaded with the name odt  |
      | docx          | The document was successfully uploaded with the name docx |
      | pdf           | The document was successfully uploaded with the name pdf  |

  @document @search @bug-dashboard-724
  Scenario Outline: Delete a "<Document_Type>" document and search for it
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload document
    And User uploads a document in <Document_Type> format
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the document is uploaded
    And Validate the subject of the first item is the expected one
    And User open the "1" email
    And User click on the Delete button
    And Validate the confirmation message after click on Delete button is with text "Do you want to delete this interaction?"
    And User confirm the deletion of the interaction
    And Title of the page is "Vereign | My Inbox"
    And User search for the document by the name of the document
    And Validate no interactions found message is presented
    And Validate there are "0" items presented in the Inbox
    And User search for the document by the email of the sender
    And Validate no interactions found message is presented
    And Validate there are "0" items presented in the Inbox
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      |
      | odt           | The document was successfully uploaded with the name odt  |
      | docx          | The document was successfully uploaded with the name docx |
      | pdf           | The document was successfully uploaded with the name pdf  |

  @document @search @bug-dashboard-653 @bug-dashboard-724 @bug-dashboard-726
  Scenario Outline: Sign a signing request for a "<Document_Type>" document and search by the subject and email of the sender/receiver
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
#    And Check if gmail account is registered and register it if not via VIAM API
    And Check if receiver account is registered and register it if not via VIAM API
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload document
    And User uploads a document in <Document_Type> format
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
    And User open the "1" email
#Sign the document
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
#Request signing
    And User click on the request signing button
    And User populates the search field with the email of the receiver
#    And User populates the search field with value "vereign.automation@gmail.com"
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
    And User click on Inbox tab
#Search by the name of the receiver
    And User populates the Inbox search field with value "testFirstname"
#Validate no interactions are presented when the document is still not signed from the receiver
    And Validate no interactions found message is presented
    And Validate there are "0" items presented in the Inbox
#Logout and login with the gmail account
    And User logs out of vereign and click on cleanup local identity
    And User logs into vereign with receiver account
    And User click on Continue button
    And User click on close tour button
#Validate the notification is presented
    And User click on Notifications
    And I refresh the current page
    And Validate the notification is presented
#Sign the signing request
    And User signs the received signing request
    And User click on Inbox tab
    And I refresh the current page
    And Validate the document is presented in the inbox
#Search by the subject of the document
    And User search for the document by the name of the document
#Validate the document after the search in the Inbox Page
    And Validate there are "1" items presented in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "1"
#bud-dashboard-726
#    And Validate the profile in the inbox has value "Social"
#Validate the document after the search in the Email page
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "1"
    And Validate the email has the expected subject
#Search by the email of the sender
    And User click on Inbox tab
    And User search for the document by the email of the sender
#Validate the document after the search in the Inbox Page
    And Validate there are "1" items presented in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "1"
#bud-dashboard-726
#    And Validate the profile in the inbox has value "Social"
#Validate the document after the search in the Email page
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "1"
    And Validate the email has the expected subject
#Logout and login with the registered account
    And User logs out of vereign and click on cleanup local identity
    And I login with the registered via the VIAM API user into dashboard
    And User click on Continue button
#Search by the name of the document in the dashboard of the sender after the signing
    And User search for the document by the name of the document
#Validate the document in the Inbox Page after the search
    And Validate there are "1" items presented in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "1"
    And Validate the profile in the inbox has value "Social"
#Validate the document in the Email Page after the search
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "1"
    And Validate the email has the expected subject
#Search by the email of the receiver
    And User click on Inbox tab
    And User search for the document by the email of the receiver
#Validate the document in the Inbox Page after the search
    And Validate there are "1" items presented in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "1"
    And Validate the profile in the inbox has value "Social"
#Validate the document in the Email Page after the search
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "Social"
    And Validate the count of the contacts in the email is "1"
    And Validate the email has the expected subject
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      |
      | odt           | The document was successfully uploaded with the name odt  |
      | docx          | The document was successfully uploaded with the name docx |
      | pdf           | The document was successfully uploaded with the name pdf  |

  @document
  Scenario Outline: Upload a document in "<Document_Type>" format from a new profile and request signing
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Upload document
    And User click on Profiles tab
    And User creates new profile with name "TestNew"
    And User click on Inbox tab
    And User upload a document in "<Document_Type>" format from "TestNew" profile
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "0"
    And Validate the profile in the inbox has value "TestNew"
#Validate the document in the Email
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "TestNew"
    And Validate the count of the contacts in the email is "0"
    And Validate the email has the expected subject
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
#Sign the document
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And Validate the document is signed
    And Validate the status of the document is Signed
#Request signing in order to validate the new profile is presented in the Important message
    And User click on the request signing button
    And Placeholder of the field has value "Add a note"
    And Validate the Important message in the Invite a signatory modal has "TestNew" profile in it
    And User populates the comment field with value "Request signing to a not registered user"
    And User populates the search field with value "notregistered23@abv.bg"
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
    And Validate the document is signed
    And Validate the status of the document is Pending
#Validate participants tab
    And User click on Participants tab
    And Validate there are "2" participants presented in the Participants tab
    And Validate participants with name "Dimitrov Boris" is presented
    And Validate participants with phone "+359884045187" is presented
    And Validate participants with the email of the sender is presented
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
    And I close the browser

    Examples:
      | Document_Type | Message_after_upload                                      |
      | odt           | The document was successfully uploaded with the name odt  |
      | docx          | The document was successfully uploaded with the name docx |
      | pdf           | The document was successfully uploaded with the name pdf  |

  @document
  Scenario Outline: Upload a "<Document_Type>" document from "<Profile_Name>" profile and sign it
    Given I open Chrome browser and navigate to vereign login page
#Register a new random user
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
    And User upload a document in "<Document_Type>" format from "<Profile_Name>" profile
#Validate the message
#@bug-dashboard-724
#    Then Confirmation Message is presented with text that contains "<Message_after_upload>" and message disappears
#Validate the document in the Inbox
    And Validate the subject of the first item is the expected one
    And Validate the Interaction Type in the inbox has value "Document"
    And Validate the count of the contacts in the inbox is "0"
    And Validate the profile in the inbox has value "<Profile_Name>"
#Validate the document in the Email
    When User open the "1" email
    Then Validate the Interaction Type in the email has value "Document"
    And Validate the profile in the email has value "<Profile_Name>"
    And Validate the count of the contacts in the email is "0"
    And Validate the email has the expected subject
    And Validate the document is not signed
    And Validate the status of the document is Unsigned
#Sign the document
    And User click on the signature icon to sign the document
    And User click "Social" profile from the presented profiles
    And User click on Sign Button to finish up the signing the document
    And Validate the document is signed
    And Validate the status of the document is Signed
#Request signing in order to validate the profile in the important message
    And User click on the request signing button
    And Placeholder of the field has value "Add a note"
    And Validate the Important message in the Invite a signatory modal has "<Profile_Name>" profile in it
    And User populates the comment field with value "Request signing to a not registered user"
    And User populates the search field with value "notregistered23@abv.bg"
    And User click on Send invite button
    And User validate the confirmation message after the sending signing request
    And User click Finish button
    And Validate the document is signed
    And Validate the status of the document is Pending
#Validate participants tab
    And User click on Participants tab
    And Validate there are "2" participants presented in the Participants tab
    And Validate participants with name "Dimitrov Boris" is presented
    And Validate participants with phone "+359884045187" is presented
    And Validate participants with the email of the sender is presented
    And Validate the status of the first participant is signed
    And Validate the status of the second participant is pending
    And I close the browser

    Examples:
      | Document_Type | Profile_Name | Message_after_upload                                      |
      | odt           | Social       | The document was successfully uploaded with the name odt  |
      | docx          | Friends      | The document was successfully uploaded with the name docx |
      | pdf           | Email        | The document was successfully uploaded with the name pdf  |






