#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard
Feature: Dashboard - Profiles

  Background:
    Given we are testing the VIAM Api

  @profiles
  Scenario: Validate default profiles values
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
    And User click on Profiles tab
    And Privacy policy and Beta Participation Agreement Links have the correct text and href
#Profiles tab
    Then Validate "3" profiles are presented
#Validate the names of the profiles
    And Validate profile with name "Social" is presented
    And Validate profile with name "Email" is presented
    And Validate profile with name "Friends" is presented
#Validate Social Profile
    When User click on profile with name "Social"
#Validate the number of the claims
    Then Validate the number of claims in the profile is "3"
#Validate the labels of the claims
    And Validate claim "Email" is presented under the profile
    And Validate claim "Full  name" is presented under the profile
    And Validate claim "Phone" is presented under the profile
#Validate the values of the claims
    And Validate Profile email field has the expected value - the email used for registration
    When User expand all sub folders
    Then Validate Profile claim with label "Family name" has value "Dimitrov"
    And Validate Profile claim with label "Given name" has value "Boris"
    And Validate Profile claim with label "Full  name" has value "Dimitrov, Boris"
    And Validate Profile claim with label "Phone" has value "+359884045187"
    When User click on Profiles tab
#Validate Email Profile
    And User click on profile with name "Email"
#Validate the number of claims presented
    Then Validate the number of claims in the profile is "2"
#Validate the label of the claims
    And Validate claim "Email" is presented under the profile
    And Validate claim "Full  name" is presented under the profile
#Validate Phone label is presented in the Identity not in the profile
    And Validate Identity claim with label "Phone" has value "+359884045187"
#Validate the values of the claims
    And Validate Profile email field has the expected value - the email used for registration
    When User expand all sub folders
    Then Validate Profile claim with label "Family name" has value "Dimitrov"
    Then Validate Profile claim with label "Given name" has value "Boris"
    When User click on Profiles tab
#Validate Friends Profile
    And User click on profile with name "Friends"
#Validate the number of claims
    Then Validate the number of claims in the profile is "3"
    And Validate claim "Email" is presented under the profile
    And Validate claim "Full  name" is presented under the profile
    And Validate claim "Phone" is presented under the profile
#Validate the values of the claims
    And Validate Profile email field has the expected value - the email used for registration
    When User expand all sub folders
    Then Validate Profile claim with label "Family name" has value "Dimitrov"
    And Validate Profile claim with label "Given name" has value "Boris"
    And Validate Profile claim with label "Phone" has value "+359884045187"
    And I close the browser

  @profiles
  Scenario: Delete a claim that is added to a profile
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
#Adding the claim to the identity
    And User click on Identity tab
    And User select "Language" option from main Identity select
    And User select "Bulgarian" from the Language select
    And User click on Add to your Identity button
    And User upload a photo with valid format to the identity
    Then Validate image is presented
    And User click on Profiles tab
    And User click on profile with name "Social"
#Validate the Language claim is presented in the Identity tab
    Then Validate Identity claim with label "Language" has value "Bulgarian"
##Add the claim to the profile
    When User add the "Language" claim to the profile
#Validate that the claim is added to the profile
    Then Validate Profile claim with label "Language" has value "Bulgarian"
#Validate that the claim is not presented on right
    And Validate "Language" claim is deleted from the Identity page
    When User click on Identity tab
#Delete the claim
    And User click on delete "Language" claim button
    And User confirms the deletion
    Then Validate "Language" claim is deleted from the Identity page
    And User click on Profiles tab
    And User click on profile with name "Social"
#Validate the claim is deleted in the profile
    Then Validate "Language" claim is deleted from the Profile page
    And Validate "Language" claim is deleted from the Identity page
#Add the image claim to the profile
    When User click on Profiles tab
    And User click on profile with name "Friends"
    And I wait for {700} mseconds
    And User Adds the image claim in the profile
    And Validate image is presented in the current profile
#Remove the image claim from the identity
    And User click on Identity tab
    And User removes the image claim from the identity
#Validate the Friends profile dont have image claim
    And User click on Profiles tab
    Then Validate image is not presented in the "Friends" profile
    And User click on profile with name "Friends"
    And Validate image is not presented in the current profile
    And I close the browser

  @profiles
  Scenario: Add and Remove Image claim to the profile
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    And User click on close tour button
    When User click on Identity tab
#Adding a image claim to the Identity
    And User upload a photo with valid format to the identity
    Then Validate image is presented
    When User click on Profiles tab
#Validate there is no image claim to the profiles
    And I wait for {2000} mseconds
    Then Validate image is not presented in the "Social" profile
    And Validate image is not presented in the "Email" profile
    And Validate image is not presented in the "Friends" profile
#Adding the Image claim to the profile
    When User click on profile with name "Social"
    Then Validate image is not presented in the current profile
    And User Adds the image claim in the profile
    And I wait for {1000} mseconds
#Validate the image is presented in the current profile
    Then Validate image is presented in the current profile
    When User click on Profiles tab
#Validate image is presented in the Social profile
    Then Validate image is presented in the "Social" profile
    And Validate image is not presented in the "Email" profile
    And Validate image is not presented in the "Friends" profile
    When User click on profile with name "Social"
#Remove the image claim from the profile
    And User Removes the image claim in the profile
#Validate the image is removed from the profile
    Then Validate image is not presented in the current profile
    When User click on Profiles tab
    Then Validate image is not presented in the "Social" profile
    And Validate image is not presented in the "Email" profile
    And Validate image is not presented in the "Friends" profile
    And I close the browser

  @profiles
  Scenario: Create new profile and add claims
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
#Adding new claim to the identity
    And User click on Identity tab
    And User select "Language" option from main Identity select
    And User select "Bulgarian" from the Language select
    And User click on Add to your Identity button
    And User click on Profiles tab
#Create new Profile
    And User creates new profile with name "Local"
#Validate the profiles are 4
    Then Validate "4" profiles are presented
#Validate the newly created profile is presented
    And Validate profile with name "Local" is presented
    When User click on profile with name "Local"
    Then Validate the number of claims in the profile is "0"
#Add the claims to the profile and validate them
    And User add the "Language" claim to the profile
    Then Validate claim "Language" is presented under the profile
#Validate the value of the Language
    When User add the "Email" claim to the profile
    Then Validate claim "Email" is presented under the profile
    When User add the "Full  name" claim to the profile
    Then Validate claim "Full  name" is presented under the profile
    When User add the "Phone" claim to the profile
    Then Validate claim "Phone" is presented under the profile
    And I close the browser

  @profiles
  Scenario: Add claims to profile and Edit them in the Identity
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email and add all claims via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in and claims added to the identity
    When User click on close tour button
    And User click on Profiles tab
#Select profile
    And User click on profile with name "Social"
#Add Address claim to the profile
    And User add the "Address" claim to the profile
    Then Validate claim "Address" is presented under the profile
#Add Chat claim to the profile
    When User add the "Chat" claim to the profile
    Then Validate claim "Chat" is presented under the profile
#Add Date of birth claim to the profile
    When User add the "Date of  birth" claim to the profile
    Then Validate claim "Date of  birth" is presented under the profile
#Add Email claim to the profile
    When User add the "Email" claim to the profile
    Then Validate claim "Emails" is presented under the profile
#Add Gender claim to the profile
    When User add the "Gender" claim to the profile
    Then Validate claim "Gender" is presented under the profile
#Add Language claim to the profile
    When User add the "Language" claim to the profile
    Then Validate claim "Language" is presented under the profile
#Add Name claim to the profile
    When User add the "Full  name" claim to the profile
    Then Validate claim "Full  name" is presented under the profile
#Add Nickname claim to the profile
    When User add the "Nickname" claim to the profile
    Then Validate claim "Nickname" is presented under the profile
#Add Phone claim to the profile
    When User add the "Phone" claim to the profile
    Then Validate claim "Phones" is presented under the profile
#Add Social Claim to the profile
    When User add the "Social" claim to the profile
    Then Validate claim "Social" is presented under the profile
#Expand all folders
    When User expand all folders
#Validate Address claim in the profile
    Then Validate Profile claim with label "Country" has value "Bulgaria"
    Then Validate Profile claim with label "Locality" has value "Sofia"
    Then Validate Profile claim with label "Postal code" has value "1777"
    Then Validate Profile claim with label "Region" has value "Sofia"
    Then Validate Profile claim with label "Street address" has value "G.M Dimitrov"
#Validate Chat claim in the profile
    Then Validate Profile claim with label "Chat service name" has value "Mirc"
    Then Validate Profile claim with label "User" has value "testingUser"
#Validate Date of birth claim in the profile
    Then Validate Profile claim with label "Date of  birth" has value "Jan 01 2000"
#Validate Email claim in the profile
    Then Validate Profile claim with label "Email" has value "tested23@abv.bg"
#Validate Name claim in the profile
    Then Validate Profile claim with label "Given name" has value "Ivan"
    Then Validate Profile claim with label "Family name" has value "Petrov"
#Validate Nickname claim in the profile
    Then Validate Profile claim with label "Nickname" has value "myNickname"
#Validate Phone claim in the profile
    Then Validate Profile claim with label "Phone" has value "+359883353610"
#Validate Social claim in the profile
    Then Validate Profile claim with label "Social network" has value "LinkedIn"
    Then Validate Profile claim with label "Url" has value "www.localhost.com"
#Open the Identity
    When User click on Identity tab
#Edit the Address Claim in the identity page
    When User click on edit "Address" claim button
    And User populates the field with placeholder "Street address" with value "G.M Dimitrov Edited"
    And User populates the field with placeholder "Locality" with value "Sofia Edited"
    And User populates the field with placeholder "Region" with value "Sofia Edited"
    And User populates the field with placeholder "Postal code" with value "1777 Edited"
    And User populates the field with placeholder "Country" with value "Bulgaria Edited"
    And User click on Update claim button
#Edit the Chat claim in the identity page
    When User click on edit "Chat" claim button
    And User populates the field with placeholder "Chat Service" with value "Mirc Edited"
    And User populates the field with placeholder "Number or Username" with value "testingUser Edited"
    And User click on Update claim button
#Edit the Date of Birth claim in the identity page
    When User click on edit "Date of  birth" claim button
    And User populates the date of birth "01/01/2001"
    And User click on Update claim button
#Expand all folders
    And User expand all folders
#Edit the Email claim in the identity page
    When User click on edit "Email" claim button
    And User populates the field with placeholder "Type your email here" with value "tested23Edited@mail.bg"
    And User click on Update claim button
#Edit the Gender claim in the identity page
    When User click on edit "Gender" claim button
    And User select random Gender from the select
    And User click on Update claim button
#Edit the Language claim in the identity page
    When User click on edit "Language" claim button
    And User select "Bulgarian" from the Language select
    And User click on Update claim button
#Edit Full Name claim in the identity page
    When User click on edit "Full  name" claim button
    And User populates the field with placeholder "First name" with value "Ivan Edited"
    And User populates the field with placeholder "Last name" with value "Petrov Edited"
    And User click on Update claim button
#Edit Nickname claim in the identity page
    When User click on edit "Nickname" claim button
    And User populates the field with placeholder "Type your nickname here" with value "myNickname Edited"
    And User click on Update claim button
#Edit the Phone claim in the identity page
    When User click on edit "Phone" claim button
    And User populates the field with placeholder "Type your phone number here" with value "+359884045187"
    And User click on Update claim button
#Edit the Social Claim in the identity page
    When User click on edit "Social" claim button
    And User populates the field with placeholder "Url" with value "www.localhost.com/edited"
    And User click on Update claim button
#Open profiles tab
    And User click on Profiles tab
    And User click on profile with name "Social"
#Validate the edited claim in the Social profile
    When User expand all folders
#Validate Address claim in the profile
    Then Validate Profile claim with label "Country" has value "Bulgaria Edited"
    Then Validate Profile claim with label "Locality" has value "Sofia Edited"
    Then Validate Profile claim with label "Postal code" has value "1777 Edited"
    Then Validate Profile claim with label "Region" has value "Sofia Edited"
    Then Validate Profile claim with label "Street address" has value "G.M Dimitrov Edited"
#Validate Chat claim in the profile
    Then Validate Profile claim with label "Chat service name" has value "Mirc Edited"
    Then Validate Profile claim with label "User" has value "testingUser Edited"
#Validate Date of birth claim in the profile
    Then Validate Profile claim with label "Date of  birth" has value "Jan 01 2001"
#Validate Email claim in the profile
    Then Validate Profile claim with label "Email" has value "tested23Edited@mail.bg"
#Validate Name claim in the profile
    Then Validate Profile claim with label "Given name" has value "Ivan Edited"
    Then Validate Profile claim with label "Family name" has value "Petrov Edited"
#Validate Nickname claim in the profile
    Then Validate Profile claim with label "Nickname" has value "myNickname Edited"
#Validate Phone claim in the profile
    Then Validate Profile claim with label "Phone" has value "+359884045187"
#Validate Social claim in the profile
    Then Validate Profile claim with label "Social network" has value "LinkedIn"
    Then Validate Profile claim with label "Url" has value "www.localhost.com/edited"
    And I close the browser

  @profiles
  Scenario:Validate Validated/Not Validated claims in the profile
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
    And User click on Profiles tab
#Social profile opened
    And User click on profile with name "Social"
#Validate email is validated and phone is not validated
    Then Validate email field is validated in the Profile Page
    And Validate phone field is not validated in the Profile Page
#Edit the email
    When User click on Identity tab
    And User click on edit "Email" claim button
    And User populates the Identity Email field with random valid email
    And User click on Update claim button
#Validate email claim is not validated in the profile
    When User click on Profiles tab
    And User click on profile with name "Social"
    Then Validate email field is not validated in the Profile Page
    And Validate Profile email field has the expected value - the email used for registration
    And Validate phone field is not validated in the Profile Page
#Edit the phone
    When User click on Identity tab
    And User click on edit "Phone" claim button
    And User populates the Phone field with random value
    And User click on Update claim button
#Validate the phone
    And User click on validate "Phone" claim button
    And User populates the confirmation code "9812" "8366"
    And User click on Confirm button
    And Validate warning icon is not presented in the "Phone" claim
    And User click on Profiles tab
#Open the Social profile
    And User click on profile with name "Social"
#Validate phone field is validated in the profile
    Then Validate email field is not validated in the Profile Page
    And Validate phone field is validated in the Profile Page
    And I close the browser

  @profiles
  Scenario: Edit the name of default and newly created profile
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
#Logged in
    When User click on close tour button
    And User click on Profiles tab
#Social profile opened - Edit Default profile
    And User click on profile with name "Social"
    And User click on edit profile button
    And User populates profile name field with value "SocialEdited" and Save
#Validate the edited profile
    Then Confirmation Message is presented with text "The name of this profile has been updated to SocialEdited" and message disappears
    Then Validate the name of the profile is "SocialEdited"
    When User click on Profiles tab
    Then Validate profile with name "SocialEdited" is presented
    And Validate "3" profiles are presented
#Create new profile
    When User creates new profile with name "Test"
    And User click on profile with name "Test"
#Edit the profile
    And I wait for {1000} mseconds
    And User click on edit profile button
    And User populates profile name field with value "TestEdited" and Save
#Validate the edited profile
    Then Confirmation Message is presented with text "The name of this profile has been updated to TestEdited" and message disappears
    And Validate the name of the profile is "TestEdited"
    When User click on Profiles tab
    Then Validate profile with name "TestEdited" is presented
    And Validate "4" profiles are presented
    And I close the browser