#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#Author: Boris Dimitrov boris.dimitrov@vereign.com

@selenium @all @dashboard
Feature: Dashboard - Device Information

  Background:
    Given we are testing the VIAM Api

  @deviceManager
  Scenario: Register a new user and validate his current device information
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Logged in
    And User click on device manager button
#Validate Devices
    Then Validate "1" devices are presented
    And Validate Device name field has value "Linux x86_64 | Chrome |" after registration
    And Validate Device type field has value "Linux x86_64"
    And Validate Device ID field is not empty
    And Validate Browser field is not empty
    And Validate Created On field is not empty
    And Validate Last Accessed field is not empty
#Validate QR Code labels
    When User click on Add new device button
    And Validate QR code info label has the expected text
    Then Validate QR code is presented and close it
    And I close the browser

  @deviceManager
  Scenario: Rename and revoke device
    Given I open Chrome browser and navigate to vereign login page
    And I register a new user with email via VIAM API
    And I login with the registered via the VIAM API user into dashboard
    When User click on close tour button
#Logged in
    And User click on device manager button
#Rename Device
    And User hovers over the options button on the first device
    And User click on Rename device button
    And User populates the new device name "Device edited" and Save
    And User click on Profiles tab
    And User click on device manager button
    Then Validate Device name field has value "Device edited"
#Revoke Device
    And User hovers over the options button on the first device
    And User click on Revoke device button
    And User confirms the deletion
    Then Validate user is navigated to login page
    And I close the browser

