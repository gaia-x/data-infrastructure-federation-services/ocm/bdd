/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package api.test.rest;

import api.test.rest.pojo.connection.Connection;
import api.test.rest.pojo.credential.Credential;
import api.test.rest.pojo.credentialDef.CredentialDef;
import api.test.rest.pojo.presentationRequest.Presentation;
import api.test.rest.pojo.schema.Schema;

public class RestSessionContainer {

    private Schema currentSchema;
    private CredentialDef currentCredentialDef;
    private Connection currentConnection;
    private Presentation currentPresentation;

    private Record currentCredential;

    private Credential currentCredentialData;

    public Presentation getCurrentPresentation() {
        return currentPresentation;
    }

    public void setCurrentPresentation(Presentation currentPresentation) {
        this.currentPresentation = currentPresentation;
    }

    public Connection getCurrentConnection() {
        return currentConnection;
    }

    public void setCurrentConnection(Connection currentConnection) {
        this.currentConnection = currentConnection;
    }

    public CredentialDef getCurrentCredentialDef() {
        return currentCredentialDef;
    }

    public void setCurrentCredentialDef(CredentialDef currentCredentialDef) {
        this.currentCredentialDef = currentCredentialDef;
    }

    public Schema getCurrentSchema() {
        return currentSchema;
    }

    public void setCurrentSchema(Schema currentSchema) {
        this.currentSchema = currentSchema;
    }

    public Record getCurrentCredential() {
        return currentCredential;
    }

    public void setCurrentCredential(Record currentCredential) {
        this.currentCredential = currentCredential;
    }

    public Credential getCurrentCredentialData() {
        return currentCredentialData;
    }

    public void setCurrentCredentialData(Credential currentCredentialData) {
        this.currentCredentialData = currentCredentialData;
    }
}

