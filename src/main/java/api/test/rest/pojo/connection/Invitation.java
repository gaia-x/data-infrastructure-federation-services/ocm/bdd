
package api.test.rest.pojo.connection;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Invitation {

    @SerializedName("@id")
    private String id;
    @Expose
    private String label;
    @Expose
    private List<String> recipientKeys;
    @Expose
    private List<Object> routingKeys;
    @Expose
    private String serviceEndpoint;
    @SerializedName("@type")
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getRecipientKeys() {
        return recipientKeys;
    }

    public void setRecipientKeys(List<String> recipientKeys) {
        this.recipientKeys = recipientKeys;
    }

    public List<Object> getRoutingKeys() {
        return routingKeys;
    }

    public void setRoutingKeys(List<Object> routingKeys) {
        this.routingKeys = routingKeys;
    }

    public String getServiceEndpoint() {
        return serviceEndpoint;
    }

    public void setServiceEndpoint(String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
