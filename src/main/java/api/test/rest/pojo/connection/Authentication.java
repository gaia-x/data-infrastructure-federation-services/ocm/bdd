
package api.test.rest.pojo.connection;


import com.google.gson.annotations.Expose;


public class Authentication {

    @Expose
    private String publicKey;
    @Expose
    private String type;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
