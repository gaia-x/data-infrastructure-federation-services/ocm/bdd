
package api.test.rest.pojo.connection;

import java.util.List;
import com.google.gson.annotations.Expose;


public class Service {

    @Expose
    private String id;
    @Expose
    private Long priority;
    @Expose
    private List<String> recipientKeys;
    @Expose
    private List<Object> routingKeys;
    @Expose
    private String serviceEndpoint;
    @Expose
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public List<String> getRecipientKeys() {
        return recipientKeys;
    }

    public void setRecipientKeys(List<String> recipientKeys) {
        this.recipientKeys = recipientKeys;
    }

    public List<Object> getRoutingKeys() {
        return routingKeys;
    }

    public void setRoutingKeys(List<Object> routingKeys) {
        this.routingKeys = routingKeys;
    }

    public String getServiceEndpoint() {
        return serviceEndpoint;
    }

    public void setServiceEndpoint(String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
