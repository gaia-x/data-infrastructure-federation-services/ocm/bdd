
package api.test.rest.pojo.connection;

import com.google.gson.annotations.Expose;

public class PublicKey {

    @Expose
    private String controller;
    @Expose
    private String id;
    @Expose
    private String publicKeyBase58;
    @Expose
    private String type;

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPublicKeyBase58() {
        return publicKeyBase58;
    }

    public void setPublicKeyBase58(String publicKeyBase58) {
        this.publicKeyBase58 = publicKeyBase58;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
