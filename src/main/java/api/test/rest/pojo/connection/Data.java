
package api.test.rest.pojo.connection;


import com.google.gson.annotations.Expose;


public class Data {

    @Expose
    private Connection connection;
    @Expose
    private Invitation invitation;
    @Expose
    private String invitationUrl;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public String getInvitationUrl() {
        return invitationUrl;
    }

    public void setInvitationUrl(String invitationUrl) {
        this.invitationUrl = invitationUrl;
    }

}
