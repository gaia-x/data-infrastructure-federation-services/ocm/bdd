
package api.test.rest.pojo.connection;


import com.google.gson.annotations.Expose;


public class Connection {

    @Expose
    private api.test.rest.pojo.connection._tags _tags;
    @Expose
    private String alias;
    @Expose
    private String createdAt;
    @Expose
    private Data data;
    @Expose
    private String did;
    @Expose
    private DidDoc didDoc;
    @Expose
    private String id;
    @Expose
    private Invitation invitation;
    @Expose
    private String message;
    @Expose
    private Metadata metadata;
    @Expose
    private Boolean multiUseInvitation;
    @Expose
    private String role;
    @Expose
    private String state;
    @Expose
    private Long statusCode;
    @Expose
    private String verkey;

    public api.test.rest.pojo.connection._tags get_tags() {
        return _tags;
    }

    public void set_tags(api.test.rest.pojo.connection._tags _tags) {
        this._tags = _tags;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public DidDoc getDidDoc() {
        return didDoc;
    }

    public void setDidDoc(DidDoc didDoc) {
        this.didDoc = didDoc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Boolean getMultiUseInvitation() {
        return multiUseInvitation;
    }

    public void setMultiUseInvitation(Boolean multiUseInvitation) {
        this.multiUseInvitation = multiUseInvitation;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Long statusCode) {
        this.statusCode = statusCode;
    }

    public String getVerkey() {
        return verkey;
    }

    public void setVerkey(String verkey) {
        this.verkey = verkey;
    }

}
