
package api.test.rest.pojo.connection;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DidDoc {

    @Expose
    private List<Authentication> authentication;
    @SerializedName("@context")
    private String context;
    @Expose
    private String id;
    @Expose
    private List<PublicKey> publicKey;
    @Expose
    private List<Service> service;

    public List<Authentication> getAuthentication() {
        return authentication;
    }

    public void setAuthentication(List<Authentication> authentication) {
        this.authentication = authentication;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<PublicKey> getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(List<PublicKey> publicKey) {
        this.publicKey = publicKey;
    }

    public List<Service> getService() {
        return service;
    }

    public void setService(List<Service> service) {
        this.service = service;
    }

}
