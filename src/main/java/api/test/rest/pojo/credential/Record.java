
package api.test.rest.pojo.credential;

import com.google.gson.annotations.SerializedName;

public class Record {

    @SerializedName("connectionId")
    private String mConnectionId;
    @SerializedName("createdDate")
    private String mCreatedDate;
    @SerializedName("credDefId")
    private String mCredDefId;
    @SerializedName("credentialId")
    private String mCredentialId;
    @SerializedName("expirationDate")
    private Object mExpirationDate;
    @SerializedName("id")
    private String mId;
    @SerializedName("principalDid")
    private String mPrincipalDid;
    @SerializedName("state")
    private String mState;
    @SerializedName("threadId")
    private String mThreadId;
    @SerializedName("updatedDate")
    private String mUpdatedDate;

    public String getConnectionId() {
        return mConnectionId;
    }

    public void setConnectionId(String connectionId) {
        mConnectionId = connectionId;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getCredDefId() {
        return mCredDefId;
    }

    public void setCredDefId(String credDefId) {
        mCredDefId = credDefId;
    }

    public String getCredentialId() {
        return mCredentialId;
    }

    public void setCredentialId(String credentialId) {
        mCredentialId = credentialId;
    }

    public Object getExpirationDate() {
        return mExpirationDate;
    }

    public void setExpirationDate(Object expirationDate) {
        mExpirationDate = expirationDate;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getPrincipalDid() {
        return mPrincipalDid;
    }

    public void setPrincipalDid(String principalDid) {
        mPrincipalDid = principalDid;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getThreadId() {
        return mThreadId;
    }

    public void setThreadId(String threadId) {
        mThreadId = threadId;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        mUpdatedDate = updatedDate;
    }

}
