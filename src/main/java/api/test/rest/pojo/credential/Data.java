
package api.test.rest.pojo.credential;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("count")
    private Long mCount;
    @SerializedName("records")
    private List<Record> mRecords;

    public Long getCount() {
        return mCount;
    }

    public void setCount(Long count) {
        mCount = count;
    }

    public List<Record> getRecords() {
        return mRecords;
    }

    public void setRecords(List<Record> records) {
        mRecords = records;
    }

}
