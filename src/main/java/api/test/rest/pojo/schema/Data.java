
package api.test.rest.pojo.schema;

import java.util.List;
import com.google.gson.annotations.Expose;

public class Data {

    @Expose
    private List<Attribute> attribute;
    @Expose
    private String createdBy;
    @Expose
    private String createdDate;
    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private String schemaID;
    @Expose
    private String updatedBy;
    @Expose
    private String updatedDate;

    public List<Attribute> getAttribute() {
        return attribute;
    }

    public void setAttribute(List<Attribute> attribute) {
        this.attribute = attribute;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchemaID() {
        return schemaID;
    }

    public void setSchemaID(String schemaID) {
        this.schemaID = schemaID;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
