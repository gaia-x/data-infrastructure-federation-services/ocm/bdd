
package api.test.rest.pojo.schema;

import com.google.gson.annotations.Expose;

public class Attribute {

    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
