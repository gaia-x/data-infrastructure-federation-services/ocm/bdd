
package api.test.rest.pojo.presentationRequest;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("connectionId")
    private String mConnectionId;
    @SerializedName("createdDate")
    private String mCreatedDate;
    @SerializedName("credentialDefId")
    private String mCredentialDefId;
    @SerializedName("id")
    private String mId;
    @SerializedName("proofRecordId")
    private String mproofRecordId;
    @SerializedName("presentationMessage")
    private String mPresentationMessage;
    @SerializedName("schemaId")
    private String mSchemaId;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("theirDid")
    private String mTheirDid;
    @SerializedName("updatedDate")
    private String mUpdatedDate;

    public String getConnectionId() {
        return mConnectionId;
    }

    public void setConnectionId(String connectionId) {
        mConnectionId = connectionId;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getCredentialDefId() {
        return mCredentialDefId;
    }

    public void setCredentialDefId(String credentialDefId) {
        mCredentialDefId = credentialDefId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getProofRecordId() {
        return mproofRecordId;
    }

    public void setProofRecordId(String proofRecordId) {
        mproofRecordId = proofRecordId;
    }

    public String getPresentationMessage() {
        return mPresentationMessage;
    }

    public void setPresentationMessage(String presentationMessage) {
        mPresentationMessage = presentationMessage;
    }

    public String getSchemaId() {
        return mSchemaId;
    }

    public void setSchemaId(String schemaId) {
        mSchemaId = schemaId;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTheirDid() {
        return mTheirDid;
    }

    public void setTheirDid(String theirDid) {
        mTheirDid = theirDid;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        mUpdatedDate = updatedDate;
    }

}
