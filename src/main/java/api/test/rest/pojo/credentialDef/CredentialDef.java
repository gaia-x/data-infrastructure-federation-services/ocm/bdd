
package api.test.rest.pojo.credentialDef;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class CredentialDef {

    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private Long statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Long statusCode) {
        this.statusCode = statusCode;
    }

}
