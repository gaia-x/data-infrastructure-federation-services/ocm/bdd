
package api.test.rest.pojo.credentialDef;

import com.google.gson.annotations.Expose;


public class Data {

    @Expose
    private String createdBy;
    @Expose
    private String createdDate;
    @Expose
    private String credDefId;
    @Expose
    private String expiryHours;
    @Expose
    private String id;
    @Expose
    private Boolean isAutoIssue;
    @Expose
    private Boolean isRevokable;
    @Expose
    private String name;
    @Expose
    private String schemaID;
    @Expose
    private String updatedBy;
    @Expose
    private String updatedDate;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCredDefId() {
        return credDefId;
    }

    public void setCredDefId(String credDefId) {
        this.credDefId = credDefId;
    }

    public String getExpiryHours() {
        return expiryHours;
    }

    public void setExpiryHours(String expiryHours) {
        this.expiryHours = expiryHours;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsAutoIssue() {
        return isAutoIssue;
    }

    public void setIsAutoIssue(Boolean isAutoIssue) {
        this.isAutoIssue = isAutoIssue;
    }

    public Boolean getIsRevokable() {
        return isRevokable;
    }

    public void setIsRevokable(Boolean isRevokable) {
        this.isRevokable = isRevokable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchemaID() {
        return schemaID;
    }

    public void setSchemaID(String schemaID) {
        this.schemaID = schemaID;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
