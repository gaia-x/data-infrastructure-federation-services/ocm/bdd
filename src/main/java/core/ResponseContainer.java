/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package core;

import core.*;

import java.util.ArrayList;
import java.util.List;

public class ResponseContainer {
    private List<Response> responseList = new ArrayList<>();
    private static ResponseContainer instance = new ResponseContainer();

    private ResponseContainer() {}

    public static ResponseContainer getInstance() {
        return instance;
    }

    public void addResponse(Response response) {
        responseList.add(response);
    }

    public Response getResponse(int index) {
        return responseList.get(index);
    }

    public Response getLastResponse() {
        if(!responseList.isEmpty())
            return responseList.get(responseList.size() - 1);
        else
            return null;
    }

    public void clearContainer() {
        responseList.clear();
    }
}
