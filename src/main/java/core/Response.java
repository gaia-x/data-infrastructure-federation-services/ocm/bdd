/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package core;


import io.restassured.http.Header;
import io.restassured.http.Headers;

import java.util.HashMap;

/**
 * Class used to store the response received.
 * It only stores the status code, the response body and the headers
 */
public class Response {
    private int statusCode;
    private String body;
    private HashMap<String, String> headers;

    public Response(int statusCode, String body, Headers headers) {
        this.statusCode = statusCode;
        this.body = body;
        this.headers = new HashMap<>();
        for (Header header : headers)
            this.headers.put(header.getName(), header.getValue());
    }

    /**
     * Get the headers of the response
     *
     * @return the headers hashmap
     */
    public HashMap<String, String> getHeaders() {
        return headers;
    }

    /**
     * Get the status code of the response
     *
     * @return the status code of the response
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Get the body of the response
     *
     * @return the body of the response
     */
    public String getBody() {
        return body;
    }

//    @Override
//    public String toString() {
//
//        return String.format("----Response----\nStatus code: %d\nHeaders:%s\nBody:\n%s\n" +
//                        "\n-------------------------------------------------------------",
//                statusCode, JsonUtils.prettyPrintMap(headers),
//                (headers.get("Content-Type") != null ? headers.get("Content-Type") : "null")
//                        .contains("application/json") ? JsonUtils.prettyPrintJson(body) : body);
//    }
}
