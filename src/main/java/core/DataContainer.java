/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package core;

import java.util.HashMap;
import java.util.Map;


public class DataContainer {
    private Map<String, Object> objectMap;

    public DataContainer() {
        objectMap = new HashMap<>();
    }

    /**
     * Store an object in the data container. This will persist for the entire scenario.
     *
     * @param key    the key used for the object
     * @param object the object that will be stored
     */
    public void addObject(String key, Object object) {
        objectMap.put(key, object);
    }

    /**
     * Retrieve and object previously stored in the data container
     *
     * @param key the key of the object
     * @return the object or null if the key does not exist
     */
    public Object getObject(String key) {
        return objectMap.get(key);
    }
}

