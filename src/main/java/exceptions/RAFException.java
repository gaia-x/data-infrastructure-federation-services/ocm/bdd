/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package exceptions;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;

/**
 * General purpose exception , when framework catch some exception but don't have special type for it than throw this one
 */
public class RAFException extends RuntimeException {

    private static final long serialVersionUID = 5696341877105038735L;

    public RAFException(Exception e, Class clazz) {
        super(e);
        if (!(e instanceof RAFException)) {
            LogManager.getLogger(clazz.getSimpleName()).error(ExceptionUtils.getMessage(e));
        }
    }

    public RAFException(String errorMsg, Class clazz) {
        super(errorMsg);
        LogManager.getLogger(clazz.getSimpleName()).error(errorMsg);
    }

}
