/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package api.test.rest.ocm.connection;

import api.test.core.BaseStepDefinitions;
import api.test.rest.RestSessionContainer;
import api.test.rest.pojo.connection.Connection;
import core.*;
import cucumber.api.java.en.Given;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;

public class ConnectionStepDefinitions extends BaseStepDefinitions {
    private static final Logger logger = LogManager.getLogger(ConnectionStepDefinitions.class.getSimpleName());
    RestSessionContainer restSessionContainer;
    Request currentRequest;


    public ConnectionStepDefinitions(RestSessionContainer restSessionContainer, Request currentRequest, DataContainer dataContainer) {
        super(dataContainer);
        this.restSessionContainer = restSessionContainer;
        this.currentRequest = currentRequest;

    }

    @Given("^an administrator generates a QR code by creating a connection with alias \\{(.*?)\\} via OCM api")
    public void an_administrator_generates_a_QR_code_by_creating_a_connection_with_alias_via_OCM_api(String alias) throws Throwable {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("alias", alias);
        currentRequest.setQueryParams(queryParams);

        an_administrator_generates_a_QR_code_by_creating_a_connection_via_OCM_api();
    }

    @Given("^an administrator generates a QR code by creating a connection via OCM api")
    public void an_administrator_generates_a_QR_code_by_creating_a_connection_via_OCM_api() throws Throwable {
        currentRequest.setPath("/connection/v1/invitation-url");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);

        if (getLastResponse() != null && getLastResponse().getStatusCode() == 201) {
            restSessionContainer.setCurrentConnection(JsonUtils.getResourceFromResponse(getLastResponse(), Connection.class));
        }
    }

    @Given("^we call the health check for connection manager via OCM api")
    public void we_call_the_health_check_for_connection_manager_via_OCM_api() {
        currentRequest.setPath("/connection/v1/health");

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^an administrator fetches all the connections via OCM api")
    public void an_administrator_fetches_all_the_connections_via_OCM_api() {
        currentRequest.setPath("/connection/v1/connections");

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^an administrator fetches all the connections with pageSize \\{(.*?)\\} and page \\{(.*?)\\} via OCM api")
    public void an_administrator_fetches_all_the_connections_with_pageSize_and_page_via_OCM_api(String pageSize,String page) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("pageSize",pageSize);
        queryParams.put("page",page);
        currentRequest.setQueryParams(queryParams);

        an_administrator_fetches_all_the_connections_via_OCM_api();
    }

    @Given("^an administrator fetches connection with connectionId \\{(.*?)\\}")
    public void an_administrator_fetches_all_the_connections_via_OCM_api(String connetionId) {
        currentRequest.setPath("/connection/v1/connections/" + connetionId);

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^an administrator fetches the current connection via OCM Api")
    public void an_administrator_fetches_the_current_connection_via_OCM_Api() {
        currentRequest.setPath("/connection/v1/connections/" + restSessionContainer.getCurrentConnection().getData().getConnection().getId());

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }
}