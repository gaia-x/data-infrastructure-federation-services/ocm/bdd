/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package api.test.rest.ocm.attestation;

import api.test.core.BaseStepDefinitions;
import api.test.rest.RestSessionContainer;
import api.test.rest.pojo.credential.Credential;
import api.test.rest.pojo.credentialDef.CredentialDef;
import api.test.rest.pojo.schema.Schema;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AttestationStepDefinitions extends BaseStepDefinitions {
    private static final Logger logger = LogManager.getLogger(AttestationStepDefinitions.class.getSimpleName());
    RestSessionContainer restSessionContainer;
    Request currentRequest;


    public AttestationStepDefinitions(RestSessionContainer restSessionContainer, Request currentRequest, DataContainer dataContainer) {
        super(dataContainer);
        this.restSessionContainer = restSessionContainer;
        this.currentRequest = currentRequest;

    }

    @Given("^we call the health check for attestation manager via OCM api")
    public void we_call_the_health_check_for_attestation_manager_via_OCM_api() {
        currentRequest.setPath("/attestation/v1/health");

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Then("^I create a new schema via Attestation Manager in OCM api")
    public void I_create_a_new_schema_via_Attestation_Manager_in_OCM_api() {
        currentRequest.setPath("/attestation/v1/schemas");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);

        if (getLastResponse() != null && getLastResponse().getStatusCode() == 201) {
            restSessionContainer.setCurrentSchema(JsonUtils.getResourceFromResponse(getLastResponse(), Schema.class));
        }
    }

    @Then("^I create a new schema with random version via Attestation Manager in OCM api")
    public void I_create_a_new_schema_with_random_version_via_Attestation_Manager_in_OCM_api() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(currentRequest.getBody(), JsonElement.class);
        // only try to load body as JsonObject if it is not null.
        JsonObject jsonObject = new JsonObject();
        if (jsonElement != null) jsonObject = jsonElement.getAsJsonObject();;
        jsonObject.addProperty("version", "1.1." + timestamp.getTime());
        currentRequest.setBody(gson.toJson(jsonObject));

        I_create_a_new_schema_via_Attestation_Manager_in_OCM_api();
    }

    @Then("^I get all the schemas via OCM Api")
    public void I_get_all_the_schemas_via_OCM_Api() {
        currentRequest.setPath("/attestation/v1/schemas");

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get all the schemas with pageSize \\{(.*?)\\} and page \\{(.*?)\\} via OCM api")
    public void I_get_all_the_schemas_with_pageSize_and_page_via_OCM_api(String pageSize,String page) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("pageSize",pageSize);
        queryParams.put("page",page);
        currentRequest.setQueryParams(queryParams);

        I_get_all_the_schemas_via_OCM_Api();
    }

    @Given("^I get schema with schemaID \\{(.*?)\\} via OCM Api")
    public void I_get_schema_with_schemaID_via_OCM_Api(String schemaID) {
        currentRequest.setPath("/attestation/v1/schemas/" + schemaID);

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get the current Schema via OCM api")
    public void I_get_the_current_Schema_via_OCM_api() {
        I_get_schema_with_schemaID_via_OCM_Api(restSessionContainer.getCurrentSchema().getData().getSchemaID());
    }

    @Then("^I create a new credential definition via OCM Api")
    public void I_create_a_new_credential_definition_OCM_Api() {
        currentRequest.setPath("/attestation/v1/credentialDef");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);

        if (getLastResponse() != null && getLastResponse().getStatusCode() == 201) {
            restSessionContainer.setCurrentCredentialDef(JsonUtils.getResourceFromResponse(getLastResponse(), CredentialDef.class));
        }
    }

    @Then("^I create a new credential definition with the current schemaId via OCM Api")
    public void I_create_a_new_credential_definition_with_the_current_schemaId_via_OCM_Api() {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(currentRequest.getBody(), JsonElement.class);
        // only try to load body as JsonObject if it is not null.
        JsonObject jsonObject = new JsonObject();
        if (jsonElement != null) jsonObject = jsonElement.getAsJsonObject();;
        jsonObject.addProperty("schemaID", restSessionContainer.getCurrentSchema().getData().getSchemaID());
        currentRequest.setBody(gson.toJson(jsonObject));


        I_create_a_new_credential_definition_OCM_Api();
    }

    @Given("^I create an offer credential with connectionId \\{(.*?)\\} via OCM Api")
    public void I_create_an_offer_credential_with_connectionId_via_OCM_Api(String connectionId) {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(currentRequest.getBody(), JsonElement.class);
        // only try to load body as JsonObject if it is not null.
        JsonObject jsonObject = new JsonObject();
        if (jsonElement != null) jsonObject = jsonElement.getAsJsonObject();;
        jsonObject.addProperty("connectionId", connectionId);
        jsonObject.addProperty("credentialDefinitionId", restSessionContainer.getCurrentCredentialDef().getData().getCredDefId());
        currentRequest.setBody(gson.toJson(jsonObject));

        I_create_an_offer_credential_via_OCM_Api();
    }

    @Given("^I create an offer credential via OCM Api")
    public void I_create_an_offer_credential_via_OCM_Api() {
        currentRequest.setPath("/attestation/v1/create-offer-credential/");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I create an offer credential for the current Connection and Schema via OCM Api")
    public void I_create_an_offer_credential_for_the_current_Connection_and_SchemaVia_OCM_Api() {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(currentRequest.getBody(), JsonElement.class);
        // only try to load body as JsonObject if it is not null.
        JsonObject jsonObject = new JsonObject();
        if (jsonElement != null) jsonObject = jsonElement.getAsJsonObject();;
        jsonObject.addProperty("connectionId", restSessionContainer.getCurrentConnection().getData().getConnection().getId());
        jsonObject.addProperty("credentialDefinitionId", restSessionContainer.getCurrentCredentialDef().getData().getCredDefId());
        currentRequest.setBody(gson.toJson(jsonObject));

        I_create_an_offer_credential_via_OCM_Api();
    }

    @Given("^I get a credential definition with credDefId \\{(.*?)\\} via OCM Api")
    public void I_get_a_credential_definition_with_credDefId_via_OCM_Api(String credDef) {
        currentRequest.setPath("/attestation/v1/credentialDef/" + credDef);

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get current Credential definition via OCM API")
    public void I_get_current_Credential_definition_via_OCM_API() {
        I_get_a_credential_definition_with_credDefId_via_OCM_Api(restSessionContainer.getCurrentCredentialDef().getData().getCredDefId());
    }

    @Given("^I accept request by providing credentialid \\{(.*?)\\} via OCM Api")
    public void I_accept_request_by_providing_credentialid__via_OCM_Api(String credentialId) {
        currentRequest.setPath("/attestation/v1/accept-request?credentialId=" + credentialId);

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I accept request for the current credentialId via OCM Api")
    public void I_accept_request_for_the_current_credentialId_via_OCM_Api() {
        I_accept_request_by_providing_credentialid__via_OCM_Api(restSessionContainer.getCurrentCredentialDef().getData().getCredDefId());
    }

    @Given("^I get all credentials via OCM Api")
    public void I_get_all_credentials_via_OCM_Api() {
        currentRequest.setPath("/attestation/v1/credential");

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);

        if (getLastResponse() != null && getLastResponse().getStatusCode() == 200) {
            restSessionContainer.setCurrentCredentialData(JsonUtils.getResourceFromResponse(getLastResponse(), Credential.class));
        }
    }

    @Given("^I get all credentials with pageSize \\{(.*?)\\} and page \\{(.*?)\\} via OCM api")
    public void I_get_all_credentials_with_pageSize_and_page_via_OCM_api(String pageSize,String page) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("pageSize",pageSize);
        queryParams.put("page",page);
        currentRequest.setQueryParams(queryParams);

        I_get_all_credentials_via_OCM_Api();
    }

    @Given("^I get a credential definition with schemaID \\{(.*?)\\} via OCM Api")
    public void I_get_a_credential_definition_with_schemaID_via_OCM_Api(String schemaID) {
        currentRequest.setPath("/attestation/v1/credentialDef?schemaID=" + schemaID);

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get a credential definition with the current schemaID via OCM Api")
    public void I_get_a_credential_definition_with_the_current_schemaID_via_OCM_Api() {
        currentRequest.setPath("/attestation/v1/credentialDef?schemaID=" + restSessionContainer.getCurrentSchema().getData().getSchemaID());

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get specific credential with filter \\{(.*?)\\} and value \\{(.*?)\\} via OCM api")
    public void I_get_specific_credential_with_filter_and_value_via_OCM_api(String filter,String value) {
        currentRequest.setPath("/attestation/v1/credential?"+filter+"=" + value);

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get the current credential with filter (threadId|principalDid|connectionId|credDefId|state|createdDateStart|expirationDateEnd) via OCM api")
    public void I_get_the_current_credential_with_filter_via_OCM_api(String choice) {
        String result = null;
        switch (choice) {
            case "threadId":
               result = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getThreadId();
                break;
            case "principalDid":
                result = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getPrincipalDid();
                break;
            case "connectionId":
                result = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getConnectionId();
                break;
            case "credDefId":
                result = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getCredDefId();
                break;
            case "state":
                result = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getState();
                break;
            case "createdDateStart":
                result = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getCreatedDate();
                break;
            case "expirationDateEnd":
                result = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getExpirationDate().toString();
                break;
        }
        I_get_specific_credential_with_filter_and_value_via_OCM_api(choice,result);
    }

    @Given("^I get credential-info with credentialId \\{(.*?)\\} via OCM api")
    public void I_get_credential_info_with_credentialId_via_OCM_api(String credentialId) {
        currentRequest.setPath("/attestation/v1/credential-info/" + credentialId);

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get the current credential-info via OCM api")
    public void I_get_the_current_credential_info_via_OCM_api() {
        String credentialId = restSessionContainer.getCurrentCredentialData().getData().getRecords().get(0).getCredentialId();
        I_get_credential_info_with_credentialId_via_OCM_api(credentialId);
    }

    @Given("^I delete credential with credentialId \\{(.*?)\\}  via OCM api")
    public void I_delete_credential_with_credentialId_via_OCM_api(String credentialId) {
        currentRequest.setPath("/attestation/v1/delete-credential/" + credentialId);

        Response response = RestClient.delete(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }
}