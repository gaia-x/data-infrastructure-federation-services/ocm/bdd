/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package api.test.rest.ocm.proof;

import api.test.core.BaseStepDefinitions;
import api.test.rest.RestSessionContainer;
import api.test.rest.pojo.presentationRequest.Presentation;
import com.google.gson.*;
import com.jayway.jsonpath.JsonPath;
import core.*;
import cucumber.api.java.en.Given;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

public class ProofStepDefinitions extends BaseStepDefinitions {
    private static final Logger logger = LogManager.getLogger(ProofStepDefinitions.class.getSimpleName());
    RestSessionContainer restSessionContainer;
    Request currentRequest;


    public ProofStepDefinitions(RestSessionContainer restSessionContainer, Request currentRequest, DataContainer dataContainer) {
        super(dataContainer);
        this.restSessionContainer = restSessionContainer;
        this.currentRequest = currentRequest;

    }

    @Given("^we call the health check for proof manager via OCM api")
    public void we_call_the_health_check_for_proof_manager_via_OCM_api() {
        currentRequest.setPath("/proof/v1/health");

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I send presentation request via OCM Api")
    public void I_send_presentation_request_via_OCM_Api() {
        currentRequest.setPath("/proof/v1/send-presentation-request");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I send presentation request with the current data via OCM Api")
    public void I_send_presentation_request_with_the_current_data_via_OCM_Api() {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(currentRequest.getBody(), JsonElement.class);
        // only try to load body as JsonObject if it is not null.
        JsonObject jsonObject = new JsonObject();
        if (jsonElement != null) jsonObject = jsonElement.getAsJsonObject();;

        jsonObject.addProperty("connectionId", restSessionContainer.getCurrentConnection().getData().getConnection().getId());
        JsonObject newAttributes = new JsonObject();
        newAttributes.addProperty("schemaId",restSessionContainer.getCurrentSchema().getData().getSchemaID());
        newAttributes.addProperty("credentialDefId",restSessionContainer.getCurrentCredentialDef().getData().getCredDefId());
        newAttributes.addProperty("attributeName","lName");
        newAttributes.addProperty("value","");
        newAttributes.addProperty("condition","");
        JsonArray ja = new JsonArray();
        ja.add(newAttributes);

        jsonObject.add("attributes",ja);

        currentRequest.setBody(gson.toJson(jsonObject));

        I_send_presentation_request_via_OCM_Api();
    }

    @Given("^I get all proof presentations via OCM Api")
    public void I_get_all_proof_presentations_via_OCM_Api() {
        currentRequest.setPath("/proof/v1/find-proof-presentation");

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I get all proof presentations with pageSize \\{(.*?)\\} and page \\{(.*?)\\} via OCM api")
    public void I_get_all_proof_presentations_with_pageSize_and_page_via_OCM_api(String pageSize,String page) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("pageSize",pageSize);
        queryParams.put("page",page);
        currentRequest.setQueryParams(queryParams);

        I_get_all_proof_presentations_via_OCM_Api();
    }

    @Given("^I get proof presentations with the current proofRecordId via OCM Api")
    public void I_get_all_proof_presentations_with_the_current_proofRecordId_via_OCM_Api() {
      I_get_proof_presentations_with_proofRecordId_via_OCM_Api(restSessionContainer.getCurrentPresentation().getData().getProofRecordId());
    }

    @Given("^I get proof presentations with proofRecordId \\{(.*?)\\} via OCM Api")
    public void I_get_proof_presentations_with_proofRecordId_via_OCM_Api(String proofRecordId) {
        currentRequest.setPath("/proof/v1/find-by-presentation-id?proofRecordId="+ proofRecordId);

        Response response = RestClient.get(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @Given("^I send out of bound presentation request via OCM Api")
    public void I_send_out_of_bound_presentation_request_via_OCM_Api() {
        currentRequest.setPath("/proof/v1/send-out-of-band-presentation-request");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);

        if (getLastResponse() != null && getLastResponse().getStatusCode() == 201) {
            restSessionContainer.setCurrentPresentation(JsonUtils.getResourceFromResponse(getLastResponse(), Presentation.class));
        }
    }

    @Given("^I send out of bound presentation request with the current data via OCM Api")
    public void I_send_out_of_bound_presentation_request_with_the_current_data_via_OCM_Api() {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(currentRequest.getBody(), JsonElement.class);
        // only try to load body as JsonObject if it is not null.
        JsonObject jsonObject = new JsonObject();
        if (jsonElement != null) jsonObject = jsonElement.getAsJsonObject();;

        jsonObject.addProperty("connectionId", restSessionContainer.getCurrentConnection().getData().getConnection().getId());
        JsonObject newAttributes = new JsonObject();
        newAttributes.addProperty("schemaId",restSessionContainer.getCurrentSchema().getData().getSchemaID());
        newAttributes.addProperty("credentialDefId",restSessionContainer.getCurrentCredentialDef().getData().getCredDefId());
        newAttributes.addProperty("attributeName","lName");
        newAttributes.addProperty("value","");
        newAttributes.addProperty("condition","");
        JsonArray ja = new JsonArray();
        ja.add(newAttributes);

        jsonObject.add("attributes",ja);

        currentRequest.setBody(gson.toJson(jsonObject));
        I_send_out_of_bound_presentation_request_via_OCM_Api();
    }
}