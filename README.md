# Testing framework
This project has been migrated to Eclipse Foundation, and it can be found under https://gitlab.eclipse.org/eclipse/xfsc/

1. [Introduction](#introduction)
1. [Setup](#setup)
1. [Description](#description)


# Introduction

This repository holds the test automation framework based on Java and used for testing TSA

# Setup

### Prerequisites

- Install Java version > 17

- Install gradle version > 7.4.1 - https://gradle.org/install/

- Favorite IDE (I recommend IntelliJ IDEA - https://www.jetbrains.com/idea/)

- If using IntelliJ - Install Cucumber for Java & Gherkin addons

### Running

- Run the tests on a remote env: ```gradle regressionSuite -PbaseUrl=https://TestEnv -Dcucumber.tags="@rest, ~@wip" -Dcourgette.threads=10 -Dcourgette.runLevel=Scenario -Dcourgette.rerunFailedScenarios=false -Dcourgette.rerunAttempts=1```

## License
<hr/>

[GNU Affero General Public license](LICENSE)
