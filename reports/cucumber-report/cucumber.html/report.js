$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/invitation-url/POST.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - invitation-url POST",
  "description": "  It is used to create the connection invitation URL to establish the peer to peer connection,\n  between two aeries agents or the participant user and the principal user.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    }
  ]
});
formatter.scenarioOutline({
  "name": "OCM - Creating a new process connection with alias \u003calias\u003e- Positive",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@connection"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {\u003calias\u003e} via OCM api",
  "keyword": "Given "
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.step({
  "name": "the status code should be {201}",
  "keyword": "And "
});
formatter.step({
  "name": "the response is valid according to the {Connection_POST_invitationURL.json} REST schema",
  "keyword": "And "
});
formatter.step({
  "name": "the field {message} contains the value {Connection created successfully}",
  "keyword": "Then "
});
formatter.step({
  "name": "the field {$..state} contains the value {invited}",
  "keyword": "And "
});
formatter.step({
  "name": "the field {$..role} contains the value {inviter}",
  "keyword": "And "
});
formatter.step({
  "name": "the field {$..alias} contains the value {\u003calias\u003e}",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "alias"
      ]
    },
    {
      "cells": [
        "member"
      ]
    },
    {
      "cells": [
        "subscriber"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Creating a new process connection with alias member- Positive",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {member} via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_generates_a_QR_code_by_creating_a_connection_with_alias_via_OCM_api(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {201}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response is valid according to the {Connection_POST_invitationURL.json} REST schema",
  "keyword": "And "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.the_response_is_valid_according_to_the_REST_schema(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Connection created successfully}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$..state} contains the value {invited}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$..role} contains the value {inviter}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$..alias} contains the value {member}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/invitation-url/POST.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - invitation-url POST",
  "description": "  It is used to create the connection invitation URL to establish the peer to peer connection,\n  between two aeries agents or the participant user and the principal user.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    }
  ]
});
formatter.scenarioOutline({
  "name": "OCM - Trying to create a new member process connection with invalid alias \u003calias\u003e - Negative",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@connection"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {\u003calias\u003e} via OCM api",
  "keyword": "Given "
});
formatter.step({
  "name": "the field {statusCode} contains the value {400}",
  "keyword": "Then "
});
formatter.step({
  "name": "the status code should be {400}",
  "keyword": "And "
});
formatter.step({
  "name": "the field {message} contains the value {Alias must be provided}",
  "keyword": "Then "
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "alias"
      ]
    },
    {
      "cells": [
        "dsadasd"
      ]
    },
    {
      "cells": [
        ""
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Trying to create a new member process connection with invalid alias dsadasd - Negative",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {dsadasd} via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_generates_a_QR_code_by_creating_a_connection_with_alias_via_OCM_api(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {400}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {400}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Alias must be provided}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_clear_the_request_body()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/invitation-url/POST.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - invitation-url POST",
  "description": "  It is used to create the connection invitation URL to establish the peer to peer connection,\n  between two aeries agents or the participant user and the principal user.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    }
  ]
});
formatter.scenarioOutline({
  "name": "OCM - Creating a new process connection with alias \u003calias\u003e- Positive",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@connection"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {\u003calias\u003e} via OCM api",
  "keyword": "Given "
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.step({
  "name": "the status code should be {201}",
  "keyword": "And "
});
formatter.step({
  "name": "the response is valid according to the {Connection_POST_invitationURL.json} REST schema",
  "keyword": "And "
});
formatter.step({
  "name": "the field {message} contains the value {Connection created successfully}",
  "keyword": "Then "
});
formatter.step({
  "name": "the field {$..state} contains the value {invited}",
  "keyword": "And "
});
formatter.step({
  "name": "the field {$..role} contains the value {inviter}",
  "keyword": "And "
});
formatter.step({
  "name": "the field {$..alias} contains the value {\u003calias\u003e}",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "alias"
      ]
    },
    {
      "cells": [
        "member"
      ]
    },
    {
      "cells": [
        "subscriber"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Creating a new process connection with alias subscriber- Positive",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {subscriber} via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_generates_a_QR_code_by_creating_a_connection_with_alias_via_OCM_api(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {201}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response is valid according to the {Connection_POST_invitationURL.json} REST schema",
  "keyword": "And "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.the_response_is_valid_according_to_the_REST_schema(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Connection created successfully}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$..state} contains the value {invited}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$..role} contains the value {inviter}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$..alias} contains the value {subscriber}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/invitation-url/POST.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - invitation-url POST",
  "description": "  It is used to create the connection invitation URL to establish the peer to peer connection,\n  between two aeries agents or the participant user and the principal user.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    }
  ]
});
formatter.scenarioOutline({
  "name": "OCM - Trying to create a new member process connection with invalid alias \u003calias\u003e - Negative",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@connection"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {\u003calias\u003e} via OCM api",
  "keyword": "Given "
});
formatter.step({
  "name": "the field {statusCode} contains the value {400}",
  "keyword": "Then "
});
formatter.step({
  "name": "the status code should be {400}",
  "keyword": "And "
});
formatter.step({
  "name": "the field {message} contains the value {Alias must be provided}",
  "keyword": "Then "
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "alias"
      ]
    },
    {
      "cells": [
        "dsadasd"
      ]
    },
    {
      "cells": [
        ""
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Trying to create a new member process connection with invalid alias  - Negative",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection with alias {} via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_generates_a_QR_code_by_creating_a_connection_with_alias_via_OCM_api(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {400}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {400}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Alias must be provided}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_clear_the_request_body()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/invitation-url/POST.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - invitation-url POST",
  "description": "  It is used to create the connection invitation URL to establish the peer to peer connection,\n  between two aeries agents or the participant user and the principal user.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Trying to create a new member process connection without alies - Negative",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator generates a QR code by creating a connection via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_generates_a_QR_code_by_creating_a_connection_via_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {400}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {400}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Alias must be provided}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_clear_the_request_body()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/attestation/v1/health/GET.feature");
formatter.feature({
  "name": "API - OCM - attestation - v1 - health GET",
  "description": "  Attestation manager health check",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@attestation"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Attestation - Health check - Positive",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@attestation"
    },
    {
      "name": "@health"
    }
  ]
});
formatter.step({
  "name": "we call the health check for attestation manager via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "AttestationStepDefinitions.we_call_the_health_check_for_attestation_manager_via_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {200}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} is present and not empty",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_is_present_and_not_empty(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_clear_the_request_body()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/proof/v1/health/GET.feature");
formatter.feature({
  "name": "API - OCM - proof - v1 - health GET",
  "description": "  proof manager health check",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@proof"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Proof - Health check - Positive",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@proof"
    },
    {
      "name": "@health"
    }
  ]
});
formatter.step({
  "name": "we call the health check for proof manager via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ProofStepDefinitions.we_call_the_health_check_for_proof_manager_via_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {200}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} is present and not empty",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_is_present_and_not_empty(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_clear_the_request_body()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/health/GET.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - health GET",
  "description": "  Connection manager health check",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Connection - Health check - Positive",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@health"
    }
  ]
});
formatter.step({
  "name": "we call the health check for connection manager via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.we_call_the_health_check_for_connection_manager_via_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the status code should be {200}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} is present and not empty",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_is_present_and_not_empty(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_clear_the_request_body()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/connections/%7BconnectionId%7D/GET.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - connections - connectionId GET",
  "description": "  This request is used to fetch connection information for the connection id provided in URL.\n  It also provides state of the connection established.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    }
  ]
});
formatter.scenarioOutline({
  "name": "OCM - Try to get connection by providing invalid connectionID - \u003cconnId\u003e - Negative",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@connections"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator fetches connection with connectionId {\u003cconnId\u003e}",
  "keyword": "Given "
});
formatter.step({
  "name": "the field {statusCode} contains the value {404}",
  "keyword": "Then "
});
formatter.step({
  "name": "the field {message} contains the value {No Data found}",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "connId"
      ]
    },
    {
      "cells": [
        "dasdasdas"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - Try to get connection by providing invalid connectionID - dasdasdas - Negative",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@connections"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator fetches connection with connectionId {dasdasdas}",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_fetches_all_the_connections_via_OCM_api(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {404}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {No Data found}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/connections/GET.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - connections GET",
  "description": "  This request fetches the connection information against the provided participantDID otherwise all the connections are fetched.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - GET all connections with pagination out of bounds - negative",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@connections"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "an administrator fetches all the connections with pageSize {9999} and page {9999} via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_fetches_all_the_connections_with_pageSize_and_page_via_OCM_api(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response is valid according to the {Connection_GetConnections_schema.json} REST schema",
  "keyword": "And "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.the_response_is_valid_according_to_the_REST_schema(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Connections fetch successfully}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$.data.records} contains {0} elements",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_elements(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/connections/GET.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - connections GET",
  "description": "  This request fetches the connection information against the provided participantDID otherwise all the connections are fetched.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - GET all connections with pagination - Positive",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@connections"
    }
  ]
});
formatter.step({
  "name": "an administrator fetches all the connections with pageSize {5} and page {1} via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_fetches_all_the_connections_with_pageSize_and_page_via_OCM_api(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response is valid according to the {Connection_GetConnections_schema.json} REST schema",
  "keyword": "And "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.the_response_is_valid_according_to_the_REST_schema(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Connections fetch successfully}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {$.data.records} contains {5} elements",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_elements(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ocm/connection/v1/connections/GET.feature");
formatter.feature({
  "name": "API - OCM - connection - v1 - connections GET",
  "description": "  This request fetches the connection information against the provided participantDID otherwise all the connections are fetched.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the OCM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "OCM - GET all connections - Positive",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@all"
    },
    {
      "name": "@ocm"
    },
    {
      "name": "@connection"
    },
    {
      "name": "@connections"
    }
  ]
});
formatter.step({
  "name": "an administrator fetches all the connections via OCM api",
  "keyword": "Given "
});
formatter.match({
  "location": "ConnectionStepDefinitions.an_administrator_fetches_all_the_connections_via_OCM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {statusCode} contains the value {200}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response is valid according to the {Connection_GetConnections_schema.json} REST schema",
  "keyword": "And "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.the_response_is_valid_according_to_the_REST_schema(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the field {message} contains the value {Connections fetch successfully}",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_contains_the_value_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});
